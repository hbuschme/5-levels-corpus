1
00:00:00,000 --> 00:00:04,110
hi my name is Talia Gershon and I'm a

2
00:00:01,860 --> 00:00:05,640
scientist at IBM Research today I've

3
00:00:04,110 --> 00:00:07,410
been challenged to explain a topic with

4
00:00:05,640 --> 00:00:09,360
five levels of increasing complexity

5
00:00:07,410 --> 00:00:10,710
it's a completely different kind of

6
00:00:09,360 --> 00:00:12,480
computing called quantum computing

7
00:00:10,710 --> 00:00:14,190
quantum computers approach solving

8
00:00:12,480 --> 00:00:16,109
problems in a fundamentally new way and

9
00:00:14,190 --> 00:00:17,699
we hope that by taking this new approach

10
00:00:16,109 --> 00:00:19,199
to computation we'll be able to start

11
00:00:17,699 --> 00:00:21,150
exploring some problems that we can

12
00:00:19,199 --> 00:00:22,500
never solve any other way hopefully by

13
00:00:21,150 --> 00:00:23,670
the end of today everyone can leave this

14
00:00:22,500 --> 00:00:28,800
discussion understanding quantum

15
00:00:23,670 --> 00:00:31,320
computing at some level what's this what

16
00:00:28,800 --> 00:00:32,880
do you think that is can't you share the

17
00:00:31,320 --> 00:00:34,829
Lear I think so too

18
00:00:32,880 --> 00:00:38,760
we jokingly call it the chandelier

19
00:00:34,829 --> 00:00:43,260
that's real gold you know this is a

20
00:00:38,760 --> 00:00:45,629
quantum computer it's a con it's a

21
00:00:43,260 --> 00:00:48,690
really special kind of computer what

22
00:00:45,629 --> 00:00:50,700
does it do it calculates things but in a

23
00:00:48,690 --> 00:00:54,059
totally different way to how your

24
00:00:50,700 --> 00:00:57,660
computer calculates things what do you

25
00:00:54,059 --> 00:01:03,300
think this is Hey yeah you know what

26
00:00:57,660 --> 00:01:05,189
your computer thinks that is this really

27
00:01:03,300 --> 00:01:07,680
specific combination of zeros and ones

28
00:01:05,189 --> 00:01:09,750
everything that your computer does

29
00:01:07,680 --> 00:01:12,450
showing you Pink Panther videos on

30
00:01:09,750 --> 00:01:14,400
YouTube calculating things searching the

31
00:01:12,450 --> 00:01:16,200
internet it does all of that with a

32
00:01:14,400 --> 00:01:18,210
really specific combination of zeros and

33
00:01:16,200 --> 00:01:20,070
ones which is crazy right that would be

34
00:01:18,210 --> 00:01:22,290
like saying your computer only

35
00:01:20,070 --> 00:01:24,509
understands these quarters for each

36
00:01:22,290 --> 00:01:28,680
quarter you need to tell it that you're

37
00:01:24,509 --> 00:01:30,600
gonna use heads tails and you assign it

38
00:01:28,680 --> 00:01:32,100
heads or tails so I can switch between

39
00:01:30,600 --> 00:01:33,960
heads and tails and I can switch the

40
00:01:32,100 --> 00:01:35,490
zeros and ones in my computer so that it

41
00:01:33,960 --> 00:01:38,700
represents what I wanted to represent

42
00:01:35,490 --> 00:01:41,220
like an A and with quantum computers we

43
00:01:38,700 --> 00:01:44,220
have new rules we get to use too we can

44
00:01:41,220 --> 00:01:45,570
actually spin one of our quarters so it

45
00:01:44,220 --> 00:01:48,530
doesn't have to choose just one or the

46
00:01:45,570 --> 00:01:52,320
other can computers help you with um

47
00:01:48,530 --> 00:01:54,899
your homework or really hard yeah it can

48
00:01:52,320 --> 00:01:56,969
especially if doing your homework

49
00:01:54,899 --> 00:01:58,649
involves calculating something or

50
00:01:56,969 --> 00:02:00,630
finding information but what if your

51
00:01:58,649 --> 00:02:03,299
homework was to discover something

52
00:02:00,630 --> 00:02:05,340
totally new a lot of those discovery

53
00:02:03,299 --> 00:02:07,079
questions are much harder to solve using

54
00:02:05,340 --> 00:02:08,369
the computers we have today so the

55
00:02:07,079 --> 00:02:10,560
reason we're building these kinds of

56
00:02:08,369 --> 00:02:11,790
computers is because we think that maybe

57
00:02:10,560 --> 00:02:13,350
one day they're going to do a lot of

58
00:02:11,790 --> 00:02:15,210
really important things like help

59
00:02:13,350 --> 00:02:18,530
understand nature better maybe help us

60
00:02:15,210 --> 00:02:18,530
create new medicines to help people

61
00:02:19,910 --> 00:02:25,110
what's your favorite kind of computer

62
00:02:21,990 --> 00:02:26,820
smartphone tablet regular laptop PC I've

63
00:02:25,110 --> 00:02:30,360
gotta go with my iPhone so what do you

64
00:02:26,820 --> 00:02:32,250
do with your iPhone social media use it

65
00:02:30,360 --> 00:02:35,670
for studying have you ever run out of

66
00:02:32,250 --> 00:02:38,040
space on your iPhone all the time always

67
00:02:35,670 --> 00:02:39,780
when I'm trying to see the photo so did

68
00:02:38,040 --> 00:02:42,630
you know that there are certain kinds of

69
00:02:39,780 --> 00:02:44,010
problems that computers sort of run out

70
00:02:42,630 --> 00:02:45,270
of space almost like you're trying to

71
00:02:44,010 --> 00:02:46,800
solve the problem and just like how do

72
00:02:45,270 --> 00:02:48,000
you run out of space on your iPhone when

73
00:02:46,800 --> 00:02:49,080
you're trying to take a picture if

74
00:02:48,000 --> 00:02:51,480
you're trying to solve the problem you

75
00:02:49,080 --> 00:02:52,410
just run on space and even if you have

76
00:02:51,480 --> 00:02:54,960
the world's the biggest supercomputer

77
00:02:52,410 --> 00:02:58,110
did you know that can still happen Wow

78
00:02:54,960 --> 00:02:59,820
so my team is working on building new

79
00:02:58,110 --> 00:03:01,380
kinds of computers all together ones

80
00:02:59,820 --> 00:03:04,680
that operate by totally different set of

81
00:03:01,380 --> 00:03:10,020
rules so do you know what that is no

82
00:03:04,680 --> 00:03:11,340
glue it's quantum computer a what you

83
00:03:10,020 --> 00:03:12,690
ever heard of a quantum computer I

84
00:03:11,340 --> 00:03:14,100
haven't have you ever heard of the word

85
00:03:12,690 --> 00:03:17,400
quantum No

86
00:03:14,100 --> 00:03:18,900
okay so quantum mechanics is a branch of

87
00:03:17,400 --> 00:03:20,520
science just like any other branch of

88
00:03:18,900 --> 00:03:22,590
science it's a branch of physics it's

89
00:03:20,520 --> 00:03:24,660
the study of things that are either

90
00:03:22,590 --> 00:03:26,610
really really small really really well

91
00:03:24,660 --> 00:03:29,070
isolated I mean really really cold and

92
00:03:26,610 --> 00:03:30,540
this particular branch of science is

93
00:03:29,070 --> 00:03:32,640
something we're using to totally

94
00:03:30,540 --> 00:03:34,080
reimagine how computing works so we're

95
00:03:32,640 --> 00:03:35,370
building totally new kinds of computers

96
00:03:34,080 --> 00:03:37,800
based on the laws of quantum mechanics

97
00:03:35,370 --> 00:03:39,060
that's what a computer is I'm gonna

98
00:03:37,800 --> 00:03:40,950
start by telling you about something

99
00:03:39,060 --> 00:03:44,760
called superposition so I'm gonna

100
00:03:40,950 --> 00:03:46,950
explain it using this giant penny is

101
00:03:44,760 --> 00:03:49,290
that like worth a hundred pennies know

102
00:03:46,950 --> 00:03:51,000
what it's worth but I can put a face up

103
00:03:49,290 --> 00:03:53,340
right and that's heads I can put a face

104
00:03:51,000 --> 00:03:55,620
down right so at any given time your

105
00:03:53,340 --> 00:03:57,660
point in time if I ask you is my penny

106
00:03:55,620 --> 00:04:00,420
heads or tails probably you could answer

107
00:03:57,660 --> 00:04:05,490
it right yeah okay but what if I spin

108
00:04:00,420 --> 00:04:08,750
the penny so let's do it okay so while

109
00:04:05,490 --> 00:04:13,710
it's spinning is it heads or tails hat

110
00:04:08,750 --> 00:04:15,660
while it's spinning oh I would know it's

111
00:04:13,710 --> 00:04:17,880
sort of it's sort of a combination of

112
00:04:15,660 --> 00:04:20,640
heads and tails right would you say so

113
00:04:17,880 --> 00:04:22,800
superposition is this idea that my penny

114
00:04:20,640 --> 00:04:24,390
is not just either heads or tails it's

115
00:04:22,800 --> 00:04:25,320
in this state which is a combination of

116
00:04:24,390 --> 00:04:27,300
heads and tails

117
00:04:25,320 --> 00:04:30,150
quantum property is something that we

118
00:04:27,300 --> 00:04:30,720
can have in real real physical objects

119
00:04:30,150 --> 00:04:32,490
in the world

120
00:04:30,720 --> 00:04:34,350
so that's superposition and the second

121
00:04:32,490 --> 00:04:36,030
thing that we'll talk about is called

122
00:04:34,350 --> 00:04:41,250
entanglement so now I'm gonna give you a

123
00:04:36,030 --> 00:04:43,710
penny when we use the word entangled in

124
00:04:41,250 --> 00:04:46,320
everyday language what do we mean that

125
00:04:43,710 --> 00:04:47,730
someone's intertwined or exactly that

126
00:04:46,320 --> 00:04:49,590
there's two things that are connected in

127
00:04:47,730 --> 00:04:51,420
some way and usually we can separate

128
00:04:49,590 --> 00:04:53,460
them again your hair is saying older

129
00:04:51,420 --> 00:04:55,230
whatever you can you can unentangled it

130
00:04:53,460 --> 00:04:57,240
right yeah but in the quantum world when

131
00:04:55,230 --> 00:04:58,980
we entangle things they're really now

132
00:04:57,240 --> 00:05:00,990
connected it's much much harder to

133
00:04:58,980 --> 00:05:02,880
separate them again so using the same

134
00:05:00,990 --> 00:05:05,930
analogy we spin our pennies and

135
00:05:02,880 --> 00:05:08,700
eventually eventually they don't stop

136
00:05:05,930 --> 00:05:11,220
and when they stop it's either heads or

137
00:05:08,700 --> 00:05:12,630
tails right so in my case I got tails

138
00:05:11,220 --> 00:05:13,860
and you got heads you see how they're

139
00:05:12,630 --> 00:05:15,960
totally disconnected from each other

140
00:05:13,860 --> 00:05:18,540
right our pennies in the real world now

141
00:05:15,960 --> 00:05:22,200
if our pennies were entangled and we

142
00:05:18,540 --> 00:05:24,240
both spun them together right when we

143
00:05:22,200 --> 00:05:26,250
stopped there if you measured your penny

144
00:05:24,240 --> 00:05:27,900
to be a head I would measure my penny to

145
00:05:26,250 --> 00:05:29,640
be a head and if you measured your penny

146
00:05:27,900 --> 00:05:31,740
to be a tails I measure my opinion to be

147
00:05:29,640 --> 00:05:33,960
a tails if we measured it at exactly the

148
00:05:31,740 --> 00:05:36,000
same time we would still find that they

149
00:05:33,960 --> 00:05:39,900
were both exactly correlated that's

150
00:05:36,000 --> 00:05:41,880
crazy the way that we are able to

151
00:05:39,900 --> 00:05:43,710
actually see these quantum properties is

152
00:05:41,880 --> 00:05:45,210
by making our quantum chips really

153
00:05:43,710 --> 00:05:46,830
really cold so that's what this is all

154
00:05:45,210 --> 00:05:48,960
about actually this is called a dilution

155
00:05:46,830 --> 00:05:50,370
refrigerator and it's a refrigerator it

156
00:05:48,960 --> 00:05:52,140
doesn't look like an Oreo frigerator

157
00:05:50,370 --> 00:05:53,760
right but it's something that we use

158
00:05:52,140 --> 00:05:56,640
actually there's usually a case around

159
00:05:53,760 --> 00:05:58,650
it to cool our quantum chips down cold

160
00:05:56,640 --> 00:06:00,420
enough that we can create super

161
00:05:58,650 --> 00:06:01,770
positions and we can entangle qubits and

162
00:06:00,420 --> 00:06:03,420
the information isn't loss of the

163
00:06:01,770 --> 00:06:05,280
environment like what could those chips

164
00:06:03,420 --> 00:06:06,750
be used to do so one of the things that

165
00:06:05,280 --> 00:06:09,540
we're trying to use quantum computers to

166
00:06:06,750 --> 00:06:11,490
do is simulating chemical bonding use a

167
00:06:09,540 --> 00:06:13,380
quantum system to model a quantum system

168
00:06:11,490 --> 00:06:14,670
yeah I mean I'm definitely gonna impress

169
00:06:13,380 --> 00:06:18,200
all my friends when I tell them about

170
00:06:14,670 --> 00:06:18,200
this they're gonna be like quantum what

171
00:06:18,320 --> 00:06:24,870
so what do you think that thing is is it

172
00:06:21,510 --> 00:06:26,580
some sort of conductor circuit that is a

173
00:06:24,870 --> 00:06:28,380
really good guess there's parts of that

174
00:06:26,580 --> 00:06:30,630
that are definitely about conducting

175
00:06:28,380 --> 00:06:35,190
this is the inside of a quantum computer

176
00:06:30,630 --> 00:06:37,410
oh wow yeah this whole infrastructure is

177
00:06:35,190 --> 00:06:38,940
all about creating levels that get

178
00:06:37,410 --> 00:06:41,130
progressively colder as you go

179
00:06:38,940 --> 00:06:42,660
from top to bottom down to the quantum

180
00:06:41,130 --> 00:06:46,050
chip which is how we actually control

181
00:06:42,660 --> 00:06:47,610
the state of the qubits oh wow so when

182
00:06:46,050 --> 00:06:48,390
you say cold or you mean like physically

183
00:06:47,610 --> 00:06:50,460
colder

184
00:06:48,390 --> 00:06:52,770
yeah like physically colder so room

185
00:06:50,460 --> 00:06:54,180
temperature is 300 Kelvin as you get

186
00:06:52,770 --> 00:06:57,530
down all the way to the bottom of the

187
00:06:54,180 --> 00:07:00,180
fridge it's at 10 Mille Kelvin oh wow

188
00:06:57,530 --> 00:07:01,770
Amanda what he study so I'm studying

189
00:07:00,180 --> 00:07:04,680
computer science currently a sophomore

190
00:07:01,770 --> 00:07:06,750
and the track that I'm in is the

191
00:07:04,680 --> 00:07:09,060
intelligent systems tracks machine

192
00:07:06,750 --> 00:07:10,830
learning artificial intelligence you

193
00:07:09,060 --> 00:07:12,600
ever heard of quantum computing from my

194
00:07:10,830 --> 00:07:15,420
understanding with a quantum computer

195
00:07:12,600 --> 00:07:18,420
rather than using transistors is using

196
00:07:15,420 --> 00:07:21,420
spins you can have superposition of

197
00:07:18,420 --> 00:07:24,090
spins so different states more

198
00:07:21,420 --> 00:07:25,920
combinations means more memory so that's

199
00:07:24,090 --> 00:07:28,620
pretty good so you mentioned

200
00:07:25,920 --> 00:07:29,940
superposition but you can also use other

201
00:07:28,620 --> 00:07:31,110
quantum properties like entanglement

202
00:07:29,940 --> 00:07:33,330
have you heard of entanglement

203
00:07:31,110 --> 00:07:35,430
I have not ok so it's this idea that you

204
00:07:33,330 --> 00:07:36,870
have two objects and when you entangle

205
00:07:35,430 --> 00:07:38,700
them together they become connected oh

206
00:07:36,870 --> 00:07:39,810
and then they're sort of permanently

207
00:07:38,700 --> 00:07:41,820
connected to each other and they behave

208
00:07:39,810 --> 00:07:43,800
in ways that are sort of a system now so

209
00:07:41,820 --> 00:07:45,300
superposition is one quantum property

210
00:07:43,800 --> 00:07:46,770
that we use entanglement is another

211
00:07:45,300 --> 00:07:48,090
quantum property and a third is

212
00:07:46,770 --> 00:07:51,570
interference how much you know about

213
00:07:48,090 --> 00:07:53,610
interference not much okay so how do

214
00:07:51,570 --> 00:07:56,490
north canceling headphones work they

215
00:07:53,610 --> 00:07:58,290
read like wave like ambient wavelengths

216
00:07:56,490 --> 00:08:00,090
and then produce like the opposite one

217
00:07:58,290 --> 00:08:01,919
to cancel out they create interference

218
00:08:00,090 --> 00:08:02,790
so you can have constructive

219
00:08:01,919 --> 00:08:04,320
interference and you can have

220
00:08:02,790 --> 00:08:05,580
destructive interference if you have

221
00:08:04,320 --> 00:08:07,890
constructive interference you have

222
00:08:05,580 --> 00:08:10,380
amplitudes wave amplitudes that add to

223
00:08:07,890 --> 00:08:12,180
the signal gets larger and if you have

224
00:08:10,380 --> 00:08:14,240
destructive interference the amplitudes

225
00:08:12,180 --> 00:08:16,830
cancel by using a property like

226
00:08:14,240 --> 00:08:19,080
interference we can control quantum

227
00:08:16,830 --> 00:08:21,120
states and amplify the kinds of signals

228
00:08:19,080 --> 00:08:23,160
that are towards the right answer and

229
00:08:21,120 --> 00:08:25,050
then cancel the types of signals that

230
00:08:23,160 --> 00:08:26,520
are leading to the wrong answer so given

231
00:08:25,050 --> 00:08:28,380
that you know that we're trying to use

232
00:08:26,520 --> 00:08:30,270
superposition and tangle men and

233
00:08:28,380 --> 00:08:32,370
interference for computation how do you

234
00:08:30,270 --> 00:08:35,430
think we build these computers I have

235
00:08:32,370 --> 00:08:37,650
known so step one is you need to be able

236
00:08:35,430 --> 00:08:40,200
to have an object is achill device we

237
00:08:37,650 --> 00:08:41,700
call it a qubit or quantum bit that can

238
00:08:40,200 --> 00:08:43,110
actually handle those things can

239
00:08:41,700 --> 00:08:45,240
actually be put into superposition of

240
00:08:43,110 --> 00:08:46,650
states you know two qubit states that

241
00:08:45,240 --> 00:08:48,510
you can physically entangle with each

242
00:08:46,650 --> 00:08:50,100
other that's not really trivial right

243
00:08:48,510 --> 00:08:51,450
things in our classical world you can't

244
00:08:50,100 --> 00:08:52,430
really entangle things in our classical

245
00:08:51,450 --> 00:08:55,430
world so easily

246
00:08:52,430 --> 00:08:57,350
we need to use devices where they can

247
00:08:55,430 --> 00:08:59,180
they can support a quantum state and we

248
00:08:57,350 --> 00:09:02,270
can manipulate that quantum state atoms

249
00:08:59,180 --> 00:09:04,160
ions and in our case superconducting

250
00:09:02,270 --> 00:09:06,530
qubits we make qubits out of

251
00:09:04,160 --> 00:09:07,310
superconducting materials but as like a

252
00:09:06,530 --> 00:09:09,920
programmer

253
00:09:07,310 --> 00:09:11,810
how would quantum computing affect a

254
00:09:09,920 --> 00:09:13,250
different way of writing a program it's

255
00:09:11,810 --> 00:09:15,170
a perfect question I mean it's very

256
00:09:13,250 --> 00:09:17,120
early for quantum computing but we're

257
00:09:15,170 --> 00:09:18,410
building assembly languages we're

258
00:09:17,120 --> 00:09:19,970
building layers of abstraction that are

259
00:09:18,410 --> 00:09:21,920
gonna get you to a point as a programmer

260
00:09:19,970 --> 00:09:23,450
where you can interchangeably be

261
00:09:21,920 --> 00:09:25,670
programming something the way that you

262
00:09:23,450 --> 00:09:27,350
already do and then make calls to a

263
00:09:25,670 --> 00:09:28,550
quantum computer so that you can bring

264
00:09:27,350 --> 00:09:30,170
it in when it makes sense

265
00:09:28,550 --> 00:09:31,850
we're not envisioning quantum computers

266
00:09:30,170 --> 00:09:32,660
completely replacing classical computers

267
00:09:31,850 --> 00:09:33,950
any time soon

268
00:09:32,660 --> 00:09:35,540
we think that quantum computing is going

269
00:09:33,950 --> 00:09:37,640
to be used to accelerate the kinds of

270
00:09:35,540 --> 00:09:39,560
things that are really hard for

271
00:09:37,640 --> 00:09:42,200
classical machines so what exactly are

272
00:09:39,560 --> 00:09:43,430
some of those problems simulating nature

273
00:09:42,200 --> 00:09:45,020
is something that's really hard because

274
00:09:43,430 --> 00:09:47,000
if you take something like you know

275
00:09:45,020 --> 00:09:49,220
modeling atomic bonding an electronic

276
00:09:47,000 --> 00:09:51,080
orbital overlap instead of now writing

277
00:09:49,220 --> 00:09:53,330
out a giant summation over many terms

278
00:09:51,080 --> 00:09:55,370
you try and actually mimic the system

279
00:09:53,330 --> 00:09:57,050
you're trying to simulate directly on a

280
00:09:55,370 --> 00:09:59,390
quantum computer which we can do for

281
00:09:57,050 --> 00:10:00,740
chemistry and we're looking at ways of

282
00:09:59,390 --> 00:10:02,210
doing that for other types of things

283
00:10:00,740 --> 00:10:03,620
there's a lot of exciting research right

284
00:10:02,210 --> 00:10:05,480
now on machine learning trying to use

285
00:10:03,620 --> 00:10:08,180
quantum systems to accelerate machine

286
00:10:05,480 --> 00:10:10,760
learning problems so would it be like in

287
00:10:08,180 --> 00:10:13,040
five years or ten years but I would be

288
00:10:10,760 --> 00:10:15,770
able to have like one of these sitting

289
00:10:13,040 --> 00:10:17,030
in my laptop just in my dorm I don't

290
00:10:15,770 --> 00:10:19,100
think you're gonna have one in your dorm

291
00:10:17,030 --> 00:10:20,780
room anytime soon but you'll have access

292
00:10:19,100 --> 00:10:22,190
to one there's three free quantum

293
00:10:20,780 --> 00:10:23,900
computers that are all sitting in this

294
00:10:22,190 --> 00:10:26,540
lab here that anyone in the world can

295
00:10:23,900 --> 00:10:29,930
access through the cloud okay so quantum

296
00:10:26,540 --> 00:10:31,910
computing creates new possibilities and

297
00:10:29,930 --> 00:10:34,100
new ways to approach problems that

298
00:10:31,910 --> 00:10:37,360
classical computers have difficulty

299
00:10:34,100 --> 00:10:40,130
doing couldn't a said it better myself

300
00:10:37,360 --> 00:10:41,900
so I'm a first year masters student and

301
00:10:40,130 --> 00:10:43,340
I'm studying machine learning so it's in

302
00:10:41,900 --> 00:10:45,560
the computer science department but it

303
00:10:43,340 --> 00:10:47,690
mixes computer science with math and

304
00:10:45,560 --> 00:10:49,670
probability and statistics so have you

305
00:10:47,690 --> 00:10:51,470
come up upon sort of any limits to

306
00:10:49,670 --> 00:10:53,740
machine learning certainly depending on

307
00:10:51,470 --> 00:10:55,790
the complexity of your model then

308
00:10:53,740 --> 00:10:57,140
computational speed is one thing I have

309
00:10:55,790 --> 00:10:58,820
colleagues here that tell me you can

310
00:10:57,140 --> 00:11:00,890
take up to weeks to train certain neural

311
00:10:58,820 --> 00:11:02,690
networks right sure yeah and actually

312
00:11:00,890 --> 00:11:04,340
machine learning is one research

313
00:11:02,690 --> 00:11:05,730
direction where we're really hoping that

314
00:11:04,340 --> 00:11:06,990
we're going to find

315
00:11:05,730 --> 00:11:08,790
key parts of the machine learning

316
00:11:06,990 --> 00:11:11,339
computation that can be sped up using

317
00:11:08,790 --> 00:11:12,750
quantum computing yeah it's exciting so

318
00:11:11,339 --> 00:11:14,820
in a classical computer you know you

319
00:11:12,750 --> 00:11:17,670
have all sorts of logical gates that

320
00:11:14,820 --> 00:11:20,519
perform operations and they change an

321
00:11:17,670 --> 00:11:22,560
input to some sort of output but I guess

322
00:11:20,519 --> 00:11:24,449
it's not immediately obvious how you do

323
00:11:22,560 --> 00:11:26,010
that with quantum computers if you think

324
00:11:24,449 --> 00:11:27,870
about even just classical information

325
00:11:26,010 --> 00:11:30,209
like bits right at the end of the day

326
00:11:27,870 --> 00:11:33,510
when you store a bit in your hard drive

327
00:11:30,209 --> 00:11:36,089
there's a magnetic domain and you have a

328
00:11:33,510 --> 00:11:38,100
magnetic polarization right sure you can

329
00:11:36,089 --> 00:11:39,779
change the magnetization to be pointing

330
00:11:38,100 --> 00:11:43,290
up or pointing down right quantum

331
00:11:39,779 --> 00:11:45,300
systems were still manipulating a device

332
00:11:43,290 --> 00:11:47,430
and changing the quantum state of that

333
00:11:45,300 --> 00:11:49,470
of that device you can imagine if it's a

334
00:11:47,430 --> 00:11:51,720
spin that you could have spin up and

335
00:11:49,470 --> 00:11:53,579
spin down but you can also if you

336
00:11:51,720 --> 00:11:56,310
isolate it enough you can have a

337
00:11:53,579 --> 00:11:58,680
superposition of up and down sure so

338
00:11:56,310 --> 00:12:00,930
what we do when we try to solve problems

339
00:11:58,680 --> 00:12:02,220
with a quantum computer is we encode

340
00:12:00,930 --> 00:12:03,959
parts of the problem we're trying to

341
00:12:02,220 --> 00:12:06,209
solve into a complex quantum state and

342
00:12:03,959 --> 00:12:07,440
then we manipulate that state to drive

343
00:12:06,209 --> 00:12:09,449
it towards what will eventually

344
00:12:07,440 --> 00:12:11,940
represent the solution so how do we

345
00:12:09,449 --> 00:12:13,560
actually encode it to start with yeah

346
00:12:11,940 --> 00:12:16,529
that's a really good question this

347
00:12:13,560 --> 00:12:18,720
actually is a model of the inside of one

348
00:12:16,529 --> 00:12:21,720
of our quantum computers ok so you need

349
00:12:18,720 --> 00:12:24,240
a chip with qubits each qubit is a

350
00:12:21,720 --> 00:12:26,490
carrier of quantum information and the

351
00:12:24,240 --> 00:12:28,740
way we control the state of that qubit

352
00:12:26,490 --> 00:12:30,329
is using microwave pulses you send them

353
00:12:28,740 --> 00:12:32,190
all the way down these cables and we've

354
00:12:30,329 --> 00:12:34,589
calibrated these microwave pulses so

355
00:12:32,190 --> 00:12:36,120
that we know exactly this kind of pulse

356
00:12:34,589 --> 00:12:38,430
with this frequency and this duration

357
00:12:36,120 --> 00:12:41,310
will put two qubit into a superposition

358
00:12:38,430 --> 00:12:45,120
or will flip the state of the qubit from

359
00:12:41,310 --> 00:12:47,100
0 to 1 or if we apply a microwave pulse

360
00:12:45,120 --> 00:12:49,050
between two qubits we can entangle them

361
00:12:47,100 --> 00:12:51,240
how do we make yes exactly also through

362
00:12:49,050 --> 00:12:53,699
microwave signals okay the key is to

363
00:12:51,240 --> 00:12:56,100
come up with algorithms where the result

364
00:12:53,699 --> 00:12:57,630
is deterministic interesting so what did

365
00:12:56,100 --> 00:12:58,889
those algorithms look like there's sort

366
00:12:57,630 --> 00:13:02,250
of two main classes of quantum

367
00:12:58,889 --> 00:13:03,899
algorithms there's algorithms which were

368
00:13:02,250 --> 00:13:05,819
developed for decades right things like

369
00:13:03,899 --> 00:13:07,380
Shor's algorithm which is for factoring

370
00:13:05,819 --> 00:13:09,060
Grover's algorithm for unstructured

371
00:13:07,380 --> 00:13:11,040
search and these algorithms were

372
00:13:09,060 --> 00:13:13,079
designed assuming that you had a perfect

373
00:13:11,040 --> 00:13:15,420
fault-tolerant quantum computer which is

374
00:13:13,079 --> 00:13:17,399
many decades away so we're currently in

375
00:13:15,420 --> 00:13:19,140
a phase where we're exploring what can

376
00:13:17,399 --> 00:13:19,769
we do with these near-term quantum

377
00:13:19,140 --> 00:13:21,779
computers

378
00:13:19,769 --> 00:13:23,189
and the answer is gonna be well we need

379
00:13:21,779 --> 00:13:24,629
different kinds of algorithms to really

380
00:13:23,189 --> 00:13:26,129
even explore that question yeah

381
00:13:24,629 --> 00:13:28,139
certainly having a search algorithm is

382
00:13:26,129 --> 00:13:30,089
very useful factoring those are

383
00:13:28,139 --> 00:13:31,679
definitely useful things that I would

384
00:13:30,089 --> 00:13:33,209
imagine could be done a lot faster on

385
00:13:31,679 --> 00:13:35,459
the quantum computer yeah

386
00:13:33,209 --> 00:13:37,230
they also unfortunately require fault

387
00:13:35,459 --> 00:13:39,839
tolerance right now the algorithms that

388
00:13:37,230 --> 00:13:41,879
we know of today to do those things on a

389
00:13:39,839 --> 00:13:44,819
quantum computer require you to have

390
00:13:41,879 --> 00:13:47,459
millions of error corrected qubits today

391
00:13:44,819 --> 00:13:49,379
we're at like 50 it's a it's actually

392
00:13:47,459 --> 00:13:51,269
amazing that we're at 50 there's things

393
00:13:49,379 --> 00:13:53,220
that we know or we have strong reason to

394
00:13:51,269 --> 00:13:55,049
believe are gonna be faster to do on a

395
00:13:53,220 --> 00:13:56,579
quantum computer and then there's things

396
00:13:55,049 --> 00:13:58,230
that we'll discover just by virtue of

397
00:13:56,579 --> 00:14:00,360
having one sure how could someone like

398
00:13:58,230 --> 00:14:01,980
me who's a grad student get involved in

399
00:14:00,360 --> 00:14:03,569
this or what kinds of challenges are you

400
00:14:01,980 --> 00:14:05,189
facing that someone like me could help

401
00:14:03,569 --> 00:14:07,649
out with I'm glad you're interested I

402
00:14:05,189 --> 00:14:10,019
think the place where lots of people can

403
00:14:07,649 --> 00:14:11,939
get involved right now is by going and

404
00:14:10,019 --> 00:14:13,470
trying it out and thinking about what

405
00:14:11,939 --> 00:14:15,329
they could do with it there's a lot of

406
00:14:13,470 --> 00:14:16,860
opportunity to find these near-term

407
00:14:15,329 --> 00:14:21,809
applications that are only going to be

408
00:14:16,860 --> 00:14:24,949
found by trying things out I'm a

409
00:14:21,809 --> 00:14:27,869
theoretical physicist I started out in

410
00:14:24,949 --> 00:14:31,529
condensed matter theory the theory that

411
00:14:27,869 --> 00:14:34,350
studies superconductors and magnets and

412
00:14:31,529 --> 00:14:37,290
you know I had to learn a new field of

413
00:14:34,350 --> 00:14:38,549
quantum optics and apply those ideas one

414
00:14:37,290 --> 00:14:40,350
of the nice things about being a

415
00:14:38,549 --> 00:14:42,149
theorist is you get to keep learning new

416
00:14:40,350 --> 00:14:43,649
things so Steve tell me about your

417
00:14:42,149 --> 00:14:45,509
research and the work you've been doing

418
00:14:43,649 --> 00:14:48,480
in quantum computing my main focus right

419
00:14:45,509 --> 00:14:50,850
now is quantum error correction and

420
00:14:48,480 --> 00:14:53,970
trying to understand this concept of

421
00:14:50,850 --> 00:14:56,399
fault tolerance which everybody thinks

422
00:14:53,970 --> 00:15:00,749
they know it when they see it but nobody

423
00:14:56,399 --> 00:15:01,829
in the quantum case can precisely define

424
00:15:00,749 --> 00:15:03,329
it it's something that we've already

425
00:15:01,829 --> 00:15:04,499
figured out for classical computing like

426
00:15:03,329 --> 00:15:05,759
something that amazes me is all the

427
00:15:04,499 --> 00:15:07,079
parallels between what we're going

428
00:15:05,759 --> 00:15:08,220
through now for quantum computing and

429
00:15:07,079 --> 00:15:10,230
what we went through for classical

430
00:15:08,220 --> 00:15:12,980
computing I was asking computer

431
00:15:10,230 --> 00:15:16,139
scientists recently where to read about

432
00:15:12,980 --> 00:15:17,999
fault tolerance in classical computing

433
00:15:16,139 --> 00:15:20,040
he said oh they don't teach that in

434
00:15:17,999 --> 00:15:23,369
computer science classes anymore because

435
00:15:20,040 --> 00:15:25,709
the hardware has become so reliable in a

436
00:15:23,369 --> 00:15:28,379
quantum system when you look at it or

437
00:15:25,709 --> 00:15:30,360
make measurements it it can change in a

438
00:15:28,379 --> 00:15:32,579
way that's beyond your control we have

439
00:15:30,360 --> 00:15:33,570
the following task build a nearly

440
00:15:32,579 --> 00:15:35,400
perfect

441
00:15:33,570 --> 00:15:38,160
computer out of a whole bunch of

442
00:15:35,400 --> 00:15:40,080
imperfect parts common myth

443
00:15:38,160 --> 00:15:41,400
how many qubits do you have that's the

444
00:15:40,080 --> 00:15:42,930
only thing that matters I just add more

445
00:15:41,400 --> 00:15:44,970
qubits what's the big deal pattern them

446
00:15:42,930 --> 00:15:47,550
on your chip the great power of a

447
00:15:44,970 --> 00:15:50,270
quantum computer is also its Achilles

448
00:15:47,550 --> 00:15:52,820
heel that it's very very sensitive to

449
00:15:50,270 --> 00:15:55,350
perturbations and noise and

450
00:15:52,820 --> 00:15:57,930
environmental effects you're just

451
00:15:55,350 --> 00:16:00,870
multiplying your problems if all you're

452
00:15:57,930 --> 00:16:02,340
doing is adding I think something that

453
00:16:00,870 --> 00:16:04,020
frustrates a lot of people about quantum

454
00:16:02,340 --> 00:16:05,280
computing is the concept of decoherence

455
00:16:04,020 --> 00:16:07,500
right you can only keep your information

456
00:16:05,280 --> 00:16:09,690
quantum for so long right and that

457
00:16:07,500 --> 00:16:11,130
limits how many operations you can do in

458
00:16:09,690 --> 00:16:15,240
a row before you lose your information

459
00:16:11,130 --> 00:16:16,950
that's the challenge I would say as much

460
00:16:15,240 --> 00:16:19,230
progress as we've made it's a

461
00:16:16,950 --> 00:16:20,460
frustration to still be facing it let's

462
00:16:19,230 --> 00:16:22,470
talk about some of the things we think

463
00:16:20,460 --> 00:16:23,880
need to happen between now and fully

464
00:16:22,470 --> 00:16:25,230
fault-tolerant quantum computers to get

465
00:16:23,880 --> 00:16:26,520
us of that reality I mean there's so

466
00:16:25,230 --> 00:16:28,500
many things that need to happen in my

467
00:16:26,520 --> 00:16:29,880
mind one of the things we need to do is

468
00:16:28,500 --> 00:16:31,290
build all these different layers of

469
00:16:29,880 --> 00:16:33,600
abstraction that make it easier for

470
00:16:31,290 --> 00:16:35,580
programmers to come in and just enter at

471
00:16:33,600 --> 00:16:37,490
the ground level yeah yeah exactly so I

472
00:16:35,580 --> 00:16:42,570
think there's gonna be a kind of

473
00:16:37,490 --> 00:16:44,670
coevolution of the hardware and the

474
00:16:42,570 --> 00:16:47,550
software up here and the sort of

475
00:16:44,670 --> 00:16:49,200
middleware and the whole stack another

476
00:16:47,550 --> 00:16:51,420
common myth in the next five years

477
00:16:49,200 --> 00:16:55,560
quantum computing will solve climate

478
00:16:51,420 --> 00:16:58,560
change cancer right in the next five

479
00:16:55,560 --> 00:17:01,410
years there'll be tremendous progress in

480
00:16:58,560 --> 00:17:03,810
the field but people really have to

481
00:17:01,410 --> 00:17:07,050
understand that we're either at the

482
00:17:03,810 --> 00:17:08,670
vacuum tube or transistors stage you

483
00:17:07,050 --> 00:17:11,430
were trying to invent the integrated

484
00:17:08,670 --> 00:17:14,400
circuit and scale it's still very very

485
00:17:11,430 --> 00:17:16,140
very early in the development of the

486
00:17:14,400 --> 00:17:18,390
field one last minute I think we should

487
00:17:16,140 --> 00:17:20,130
bus Steve quantum computers are on the

488
00:17:18,390 --> 00:17:21,959
verge of breaking into your bank account

489
00:17:20,130 --> 00:17:24,120
and breaking encryption and creative

490
00:17:21,959 --> 00:17:26,339
cryptography there does exist an

491
00:17:24,120 --> 00:17:30,210
algorithm Shor's algorithm which has

492
00:17:26,339 --> 00:17:33,140
been proven mathematically that if you

493
00:17:30,210 --> 00:17:37,440
had a large enough quantum computer you

494
00:17:33,140 --> 00:17:40,380
could find the prime factors of large

495
00:17:37,440 --> 00:17:42,929
numbers the basis of the RSA encryption

496
00:17:40,380 --> 00:17:46,440
that's the most common leaves thing

497
00:17:42,929 --> 00:17:50,129
on the Internet first we're far away

498
00:17:46,440 --> 00:17:53,190
from being able to have a quantum

499
00:17:50,129 --> 00:17:56,370
computer big enough to execute Shor's

500
00:17:53,190 --> 00:17:58,830
algorithm on that scale second there are

501
00:17:56,370 --> 00:18:01,799
plenty of other encryption schemes that

502
00:17:58,830 --> 00:18:03,929
don't use factoring and I don't think

503
00:18:01,799 --> 00:18:06,210
anybody has to be concerned at the

504
00:18:03,929 --> 00:18:09,360
moment and in the end quantum mechanics

505
00:18:06,210 --> 00:18:11,580
goes to the side of privacy enhancement

506
00:18:09,360 --> 00:18:15,029
if you have a quantum communication

507
00:18:11,580 --> 00:18:18,809
channel you can encode information and

508
00:18:15,029 --> 00:18:22,529
send it through there and it's provably

509
00:18:18,809 --> 00:18:23,789
secure based on the laws of physics you

510
00:18:22,529 --> 00:18:25,230
know now that everybody around the world

511
00:18:23,789 --> 00:18:26,730
can access a quantum computer through

512
00:18:25,230 --> 00:18:28,440
the cloud people are doing all kinds of

513
00:18:26,730 --> 00:18:29,820
cool things are building games we've

514
00:18:28,440 --> 00:18:31,529
seen the emergence of quantum games

515
00:18:29,820 --> 00:18:34,230
right what do you think people want to

516
00:18:31,529 --> 00:18:37,169
do with them I have no idea what people

517
00:18:34,230 --> 00:18:40,350
are going to end up using them for I

518
00:18:37,169 --> 00:18:42,570
mean if you had gone back thirty years

519
00:18:40,350 --> 00:18:45,179
and handed somebody an iPhone they would

520
00:18:42,570 --> 00:18:49,519
have called you a wizard things are

521
00:18:45,179 --> 00:18:49,519
gonna happen that we just can't foresee

522
00:18:53,629 --> 00:18:57,600
so I hope you enjoyed that foray into

523
00:18:56,220 --> 00:18:59,309
the field of quantum computing I know

524
00:18:57,600 --> 00:19:01,080
I've personally enjoyed getting to see

525
00:18:59,309 --> 00:19:02,159
quantum computing through other people's

526
00:19:01,080 --> 00:19:03,629
eyes coming at it from all these

527
00:19:02,159 --> 00:19:05,039
different levels this is such an

528
00:19:03,629 --> 00:19:07,110
exciting time in the history of quantum

529
00:19:05,039 --> 00:19:08,759
computing only in the last couple years

530
00:19:07,110 --> 00:19:10,230
have real quantum computers become

531
00:19:08,759 --> 00:19:12,210
available to everyone around the world

532
00:19:10,230 --> 00:19:13,860
this is the beginning of a many decade

533
00:19:12,210 --> 00:19:15,269
adventure where we'll discover so many

534
00:19:13,860 --> 00:19:16,860
things about quantum computing and what

535
00:19:15,269 --> 00:19:18,210
it will do we don't even know all the

536
00:19:16,860 --> 00:19:21,280
amazing things that's going to do and to

537
00:19:18,210 --> 00:19:28,089
me that's the most exciting part

538
00:19:21,280 --> 00:19:28,089
[Music]

