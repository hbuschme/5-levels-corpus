1
00:00:00,030 --> 00:00:04,140
my name is Neville San Gianna I'm a

2
00:00:01,860 --> 00:00:05,460
biologist at New York University and the

3
00:00:04,140 --> 00:00:07,379
New York genome Center and I've been

4
00:00:05,460 --> 00:00:13,410
challenged today to teach one concept in

5
00:00:07,379 --> 00:00:15,990
five levels of increasing complexity my

6
00:00:13,410 --> 00:00:18,000
topic is CRISPR CRISPR is a new area of

7
00:00:15,990 --> 00:00:19,980
biomedical science that enables gene

8
00:00:18,000 --> 00:00:22,109
editing and it's helping us understand

9
00:00:19,980 --> 00:00:24,900
the genetic basis of many diseases like

10
00:00:22,109 --> 00:00:26,519
autism or cancer I think today everyone

11
00:00:24,900 --> 00:00:31,470
can leave with understanding something

12
00:00:26,519 --> 00:00:32,730
about CRISPR at some level a Tegan do

13
00:00:31,470 --> 00:00:36,660
you know what we're here to talk about

14
00:00:32,730 --> 00:00:37,770
today we are here to talk about science

15
00:00:36,660 --> 00:00:40,820
we're here to talk about something

16
00:00:37,770 --> 00:00:44,070
called CRISPR have you heard of that no

17
00:00:40,820 --> 00:00:48,239
CRISPR is a tool that scientists are

18
00:00:44,070 --> 00:00:50,430
using to edit or change genomes do you

19
00:00:48,239 --> 00:00:51,899
know what a genome is no it's kind of

20
00:00:50,430 --> 00:00:54,660
like an instruction manual the

21
00:00:51,899 --> 00:00:55,230
instruction manual that makes you who

22
00:00:54,660 --> 00:00:57,329
you are

23
00:00:55,230 --> 00:01:00,739
sometimes there's mistakes in the

24
00:00:57,329 --> 00:01:03,149
instruction manual like people get sick

25
00:01:00,739 --> 00:01:06,680
like allergies do you have do you have

26
00:01:03,149 --> 00:01:06,680
friends at school that have allergies

27
00:01:07,090 --> 00:01:11,040
[Music]

28
00:01:08,930 --> 00:01:12,630
that's good that you actually know

29
00:01:11,040 --> 00:01:14,250
exactly what you're allergic to for

30
00:01:12,630 --> 00:01:16,890
people that have really severe allergies

31
00:01:14,250 --> 00:01:18,479
we could erase we're in that in that big

32
00:01:16,890 --> 00:01:21,030
instruction manual where they have those

33
00:01:18,479 --> 00:01:28,650
allergies and maybe make it so that they

34
00:01:21,030 --> 00:01:31,670
don't have those allergies anymore do

35
00:01:28,650 --> 00:01:34,170
you know what CRISPR is absolutely not

36
00:01:31,670 --> 00:01:36,060
CRISPR is a way to edit the genome do

37
00:01:34,170 --> 00:01:39,659
you know what a genome is yeah it's the

38
00:01:36,060 --> 00:01:40,979
DNA DNA is kind of the language that the

39
00:01:39,659 --> 00:01:42,810
genome is written in and the genome

40
00:01:40,979 --> 00:01:45,119
itself is is an instruction manual it

41
00:01:42,810 --> 00:01:47,460
describes how to make you how tall you

42
00:01:45,119 --> 00:01:49,920
should be what color hair you have or

43
00:01:47,460 --> 00:01:51,899
what color eyes you have so what CRISPR

44
00:01:49,920 --> 00:01:54,060
is and an easy way to think about it

45
00:01:51,899 --> 00:01:56,219
it's like a molecular pair of scissors

46
00:01:54,060 --> 00:01:58,770
that can go through that long long

47
00:01:56,219 --> 00:02:01,350
genome and find specific places make

48
00:01:58,770 --> 00:02:04,469
small cuts and edit it what do you think

49
00:02:01,350 --> 00:02:05,860
about being able to edit genomes then

50
00:02:04,469 --> 00:02:08,590
you could change

51
00:02:05,860 --> 00:02:10,690
we changed things about a person if you

52
00:02:08,590 --> 00:02:12,370
edit the DNA sure so how do we determine

53
00:02:10,690 --> 00:02:13,510
what's what's the right uses thing I

54
00:02:12,370 --> 00:02:16,720
don't think you should be used for

55
00:02:13,510 --> 00:02:19,150
almost cosmetology research like for

56
00:02:16,720 --> 00:02:21,130
people just be like oh I want to be five

57
00:02:19,150 --> 00:02:23,170
foot six instead of five foot four or

58
00:02:21,130 --> 00:02:24,640
like reasons that aren't necessarily the

59
00:02:23,170 --> 00:02:27,310
most informed I just think if it could

60
00:02:24,640 --> 00:02:28,900
genuinely help someone like if someone

61
00:02:27,310 --> 00:02:30,940
had cancer and there was a way to fix it

62
00:02:28,900 --> 00:02:33,070
or like slow down the growth so a lot of

63
00:02:30,940 --> 00:02:36,310
the work that we do in my lab is about

64
00:02:33,070 --> 00:02:38,020
being engineers of DNA we try and look

65
00:02:36,310 --> 00:02:39,700
to see what mutations caused diseases

66
00:02:38,020 --> 00:02:42,730
and see if when we change those

67
00:02:39,700 --> 00:02:44,650
mutations if we can take a sick cell or

68
00:02:42,730 --> 00:02:46,959
organism and make it healthy again you

69
00:02:44,650 --> 00:02:48,730
graduate technically cuz the p53 when

70
00:02:46,959 --> 00:02:51,040
it's defect it doesn't that's what

71
00:02:48,730 --> 00:02:52,870
causes cancers p53 is the most common

72
00:02:51,040 --> 00:02:54,790
mutation in cancer that's that's right

73
00:02:52,870 --> 00:02:58,030
yeah that's a great idea actually to use

74
00:02:54,790 --> 00:03:00,940
CRISPR to target tumor cells and restore

75
00:02:58,030 --> 00:03:05,280
p53 to fix that that mutation so to make

76
00:03:00,940 --> 00:03:05,280
that cut with these scissors and fix it

77
00:03:05,310 --> 00:03:10,480
have you heard about CRISPR yes this

78
00:03:07,810 --> 00:03:13,120
revolutionary team editing tool I know

79
00:03:10,480 --> 00:03:16,180
there have been previous gene editing

80
00:03:13,120 --> 00:03:18,970
tools but CRISPR is more revolutionary

81
00:03:16,180 --> 00:03:20,739
in that it's more precise and a little

82
00:03:18,970 --> 00:03:22,959
bit more affordable do you know how

83
00:03:20,739 --> 00:03:27,700
CRISPR works so it works where you have

84
00:03:22,959 --> 00:03:30,040
this CRISPR this caste 9 complex protein

85
00:03:27,700 --> 00:03:32,950
calcium protein caste 9 protein complex

86
00:03:30,040 --> 00:03:35,320
along with something called a guide RNA

87
00:03:32,950 --> 00:03:38,530
that's right that's right and that RNA

88
00:03:35,320 --> 00:03:40,900
will basically tell this protein where

89
00:03:38,530 --> 00:03:43,209
to go and what you need to sort of cut

90
00:03:40,900 --> 00:03:44,920
out right and this is what makes the

91
00:03:43,209 --> 00:03:47,049
CRISPR system very programmable is that

92
00:03:44,920 --> 00:03:49,299
the low piece of guide RNA is easy to

93
00:03:47,049 --> 00:03:51,220
make we can program CRISPR to go to many

94
00:03:49,299 --> 00:03:53,799
different places in the genome quite

95
00:03:51,220 --> 00:03:56,830
easily but I've heard they're like close

96
00:03:53,799 --> 00:03:59,650
to almost curing muscular dystrophy with

97
00:03:56,830 --> 00:04:01,630
it but there's a lot of I guess ethical

98
00:03:59,650 --> 00:04:03,760
issues that come up with it too one of

99
00:04:01,630 --> 00:04:05,500
the really nice things actually about

100
00:04:03,760 --> 00:04:07,030
CRISPR is that we can use it in human

101
00:04:05,500 --> 00:04:09,160
cells no if you ask most people should

102
00:04:07,030 --> 00:04:11,200
you use it to cure cancer most people

103
00:04:09,160 --> 00:04:13,030
would say yeah those those are good good

104
00:04:11,200 --> 00:04:14,290
uses but there are other areas that are

105
00:04:13,030 --> 00:04:16,510
a little bit more problematic like

106
00:04:14,290 --> 00:04:19,330
editing the germline

107
00:04:16,510 --> 00:04:21,610
means something that could be passed off

108
00:04:19,330 --> 00:04:23,320
ethics wise a lot of people have that

109
00:04:21,610 --> 00:04:25,000
natural reaction of fear against

110
00:04:23,320 --> 00:04:27,160
something new because it has a lot of

111
00:04:25,000 --> 00:04:29,140
potential and we just don't know where

112
00:04:27,160 --> 00:04:31,390
it can take us yet it was sort of

113
00:04:29,140 --> 00:04:34,030
similar to like when people started

114
00:04:31,390 --> 00:04:36,130
doing in vitro fertilization test-tube

115
00:04:34,030 --> 00:04:37,780
babies to B that was the kind of scare

116
00:04:36,130 --> 00:04:40,390
totally exactly

117
00:04:37,780 --> 00:04:50,680
but now we see it's perfectly fine as

118
00:04:40,390 --> 00:04:52,000
long you know it's regulated and so

119
00:04:50,680 --> 00:04:54,430
there's been a lot of talk about using

120
00:04:52,000 --> 00:04:56,050
CRISPR for gene editing in humans and

121
00:04:54,430 --> 00:04:57,550
that that can be a controversial thing I

122
00:04:56,050 --> 00:04:59,320
think very naturally there's many

123
00:04:57,550 --> 00:05:01,990
aspects of ethics that need to be looked

124
00:04:59,320 --> 00:05:04,570
at firstly if we're talking about using

125
00:05:01,990 --> 00:05:08,170
CRISPR are we using it in somatic cells

126
00:05:04,570 --> 00:05:11,740
such as t-cells or are we using it in

127
00:05:08,170 --> 00:05:14,800
embryos or germ stem cells and the issue

128
00:05:11,740 --> 00:05:16,660
with that is now you're modifying the

129
00:05:14,800 --> 00:05:18,580
genome at a germline levels any

130
00:05:16,660 --> 00:05:20,200
unintended consequences could go forward

131
00:05:18,580 --> 00:05:21,850
many generations you're saying with

132
00:05:20,200 --> 00:05:23,560
germline is sure there's unintended

133
00:05:21,850 --> 00:05:26,320
consequences but there's also lack of

134
00:05:23,560 --> 00:05:28,090
consent if you are not around you can't

135
00:05:26,320 --> 00:05:30,430
consent exactly so these future

136
00:05:28,090 --> 00:05:33,280
generations or products of CRISPR they

137
00:05:30,430 --> 00:05:36,220
can't consent another I think ethical

138
00:05:33,280 --> 00:05:38,500
component is if CRISPR does make it to

139
00:05:36,220 --> 00:05:41,050
market who will have access to it is

140
00:05:38,500 --> 00:05:43,750
that going to be cost prohibitive we'll

141
00:05:41,050 --> 00:05:45,700
only a certain select few be able to use

142
00:05:43,750 --> 00:05:47,500
it as many new cancer drugs are today

143
00:05:45,700 --> 00:05:49,540
right right will that be how it is yeah

144
00:05:47,500 --> 00:05:51,940
would people start using it for non

145
00:05:49,540 --> 00:05:54,190
therapeutic reasons and I think this

146
00:05:51,940 --> 00:05:56,230
then can get into a whole slippery slope

147
00:05:54,190 --> 00:05:58,270
so I think a lot of people are concerned

148
00:05:56,230 --> 00:05:59,770
maybe that CRISPR will be used in kind

149
00:05:58,270 --> 00:06:02,800
of frivolous ways maybe just to choose

150
00:05:59,770 --> 00:06:05,560
somebody's eye color or or how tall they

151
00:06:02,800 --> 00:06:07,060
are what color hair they have and I

152
00:06:05,560 --> 00:06:09,310
think what a lot of people don't realize

153
00:06:07,060 --> 00:06:12,610
maybe is that the state of genetics is

154
00:06:09,310 --> 00:06:14,680
not quite there it's nice to think that

155
00:06:12,610 --> 00:06:17,470
I can go in and choose exactly what I

156
00:06:14,680 --> 00:06:19,560
color I want but fundamentally science

157
00:06:17,470 --> 00:06:21,480
doesn't exactly know all of that yet

158
00:06:19,560 --> 00:06:23,250
we don't know every single gene related

159
00:06:21,480 --> 00:06:26,100
to eye color nor the regulatory

160
00:06:23,250 --> 00:06:28,680
mechanisms nor the epigenetics this is a

161
00:06:26,100 --> 00:06:31,020
right so so we can use CRISPR now to try

162
00:06:28,680 --> 00:06:33,389
and understand better how do our genes

163
00:06:31,020 --> 00:06:35,250
relate to these these different features

164
00:06:33,389 --> 00:06:37,200
these different phenotypes that that we

165
00:06:35,250 --> 00:06:39,090
have there is a lot of work left to do

166
00:06:37,200 --> 00:06:40,650
before we even know well I think what

167
00:06:39,090 --> 00:06:43,560
are the knobs and dials what are the

168
00:06:40,650 --> 00:06:45,690
controls that you know affect eye color

169
00:06:43,560 --> 00:06:46,830
or how tall you are so I think this

170
00:06:45,690 --> 00:06:49,440
question right now even though it's

171
00:06:46,830 --> 00:06:52,440
important to discuss I kind of feel like

172
00:06:49,440 --> 00:06:54,870
it's more theoretical right now yeah or

173
00:06:52,440 --> 00:06:56,520
is it really even a point necessary for

174
00:06:54,870 --> 00:06:58,919
conversation because people are going

175
00:06:56,520 --> 00:07:01,380
out every single day and getting plastic

176
00:06:58,919 --> 00:07:03,810
surgery to change how they look how is

177
00:07:01,380 --> 00:07:06,360
this any different and in the same slant

178
00:07:03,810 --> 00:07:08,550
people are using in vitro fertilization

179
00:07:06,360 --> 00:07:10,500
right so this is manipulating genomes

180
00:07:08,550 --> 00:07:13,830
you're choosing which embryos to implant

181
00:07:10,500 --> 00:07:18,710
so we're almost doing little aspects of

182
00:07:13,830 --> 00:07:18,710
what CRISPR has the potential to do

183
00:07:20,180 --> 00:07:23,879
so Matt what are you doing right now in

184
00:07:22,319 --> 00:07:25,919
the lab and how are you using gene

185
00:07:23,879 --> 00:07:27,719
editing in your own work right now one

186
00:07:25,919 --> 00:07:29,129
of the questions I've been very much

187
00:07:27,719 --> 00:07:32,009
interested in is trying to understand

188
00:07:29,129 --> 00:07:34,830
the effect of human genetic variation on

189
00:07:32,009 --> 00:07:36,539
using CRISPR casts nine reagents because

190
00:07:34,830 --> 00:07:38,039
you and I just sitting here different

191
00:07:36,539 --> 00:07:40,379
millions of different in locations

192
00:07:38,039 --> 00:07:42,990
within our genome we use these guide

193
00:07:40,379 --> 00:07:44,310
RNAs which are 20 base pairs and we

194
00:07:42,990 --> 00:07:46,469
match it to different places in the

195
00:07:44,310 --> 00:07:48,330
genome but what if there's a mutation at

196
00:07:46,469 --> 00:07:49,409
that site and how does that affect a

197
00:07:48,330 --> 00:07:50,909
mutation that's different between you

198
00:07:49,409 --> 00:07:52,710
and me it's the same site but there's a

199
00:07:50,909 --> 00:07:54,810
slight difference in the DNA between you

200
00:07:52,710 --> 00:07:56,639
and it is exactly let's say in you it's

201
00:07:54,810 --> 00:07:58,949
a perfect match in me there's one that's

202
00:07:56,639 --> 00:08:01,860
actually a mismatch and how does that

203
00:07:58,949 --> 00:08:03,900
affect things in the lab because maybe

204
00:08:01,860 --> 00:08:05,759
it won't be as efficient it won't be

205
00:08:03,900 --> 00:08:07,590
able to bring caste 9 to the correct

206
00:08:05,759 --> 00:08:09,569
site and cut there so when when people

207
00:08:07,590 --> 00:08:11,879
are using CRISPR now in the lab are they

208
00:08:09,569 --> 00:08:13,710
really sequencing the exact cell type

209
00:08:11,879 --> 00:08:15,629
they use sequencing reading out the

210
00:08:13,710 --> 00:08:17,789
genome of that cell before they use

211
00:08:15,629 --> 00:08:19,710
CRISPR not many do that it would

212
00:08:17,789 --> 00:08:21,539
ultimately the best way to do it

213
00:08:19,710 --> 00:08:24,599
for sure because the reference genome

214
00:08:21,539 --> 00:08:27,330
that was published in 2001 was kind of

215
00:08:24,599 --> 00:08:28,830
just one person's genome and as we know

216
00:08:27,330 --> 00:08:29,849
very well from sequencing many different

217
00:08:28,830 --> 00:08:32,370
individuals there are a lot of

218
00:08:29,849 --> 00:08:33,899
differences and so there are some

219
00:08:32,370 --> 00:08:36,510
concerns that when you use these in the

220
00:08:33,899 --> 00:08:37,919
lab that you're intending for a cut to

221
00:08:36,510 --> 00:08:39,269
be made in this one place in the genome

222
00:08:37,919 --> 00:08:40,860
but if there happens to be a mismatch

223
00:08:39,269 --> 00:08:42,750
there because there's some variant in

224
00:08:40,860 --> 00:08:45,060
the cells that you're studying maybe it

225
00:08:42,750 --> 00:08:46,649
won't cut there at all and even maybe

226
00:08:45,060 --> 00:08:48,180
worse is that maybe it'll cut somewhere

227
00:08:46,649 --> 00:08:49,800
else because maybe now it can match

228
00:08:48,180 --> 00:08:51,779
elsewhere so you're saying right now

229
00:08:49,800 --> 00:08:53,100
that it's it's kind of early days with

230
00:08:51,779 --> 00:08:55,260
CRISPR and that there's a lot of

231
00:08:53,100 --> 00:08:58,110
technical issues that really need to be

232
00:08:55,260 --> 00:09:00,029
ironed out that it's not anything can be

233
00:08:58,110 --> 00:09:01,800
targeted at any time we're trying to

234
00:09:00,029 --> 00:09:04,320
develop the scientific steps that will

235
00:09:01,800 --> 00:09:07,290
take us to that kind of total genome

236
00:09:04,320 --> 00:09:08,640
control it is still the early days and

237
00:09:07,290 --> 00:09:10,620
as much progress that's been done which

238
00:09:08,640 --> 00:09:12,449
is actually quite remarkable given how

239
00:09:10,620 --> 00:09:14,699
new this technology is I mean we're just

240
00:09:12,449 --> 00:09:15,600
a few years away from when it was really

241
00:09:14,699 --> 00:09:18,089
started being used for these

242
00:09:15,600 --> 00:09:20,430
applications however having said that I

243
00:09:18,089 --> 00:09:22,230
holds a lot of potential and I think can

244
00:09:20,430 --> 00:09:23,970
get there but there's a lot of issues

245
00:09:22,230 --> 00:09:25,199
and concerns that need to be worked out

246
00:09:23,970 --> 00:09:27,480
I was going to say that a lot of what

247
00:09:25,199 --> 00:09:30,890
inspired me to get into bioengineering

248
00:09:27,480 --> 00:09:33,050
are movies movies like Jurassic Park

249
00:09:30,890 --> 00:09:35,510
which both shows the good

250
00:09:33,050 --> 00:09:37,640
the bad sides of manipulating the nature

251
00:09:35,510 --> 00:09:39,080
around us and so maybe it's a bit of a

252
00:09:37,640 --> 00:09:41,120
silly question but what do you think in

253
00:09:39,080 --> 00:09:43,160
our lifetime will we see something like

254
00:09:41,120 --> 00:09:45,410
Jurassic Park where humans have actually

255
00:09:43,160 --> 00:09:46,700
engineered different different animals

256
00:09:45,410 --> 00:09:48,529
anything is possible

257
00:09:46,700 --> 00:09:50,000
I'm not sure that we will see that in

258
00:09:48,529 --> 00:09:51,829
our lifetimes but I would be very

259
00:09:50,000 --> 00:09:53,540
excited to attend Jurassic Park if that

260
00:09:51,829 --> 00:09:56,000
were the case having said that I do

261
00:09:53,540 --> 00:09:59,209
think that CRISPR Cassadine does have a

262
00:09:56,000 --> 00:10:00,950
lot of potential to two things that are

263
00:09:59,209 --> 00:10:02,690
kind of out of the box in the past few

264
00:10:00,950 --> 00:10:04,640
years we've been able to tackle ideas

265
00:10:02,690 --> 00:10:06,920
that people didn't think we're possible

266
00:10:04,640 --> 00:10:09,829
before that more than anything

267
00:10:06,920 --> 00:10:11,930
CRISPR has created a whole open field of

268
00:10:09,829 --> 00:10:13,730
opportunities and so whether or not that

269
00:10:11,930 --> 00:10:15,470
leads us Jurassic Park I can't tell you

270
00:10:13,730 --> 00:10:17,240
that answer for sure but I certainly

271
00:10:15,470 --> 00:10:18,560
hope that it does and I think you're

272
00:10:17,240 --> 00:10:20,120
right that the opportunities might be

273
00:10:18,560 --> 00:10:22,100
much broader than just biomedical

274
00:10:20,120 --> 00:10:25,279
science it might be things like using

275
00:10:22,100 --> 00:10:27,620
DNA as a hard drive being able to use

276
00:10:25,279 --> 00:10:30,110
CRISPR as a diagnostic instead of just a

277
00:10:27,620 --> 00:10:32,060
tool for editing and we yeah use cells

278
00:10:30,110 --> 00:10:34,310
to record data over time with with

279
00:10:32,060 --> 00:10:35,930
CRISPR can we use CRISPR to track cells

280
00:10:34,310 --> 00:10:37,640
in a developing embryo I think this is

281
00:10:35,930 --> 00:10:39,170
one of the most exciting recent uses of

282
00:10:37,640 --> 00:10:41,420
CRISPR I've seen that I would have never

283
00:10:39,170 --> 00:10:43,880
predicted a year ago the dominant

284
00:10:41,420 --> 00:10:45,709
paradigm right now is CRISPR is just for

285
00:10:43,880 --> 00:10:47,810
cutting DNA but when you think of it

286
00:10:45,709 --> 00:10:50,300
more as a general platform a pointer to

287
00:10:47,810 --> 00:10:52,220
a location in a genome then that becomes

288
00:10:50,300 --> 00:10:53,680
a very very powerful thing do you think

289
00:10:52,220 --> 00:10:56,660
CRISPR will be helpful for understanding

290
00:10:53,680 --> 00:10:58,670
human variation beyond what we already

291
00:10:56,660 --> 00:11:00,980
have just by sequencing people's genomes

292
00:10:58,670 --> 00:11:03,320
yes absolutely I think that the real

293
00:11:00,980 --> 00:11:04,940
power of CRISPR lets us to follow up on

294
00:11:03,320 --> 00:11:06,890
this genetic information that we've been

295
00:11:04,940 --> 00:11:08,450
studying and following for a very long

296
00:11:06,890 --> 00:11:09,890
time now there been a lot of studies

297
00:11:08,450 --> 00:11:11,899
where you take a group of people with

298
00:11:09,890 --> 00:11:13,880
disease X and people without that

299
00:11:11,899 --> 00:11:15,770
disease and you see if any mutations

300
00:11:13,880 --> 00:11:17,750
seem to be preferentially in the group

301
00:11:15,770 --> 00:11:19,339
with the disease and kind of absent in

302
00:11:17,750 --> 00:11:21,410
the group without the disease people

303
00:11:19,339 --> 00:11:23,300
have observed these differences but what

304
00:11:21,410 --> 00:11:26,660
you're saying is the CRISPR we can now

305
00:11:23,300 --> 00:11:29,180
test is this causal this factor if we

306
00:11:26,660 --> 00:11:30,769
make this mutation does it cause the

307
00:11:29,180 --> 00:11:33,110
disease does it either create

308
00:11:30,769 --> 00:11:34,760
sickle-cell anemia or ameliorate sickle

309
00:11:33,110 --> 00:11:36,680
cell anemia and prevent prevent it do

310
00:11:34,760 --> 00:11:39,230
you think we'll ever be at a place where

311
00:11:36,680 --> 00:11:41,329
based on looking at someone's genome and

312
00:11:39,230 --> 00:11:43,570
these kinds of CRISPR functional screens

313
00:11:41,329 --> 00:11:44,950
we can predict

314
00:11:43,570 --> 00:11:47,260
what kinds of diseases these people

315
00:11:44,950 --> 00:11:49,330
might get 10 20 years in the future I

316
00:11:47,260 --> 00:11:51,880
think that's certainly a great goal as

317
00:11:49,330 --> 00:11:53,800
of now we tend to focus on variants or

318
00:11:51,880 --> 00:11:55,240
mutations that we know the function what

319
00:11:53,800 --> 00:11:57,400
about mutations we've never seen before

320
00:11:55,240 --> 00:11:59,260
something that comes from let's say UV

321
00:11:57,400 --> 00:12:01,240
radiation in melanoma or something I

322
00:11:59,260 --> 00:12:03,490
think that we will ultimately have the

323
00:12:01,240 --> 00:12:05,770
power to do that and so thinking about

324
00:12:03,490 --> 00:12:08,230
when you start from mutation do you look

325
00:12:05,770 --> 00:12:09,820
at the expression of RNA is that change

326
00:12:08,230 --> 00:12:11,350
is it changing things at the protein

327
00:12:09,820 --> 00:12:13,330
level so this is interesting so you

328
00:12:11,350 --> 00:12:15,430
mentioned that we can analyze genomes

329
00:12:13,330 --> 00:12:16,990
the DNA level and then you said the RNA

330
00:12:15,430 --> 00:12:18,730
level looking at the expression of genes

331
00:12:16,990 --> 00:12:21,040
and then the proteins the products that

332
00:12:18,730 --> 00:12:24,490
they actually make with proteomics or

333
00:12:21,040 --> 00:12:25,840
mass spectrometry but do you think it's

334
00:12:24,490 --> 00:12:28,300
kind of like another level of

335
00:12:25,840 --> 00:12:30,670
understanding the function of the genome

336
00:12:28,300 --> 00:12:32,680
or do you view it as part of one one of

337
00:12:30,670 --> 00:12:34,420
those those levels I do think it

338
00:12:32,680 --> 00:12:37,570
provides a different level of data the

339
00:12:34,420 --> 00:12:39,820
ability to rapidly and genome-wide

340
00:12:37,570 --> 00:12:42,010
assess and make changes at the DNA level

341
00:12:39,820 --> 00:12:43,510
and then look at the resulting changes

342
00:12:42,010 --> 00:12:44,770
of the RNA and protein level I think is

343
00:12:43,510 --> 00:12:47,080
something that we haven't been able to

344
00:12:44,770 --> 00:12:48,610
do and is now I think on a rapidly

345
00:12:47,080 --> 00:12:50,290
advanced understanding at a genetic

346
00:12:48,610 --> 00:12:52,240
level at the DNA level of a lot of

347
00:12:50,290 --> 00:12:54,070
different diseases so so you and I have

348
00:12:52,240 --> 00:12:55,960
done functional screens we've done these

349
00:12:54,070 --> 00:12:58,660
large-scale CRISPR screens where we

350
00:12:55,960 --> 00:13:00,730
modify many genes thousands of genes or

351
00:12:58,660 --> 00:13:02,920
thousands of locations in the genome all

352
00:13:00,730 --> 00:13:04,590
in kind of a test system but do you ever

353
00:13:02,920 --> 00:13:07,360
see this one day the idea of these

354
00:13:04,590 --> 00:13:09,190
multiplex pooled CRISPR screens going

355
00:13:07,360 --> 00:13:10,840
into the clinic that we actually take a

356
00:13:09,190 --> 00:13:13,180
clinical cell line a cell line from a

357
00:13:10,840 --> 00:13:14,830
patient absolutely I think that is kind

358
00:13:13,180 --> 00:13:16,960
of one of the ultimate goals ultimately

359
00:13:14,830 --> 00:13:19,570
it would be very nice to be able to

360
00:13:16,960 --> 00:13:21,160
correct the mutations for these diseases

361
00:13:19,570 --> 00:13:22,390
we've known about for sometimes hundreds

362
00:13:21,160 --> 00:13:24,100
of years and we just haven't been able

363
00:13:22,390 --> 00:13:25,840
to do anything about there's a lot of

364
00:13:24,100 --> 00:13:28,120
kind of ethical issues surrounding very

365
00:13:25,840 --> 00:13:29,230
reasonably using CRISPR in the clinic I

366
00:13:28,120 --> 00:13:32,080
think there's a lot of concern about

367
00:13:29,230 --> 00:13:34,630
trying to use CRISPR technology other

368
00:13:32,080 --> 00:13:37,210
genome technologies to make humans

369
00:13:34,630 --> 00:13:39,100
designed in a specific way whether you

370
00:13:37,210 --> 00:13:41,440
want to change eye color height or

371
00:13:39,100 --> 00:13:43,360
things like that like kind of irony of

372
00:13:41,440 --> 00:13:45,310
that situation is that the genetics of a

373
00:13:43,360 --> 00:13:47,080
lot of the traits that are you know you

374
00:13:45,310 --> 00:13:50,650
can see by just looking at a person are

375
00:13:47,080 --> 00:13:52,330
quite complex and are not worked out in

376
00:13:50,650 --> 00:13:54,010
the same way so you're saying that the

377
00:13:52,330 --> 00:13:56,410
ethical problem is not really right now

378
00:13:54,010 --> 00:13:57,819
a clear and present issue because for

379
00:13:56,410 --> 00:13:59,829
many of the

380
00:13:57,819 --> 00:14:02,109
perhaps a little bit more superficial

381
00:13:59,829 --> 00:14:04,509
traits that we have we don't really even

382
00:14:02,109 --> 00:14:07,509
know what what genes can control these

383
00:14:04,509 --> 00:14:08,559
these traits so it's probably a good

384
00:14:07,509 --> 00:14:11,549
idea to start thinking a little bit

385
00:14:08,559 --> 00:14:13,600
about engineering somatic cells versus

386
00:14:11,549 --> 00:14:16,299
germline cells which are passed on to

387
00:14:13,600 --> 00:14:17,769
future generations and I think one nice

388
00:14:16,299 --> 00:14:19,749
thing is that most scientists agree that

389
00:14:17,769 --> 00:14:21,609
for at least for human gene editing that

390
00:14:19,749 --> 00:14:23,049
we need to focus on somatic cells which

391
00:14:21,609 --> 00:14:25,419
really impacts a lot of people with

392
00:14:23,049 --> 00:14:27,249
these diseases and that there's many

393
00:14:25,419 --> 00:14:29,319
many issues around editing the germline

394
00:14:27,249 --> 00:14:31,540
where there you're doing something that

395
00:14:29,319 --> 00:14:33,730
might be inherited for for many many

396
00:14:31,540 --> 00:14:35,019
generations so it's kind of an

397
00:14:33,730 --> 00:14:37,600
interesting misconception that people

398
00:14:35,019 --> 00:14:40,359
think that CRISPR is just one thing just

399
00:14:37,600 --> 00:14:41,980
one enzyme casts nine I think that's a

400
00:14:40,359 --> 00:14:43,569
really great point and that speaks to

401
00:14:41,980 --> 00:14:45,369
just how fast the technology is

402
00:14:43,569 --> 00:14:48,009
developing because it started with us

403
00:14:45,369 --> 00:14:50,439
just knowing about cast nine but in just

404
00:14:48,009 --> 00:14:52,899
one cast nine right s pyogenes cast nine

405
00:14:50,439 --> 00:14:56,169
was kind of the first one used for gene

406
00:14:52,899 --> 00:14:58,419
editing in human cells exactly and just

407
00:14:56,169 --> 00:15:00,790
in the past few years this this toolbox

408
00:14:58,419 --> 00:15:02,019
has now been expanded well past that for

409
00:15:00,790 --> 00:15:04,119
example you can look into other

410
00:15:02,019 --> 00:15:06,699
different species of bacteria to find

411
00:15:04,119 --> 00:15:08,679
cast 9 ok so cast 9 is over many

412
00:15:06,699 --> 00:15:10,569
different species it's over a tremendous

413
00:15:08,679 --> 00:15:13,299
amount of bacteria and so you had

414
00:15:10,569 --> 00:15:15,129
reference strep pyogenes cast 9 well if

415
00:15:13,299 --> 00:15:17,949
they you look into Staphylococcus aureus

416
00:15:15,129 --> 00:15:20,049
you can also find cast 9 and that cast

417
00:15:17,949 --> 00:15:21,669
line is just slightly different it can

418
00:15:20,049 --> 00:15:23,199
target different regions of the genome

419
00:15:21,669 --> 00:15:24,819
and so that opens up a whole new set of

420
00:15:23,199 --> 00:15:26,829
possibilities in terms of genome editing

421
00:15:24,819 --> 00:15:28,329
so it increases the targetable space

422
00:15:26,829 --> 00:15:30,699
within the human genome you can target

423
00:15:28,329 --> 00:15:33,399
more regions that you couldn't with that

424
00:15:30,699 --> 00:15:34,360
first first cast line and what I find

425
00:15:33,399 --> 00:15:37,269
actually is a really interesting thing

426
00:15:34,360 --> 00:15:39,699
is that there's this huge meta genomic

427
00:15:37,269 --> 00:15:41,709
diversity of different CRISPR systems

428
00:15:39,699 --> 00:15:43,539
and most of them are really just not

429
00:15:41,709 --> 00:15:45,339
even well characterized yet and I think

430
00:15:43,539 --> 00:15:47,799
the best example that we were talking

431
00:15:45,339 --> 00:15:49,689
about it earlier is this new RNA

432
00:15:47,799 --> 00:15:53,319
targeting CRISPR so instead of cutting

433
00:15:49,689 --> 00:15:54,819
DNA it targets RNA and I think this is

434
00:15:53,319 --> 00:15:56,319
just one example of a complete paradigm

435
00:15:54,819 --> 00:15:58,119
shift I don't think anyone have

436
00:15:56,319 --> 00:15:59,709
necessarily predicted at the beginning

437
00:15:58,119 --> 00:16:01,660
that this would be one of the things

438
00:15:59,709 --> 00:16:03,039
that just was created and existed in

439
00:16:01,660 --> 00:16:04,839
nature and it just took some time for

440
00:16:03,039 --> 00:16:06,459
people to uncover it I think it's a

441
00:16:04,839 --> 00:16:08,019
great good example why it's important

442
00:16:06,459 --> 00:16:09,549
and actually the fund base basic science

443
00:16:08,019 --> 00:16:10,610
because you sometimes when you start out

444
00:16:09,549 --> 00:16:12,140
down a certain path

445
00:16:10,610 --> 00:16:13,940
you never know where you're gonna end up

446
00:16:12,140 --> 00:16:16,040
in the end the more research I can be

447
00:16:13,940 --> 00:16:17,510
done in this area can really unleash new

448
00:16:16,040 --> 00:16:19,670
possibilities finding new things

449
00:16:17,510 --> 00:16:21,740
different versions of cast 9 or other

450
00:16:19,670 --> 00:16:23,480
things that function like cast 9 or

451
00:16:21,740 --> 00:16:25,040
things completely different than at this

452
00:16:23,480 --> 00:16:28,010
moment I can't even think of the

453
00:16:25,040 --> 00:16:29,690
possibilities are endless to be a little

454
00:16:28,010 --> 00:16:31,940
cliche about it now literally I think

455
00:16:29,690 --> 00:16:33,440
they are endless for sure I think it's

456
00:16:31,940 --> 00:16:34,940
great for people of all ages to

457
00:16:33,440 --> 00:16:36,980
understand CRISPR and at least a little

458
00:16:34,940 --> 00:16:38,720
bit about genome engineering because so

459
00:16:36,980 --> 00:16:40,820
much of the world today revolves around

460
00:16:38,720 --> 00:16:43,550
biology also there's something about

461
00:16:40,820 --> 00:16:45,950
self understanding we really want to

462
00:16:43,550 --> 00:16:48,410
understand ourselves and what makes up

463
00:16:45,950 --> 00:16:50,149
ourselves and CRISPR and gene editing is

464
00:16:48,410 --> 00:16:54,010
another way to get at what is the

465
00:16:50,149 --> 00:16:54,010
substance underlying us

