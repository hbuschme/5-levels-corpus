1
00:00:00,030 --> 00:00:03,959
hello everybody my name is Jacob Collier

2
00:00:01,829 --> 00:00:06,180
and I'm a musician I've been challenged

3
00:00:03,959 --> 00:00:09,719
today to explain one simple concept in

4
00:00:06,180 --> 00:00:12,480
five levels of increasing complexity my

5
00:00:09,719 --> 00:00:14,519
topic harmony I'm positive that

6
00:00:12,480 --> 00:00:19,770
everybody can leave this video with some

7
00:00:14,519 --> 00:00:20,789
understanding at some level od how's it

8
00:00:19,770 --> 00:00:23,460
going good

9
00:00:20,789 --> 00:00:26,820
cool so do you know what harmony is it's

10
00:00:23,460 --> 00:00:27,660
when people sing together and it sounds

11
00:00:26,820 --> 00:00:29,369
nice yeah

12
00:00:27,660 --> 00:00:31,529
that's 100% correct have you ever heard

13
00:00:29,369 --> 00:00:33,540
a song called amazing grace yeah here's

14
00:00:31,529 --> 00:00:39,329
a good one the melody on its own just

15
00:00:33,540 --> 00:00:41,790
goes that melody on its own is kind of

16
00:00:39,329 --> 00:00:43,910
lonely right and no one really knows how

17
00:00:41,790 --> 00:00:43,910
it feels

18
00:00:46,920 --> 00:00:51,510
so which one did you prefer second one

19
00:00:49,710 --> 00:00:53,340
awesome and why did you prefer that one

20
00:00:51,510 --> 00:00:55,320
because it sounds better yeah oh that's

21
00:00:53,340 --> 00:00:57,570
great I can decide how I want this

22
00:00:55,320 --> 00:00:59,340
ability to feel and the more notes there

23
00:00:57,570 --> 00:01:02,809
are the more exciting it is that's what

24
00:00:59,340 --> 00:01:09,000
musical harmony is does that make sense

25
00:01:02,809 --> 00:01:11,250
thank you have you ever heard of harmony

26
00:01:09,000 --> 00:01:12,930
yes okay so what do you think harmony is

27
00:01:11,250 --> 00:01:15,030
I think basically it's like one person

28
00:01:12,930 --> 00:01:17,040
has the lower voice and like a girl

29
00:01:15,030 --> 00:01:19,229
usually has the higher voice yeah and

30
00:01:17,040 --> 00:01:21,270
then they blend it together I like it

31
00:01:19,229 --> 00:01:24,690
that's absolutely correct harmony is

32
00:01:21,270 --> 00:01:26,430
about injecting melody with emotion or

33
00:01:24,690 --> 00:01:27,930
so that ultimately you leave home and

34
00:01:26,430 --> 00:01:30,180
you return home and you've learned

35
00:01:27,930 --> 00:01:31,890
something along the way yeah so a nice

36
00:01:30,180 --> 00:01:33,899
place to start is with the idea of a

37
00:01:31,890 --> 00:01:37,050
triad I've tried is a three-part

38
00:01:33,899 --> 00:01:38,399
harmonic basically so that's a trap you

39
00:01:37,050 --> 00:01:40,530
know so this try it's called C major

40
00:01:38,399 --> 00:01:41,910
alright so have you ever heard this idea

41
00:01:40,530 --> 00:01:46,470
of like major chords and minor chords

42
00:01:41,910 --> 00:01:49,710
yeah so this is C major yeah and then

43
00:01:46,470 --> 00:01:51,030
this is C C minor so the feelings are

44
00:01:49,710 --> 00:01:54,360
different right yeah I feels like

45
00:01:51,030 --> 00:01:58,380
darkness yeah how does this will make

46
00:01:54,360 --> 00:02:01,470
you feel them right yeah like yeah so in

47
00:01:58,380 --> 00:02:03,890
Amazing Grace you start with F you know

48
00:02:01,470 --> 00:02:03,890
if I go

49
00:02:09,099 --> 00:02:13,120
and my job is to get back home but to

50
00:02:11,499 --> 00:02:16,469
make it to make this cord make sense

51
00:02:13,120 --> 00:02:16,469
yeah so I'm like

52
00:02:24,660 --> 00:02:30,340
my job as a harmonizer is to find that

53
00:02:28,480 --> 00:02:31,870
narrative and make it make sense that

54
00:02:30,340 --> 00:02:33,819
was interesting because I didn't think

55
00:02:31,870 --> 00:02:34,989
that that would work together right

56
00:02:33,819 --> 00:02:36,730
because there are two completely

57
00:02:34,989 --> 00:02:43,780
different sounds but then like it just

58
00:02:36,730 --> 00:02:46,420
made it happen this is like essentially

59
00:02:43,780 --> 00:02:48,700
what harmony is is like a language right

60
00:02:46,420 --> 00:02:50,230
and so as with any language the more

61
00:02:48,700 --> 00:02:51,850
words you're capable of speaking in a

62
00:02:50,230 --> 00:02:54,010
language the more you can say all right

63
00:02:51,850 --> 00:02:55,930
so in harmony this might be how many

64
00:02:54,010 --> 00:02:57,370
notes you can think to add to a chord to

65
00:02:55,930 --> 00:02:58,930
make it feel a different way have you

66
00:02:57,370 --> 00:03:00,850
ever heard of the circle of fifths mmm

67
00:02:58,930 --> 00:03:02,200
okay that's great on one side we have a

68
00:03:00,850 --> 00:03:04,650
lot of the notes which make us feel

69
00:03:02,200 --> 00:03:07,330
brighter you know like these kinds of

70
00:03:04,650 --> 00:03:10,269
these rewrite sounds and the other side

71
00:03:07,330 --> 00:03:12,519
is a lot more to do with the darkness of

72
00:03:10,269 --> 00:03:14,230
a key Center so we're having F but

73
00:03:12,519 --> 00:03:16,330
imagine we're taking a quick visit to B

74
00:03:14,230 --> 00:03:18,190
flat but then the Sun comes out oh yeah

75
00:03:16,330 --> 00:03:22,150
yeah what notes would you suggest I add

76
00:03:18,190 --> 00:03:24,430
F F is a great one yeah and if we keep

77
00:03:22,150 --> 00:03:28,750
going in that direction yeah I see yeah

78
00:03:24,430 --> 00:03:30,820
and then G yeah yeah so this is really

79
00:03:28,750 --> 00:03:32,680
this is like a gleaming blade or

80
00:03:30,820 --> 00:03:34,570
something you know let's play this tune

81
00:03:32,680 --> 00:03:36,280
amazing grace let's play it in its

82
00:03:34,570 --> 00:03:39,329
simplest form yeah all right so we're

83
00:03:36,280 --> 00:03:39,329
gonna sign F right yes

84
00:03:55,489 --> 00:04:00,510
yeah nice okay cool let's try one more

85
00:03:58,140 --> 00:04:02,640
version will we ask some colors all

86
00:04:00,510 --> 00:04:04,349
right so let's visit the B flat with a

87
00:04:02,640 --> 00:04:06,650
bit more imagination this is how things

88
00:04:04,349 --> 00:04:12,599
feel yeah all right listen let's do it

89
00:04:06,650 --> 00:04:16,199
so it's fmaj7 but it's over a just f7

90
00:04:12,599 --> 00:04:19,620
over a b flat major seven with an a

91
00:04:16,199 --> 00:04:28,320
energy in it F major seven with an E and

92
00:04:19,620 --> 00:04:34,490
a DRG D minor 7 with the g-unit cease us

93
00:04:28,320 --> 00:04:39,949
for minor nice b flat major

94
00:04:34,490 --> 00:04:41,930
g7 that felt major 7 like that

95
00:04:39,949 --> 00:04:43,360
so what harmony does is just gives us

96
00:04:41,930 --> 00:04:50,509
even more tools to tell those stories

97
00:04:43,360 --> 00:04:52,099
yeah an awesome one nice emotional

98
00:04:50,509 --> 00:04:54,139
device when it comes to harmony is just

99
00:04:52,099 --> 00:04:55,430
think about how to arrive somewhere you

100
00:04:54,139 --> 00:04:56,569
know it can be so sparse it can be so

101
00:04:55,430 --> 00:04:58,610
rich and it can be really emotional so

102
00:04:56,569 --> 00:05:00,889
this idea of the overtone are understood

103
00:04:58,610 --> 00:05:03,139
in the harmonic series how much do you

104
00:05:00,889 --> 00:05:04,639
know about this only until I started

105
00:05:03,139 --> 00:05:07,099
listening to singing like barbershop

106
00:05:04,639 --> 00:05:09,229
quartet and and as I was a violinist as

107
00:05:07,099 --> 00:05:10,789
well then I finally understood that day

108
00:05:09,229 --> 00:05:12,889
where the overtone series came from that

109
00:05:10,789 --> 00:05:15,349
if a bunch of singers were to a nail a

110
00:05:12,889 --> 00:05:17,180
chord or have it perfectly tuned the

111
00:05:15,349 --> 00:05:19,039
overtones you would hear a tone that

112
00:05:17,180 --> 00:05:20,660
necessarily wasn't being produced by one

113
00:05:19,039 --> 00:05:22,370
of the chairs the amazing about harmony

114
00:05:20,660 --> 00:05:24,080
is that it exists in nature

115
00:05:22,370 --> 00:05:26,150
so take that morning series of the note

116
00:05:24,080 --> 00:05:29,750
F for example K you have the option in

117
00:05:26,150 --> 00:05:31,340
the 5th and the 4th and then that's a

118
00:05:29,750 --> 00:05:33,500
little bit sharper that's both and then

119
00:05:31,340 --> 00:05:35,720
the intervals get increasingly small

120
00:05:33,500 --> 00:05:37,190
beneath that note you have the under

121
00:05:35,720 --> 00:05:38,479
tone series which essentially is like a

122
00:05:37,190 --> 00:05:40,550
reflection or something in the same way

123
00:05:38,479 --> 00:05:41,840
that when a tree grows in nature you

124
00:05:40,550 --> 00:05:43,880
have the branches which go upwards in

125
00:05:41,840 --> 00:05:46,280
the roots which go downwards so it's

126
00:05:43,880 --> 00:05:48,919
quite a nice thing to think about this

127
00:05:46,280 --> 00:05:50,870
being the key Center that the floor the

128
00:05:48,919 --> 00:05:52,370
ground and then these two different

129
00:05:50,870 --> 00:05:54,320
directions of ways in which it can

130
00:05:52,370 --> 00:05:56,270
express itself and the difference is in

131
00:05:54,320 --> 00:05:57,139
the sensations with that lots of the

132
00:05:56,270 --> 00:05:59,270
time I think when it comes to

133
00:05:57,139 --> 00:06:01,070
reharmonization or harmonization people

134
00:05:59,270 --> 00:06:04,159
think that the solution to the problems

135
00:06:01,070 --> 00:06:06,050
come when we add more notes mm-hmm yeah

136
00:06:04,159 --> 00:06:07,520
I think that people forget that you can

137
00:06:06,050 --> 00:06:09,590
work with the notes already have by just

138
00:06:07,520 --> 00:06:11,690
rearranging them just the simple idea of

139
00:06:09,590 --> 00:06:15,969
inversion you know in version of the

140
00:06:11,690 --> 00:06:15,969
simple triad of F major so

141
00:06:17,249 --> 00:06:19,279
Oh

142
00:06:24,030 --> 00:06:28,320
see know how home do I want to go here

143
00:06:27,570 --> 00:06:29,910
you know

144
00:06:28,320 --> 00:06:31,500
is there another verse to come right

145
00:06:29,910 --> 00:06:33,450
because I can do I can delay the

146
00:06:31,500 --> 00:06:34,860
gratification of going home first of all

147
00:06:33,450 --> 00:06:36,750
just by using inversions even before we

148
00:06:34,860 --> 00:06:39,170
add notes to the chord one thing that I

149
00:06:36,750 --> 00:06:41,430
was very joyful to discover is that

150
00:06:39,170 --> 00:06:44,760
every single melody note works with

151
00:06:41,430 --> 00:06:46,770
every single bass note hmm so you

152
00:06:44,760 --> 00:06:51,630
demonstrate that yeah so this is the

153
00:06:46,770 --> 00:06:54,480
note there yeah F major F minor G flat

154
00:06:51,630 --> 00:06:57,150
major seven G flat diminished G flat

155
00:06:54,480 --> 00:07:02,820
minor major seven G minor seven cheese

156
00:06:57,150 --> 00:07:05,700
sauce g7 g7 sharp eleven and then g7

157
00:07:02,820 --> 00:07:07,980
with a flat 13 but a flat sauce flat

158
00:07:05,700 --> 00:07:10,740
major seven a flat major seven sharp

159
00:07:07,980 --> 00:07:12,120
five with a sharp 11 essentially what

160
00:07:10,740 --> 00:07:14,610
that demonstrates is that every note and

161
00:07:12,120 --> 00:07:16,860
every bass note are compatible so once

162
00:07:14,610 --> 00:07:19,380
we realize this it's like that's great

163
00:07:16,860 --> 00:07:20,910
now what should we do what should we

164
00:07:19,380 --> 00:07:22,740
watch what would what am I supposed to

165
00:07:20,910 --> 00:07:23,790
do right and sometimes the power lies

166
00:07:22,740 --> 00:07:25,410
anything when it comes to arranging

167
00:07:23,790 --> 00:07:27,030
there are too many options too many

168
00:07:25,410 --> 00:07:28,590
things are possible so that's when it

169
00:07:27,030 --> 00:07:30,600
becomes super super important to be

170
00:07:28,590 --> 00:07:32,370
aware of what you want to try and say

171
00:07:30,600 --> 00:07:35,010
emotionally you know what about negative

172
00:07:32,370 --> 00:07:36,510
harmony the dark side so essentially the

173
00:07:35,010 --> 00:07:38,100
way I'd apply negative harmony would be

174
00:07:36,510 --> 00:07:39,390
this idea of polarity you know between

175
00:07:38,100 --> 00:07:40,980
the undertones there is new overtone

176
00:07:39,390 --> 00:07:42,210
series or you know the one side the

177
00:07:40,980 --> 00:07:45,770
other side the perfect in the play go

178
00:07:42,210 --> 00:07:48,510
the feeling of a minor playful cadence

179
00:07:45,770 --> 00:07:49,650
resolving is so moving you know and it's

180
00:07:48,510 --> 00:07:53,400
just it's a good alternative to

181
00:07:49,650 --> 00:07:55,170
something like 22 you know what you

182
00:07:53,400 --> 00:07:57,000
doing that makes something in a major

183
00:07:55,170 --> 00:07:59,040
key sound the kind of a wistful sad song

184
00:07:57,000 --> 00:08:00,690
right you know I'll change the feeling

185
00:07:59,040 --> 00:08:01,680
of that what otherwise would you know if

186
00:08:00,690 --> 00:08:02,910
you would tell the kid that this is a

187
00:08:01,680 --> 00:08:05,820
major song should be happy

188
00:08:02,910 --> 00:08:07,740
exactly yeah no for sure an inner F

189
00:08:05,820 --> 00:08:10,080
major can be something you arrive in

190
00:08:07,740 --> 00:08:11,970
from if you arrive in F major from D

191
00:08:10,080 --> 00:08:13,440
flat yeah that is like the sun's come

192
00:08:11,970 --> 00:08:16,550
out right but if you arrive an F major

193
00:08:13,440 --> 00:08:16,550
from a major

194
00:08:16,690 --> 00:08:21,160
and it's like the sun's going in there's

195
00:08:19,630 --> 00:08:22,750
a lot about context I think once you

196
00:08:21,160 --> 00:08:24,340
have a language it's about using it and

197
00:08:22,750 --> 00:08:25,030
applying it and there's emotional ways I

198
00:08:24,340 --> 00:08:29,470
think that's what makes the difference

199
00:08:25,030 --> 00:08:31,300
all right a lot of people will see you

200
00:08:29,470 --> 00:08:32,560
as somebody who's drank in harmony their

201
00:08:31,300 --> 00:08:34,450
whole life they've seen so much harmony

202
00:08:32,560 --> 00:08:36,039
heard so much harmony how do you make

203
00:08:34,450 --> 00:08:38,740
the choices there's so much that's

204
00:08:36,039 --> 00:08:40,510
possible when you know stuff how do you

205
00:08:38,740 --> 00:08:43,270
how do you have the courage to make a

206
00:08:40,510 --> 00:08:46,690
choice it comes from just your life

207
00:08:43,270 --> 00:08:49,510
experience mm-hmm and and that moves you

208
00:08:46,690 --> 00:08:54,070
in a certain direction how it gets

209
00:08:49,510 --> 00:08:57,490
expressed many times it's a complete

210
00:08:54,070 --> 00:08:59,860
surprise I find it fascinating in music

211
00:08:57,490 --> 00:09:02,230
to think about you know say I arrive in

212
00:08:59,860 --> 00:09:04,600
a place if I'm going I'm going to D flat

213
00:09:02,230 --> 00:09:06,040
major and there was something you once

214
00:09:04,600 --> 00:09:08,050
taught me you taught me this song called

215
00:09:06,040 --> 00:09:10,090
don't follow the crowd oh yeah and

216
00:09:08,050 --> 00:09:11,890
there's that chord it's not a dominant

217
00:09:10,090 --> 00:09:13,480
chord but it functions as a dominant

218
00:09:11,890 --> 00:09:14,560
chord and it doesn't matter because

219
00:09:13,480 --> 00:09:16,420
you're still going to the place you're

220
00:09:14,560 --> 00:09:19,020
going to right right right it's

221
00:09:16,420 --> 00:09:19,020
something like

222
00:09:22,510 --> 00:09:31,190
right right so this called being like a

223
00:09:26,810 --> 00:09:33,830
major 7 with a Saab 5 and a natural like

224
00:09:31,190 --> 00:09:38,120
there's no dominant there but at that

225
00:09:33,830 --> 00:09:40,700
moment I'm coming from here and I want

226
00:09:38,120 --> 00:09:50,360
to afford some some solution I get

227
00:09:40,700 --> 00:09:54,050
myself in a situation that haunted me

228
00:09:50,360 --> 00:09:56,030
for days you know I just wouldn't think

229
00:09:54,050 --> 00:09:58,070
to use that cord in that situation yeah

230
00:09:56,030 --> 00:09:59,690
and there it was and yeah you know if

231
00:09:58,070 --> 00:10:01,490
you read the rulebook it's that's not in

232
00:09:59,690 --> 00:10:03,560
it yeah and it this is what I learned

233
00:10:01,490 --> 00:10:05,660
from the great person Sanderson yeah

234
00:10:03,560 --> 00:10:09,440
yeah yeah the idea of going to D flat

235
00:10:05,660 --> 00:10:15,050
chord normally there's a dominant black

236
00:10:09,440 --> 00:10:19,000
day flat 7 we're back in the twenties

237
00:10:15,050 --> 00:10:19,000
they also you used to do things like

238
00:10:19,180 --> 00:10:24,520
yeah

239
00:10:21,190 --> 00:10:24,520
so it's come

240
00:10:25,750 --> 00:10:33,540
coming from just just below the keep you

241
00:10:29,770 --> 00:10:33,540
from c-27

242
00:10:33,589 --> 00:10:42,499
and we don't normally say a c7 and and a

243
00:10:38,180 --> 00:10:44,180
flat seven all right I related yeah you

244
00:10:42,499 --> 00:10:56,240
know because some of that so many of the

245
00:10:44,180 --> 00:10:58,129
notes are different it's not even but if

246
00:10:56,240 --> 00:11:00,379
you've always that correctly oh yeah

247
00:10:58,129 --> 00:11:02,269
then you're safe you can get all you can

248
00:11:00,379 --> 00:11:05,300
get away with it yeah

249
00:11:02,269 --> 00:11:09,559
the other thing that I like that

250
00:11:05,300 --> 00:11:13,339
we both do on occasion is to be on the

251
00:11:09,559 --> 00:11:17,959
court we want to arrive at with the

252
00:11:13,339 --> 00:11:19,970
bottom part of our structure and accord

253
00:11:17,959 --> 00:11:28,389
before the arrival court to have that on

254
00:11:19,970 --> 00:11:28,389
top yeah oh yeah yeah yeah

255
00:11:34,589 --> 00:11:38,560
and that's not because like emotionally

256
00:11:36,670 --> 00:11:43,320
it's almost like I'm here I'm arriving

257
00:11:38,560 --> 00:11:48,610
here but if the base new is the same

258
00:11:43,320 --> 00:11:52,390
it's like this inevitability about it

259
00:11:48,610 --> 00:11:53,529
makes a pool yeah yeah it's like one

260
00:11:52,390 --> 00:11:54,880
thing is moving in one direction one

261
00:11:53,529 --> 00:11:57,160
thing has arrived so it's this tension

262
00:11:54,880 --> 00:11:58,600
and it's right glorious I love thinking

263
00:11:57,160 --> 00:12:00,339
like these things emotionally just

264
00:11:58,600 --> 00:12:01,990
because that's a feeling you I know that

265
00:12:00,339 --> 00:12:04,540
feeling in my life music isn't that

266
00:12:01,990 --> 00:12:06,519
different from life no you know and I

267
00:12:04,540 --> 00:12:09,339
think that's probably the greatest

268
00:12:06,519 --> 00:12:10,120
attraction to those of us who play music

269
00:12:09,339 --> 00:12:11,529
yeah

270
00:12:10,120 --> 00:12:13,390
was there ever a point in your life when

271
00:12:11,529 --> 00:12:15,459
you were younger where you felt like you

272
00:12:13,390 --> 00:12:18,130
had consistently fell back into the same

273
00:12:15,459 --> 00:12:20,440
habits you'd finally I'm not gonna play

274
00:12:18,130 --> 00:12:21,970
the same thing again I had a really

275
00:12:20,440 --> 00:12:24,540
great experience when I was working with

276
00:12:21,970 --> 00:12:27,820
Miles Davis I felt like I was in a rut

277
00:12:24,540 --> 00:12:30,370
playing the same stuff and I was getting

278
00:12:27,820 --> 00:12:32,709
depressed because of it and Miles said

279
00:12:30,370 --> 00:12:35,529
something to me I thought he said don't

280
00:12:32,709 --> 00:12:37,320
play the butter notes right and so I

281
00:12:35,529 --> 00:12:40,270
thought what do you mean by that

282
00:12:37,320 --> 00:12:41,800
so you thought it said butter butter no

283
00:12:40,270 --> 00:12:44,950
he said bottom but you thought he said

284
00:12:41,800 --> 00:12:47,560
butter yeah Wow inside sad thing oh quit

285
00:12:44,950 --> 00:12:49,300
what quit butter be what is better then

286
00:12:47,560 --> 00:12:51,579
I said thing what are the obvious notes

287
00:12:49,300 --> 00:12:54,430
that for example in a chord the obvious

288
00:12:51,579 --> 00:12:56,380
notes are the third in the seventh hmm

289
00:12:54,430 --> 00:12:58,779
so I said oh maybe if I leave those out

290
00:12:56,380 --> 00:13:03,100
it changed everything for me from that

291
00:12:58,779 --> 00:13:05,940
moment on I got more applause than for

292
00:13:03,100 --> 00:13:09,220
that song than I did the whole week

293
00:13:05,940 --> 00:13:12,430
well I wouldn't played the voicings I

294
00:13:09,220 --> 00:13:14,860
play today if that had not happened

295
00:13:12,430 --> 00:13:18,070
that's amazing you know I've been using

296
00:13:14,860 --> 00:13:19,270
this tune amazing grace oh and so we

297
00:13:18,070 --> 00:13:21,640
could play a bit of that if you want to

298
00:13:19,270 --> 00:13:24,120
okay we could talk about some stuff I'll

299
00:13:21,640 --> 00:13:24,120
be doing an F

300
00:15:38,839 --> 00:15:41,959
thank you

