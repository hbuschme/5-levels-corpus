1
00:00:00,000 --> 00:00:04,470
hi I'm John Carmack I'm the chief

2
00:00:01,770 --> 00:00:06,509
technology officer for oculus I work on

3
00:00:04,470 --> 00:00:08,970
virtual reality I've been challenged

4
00:00:06,509 --> 00:00:11,429
today to talk about one concept at five

5
00:00:08,970 --> 00:00:12,900
levels of increasing complexity so we're

6
00:00:11,429 --> 00:00:14,880
gonna be talking about reality and

7
00:00:12,900 --> 00:00:17,550
virtual reality what the technology

8
00:00:14,880 --> 00:00:19,560
allows us to do today what it may allow

9
00:00:17,550 --> 00:00:21,480
us to do in the future and whether that

10
00:00:19,560 --> 00:00:25,320
should even be our goal to approximate

11
00:00:21,480 --> 00:00:28,830
reality so do you know what virtual

12
00:00:25,320 --> 00:00:31,980
reality is yes it's simple it's vert

13
00:00:28,830 --> 00:00:33,780
it's like a video game except it feels

14
00:00:31,980 --> 00:00:35,670
like you're into video games that's

15
00:00:33,780 --> 00:00:38,430
actually a really good description the

16
00:00:35,670 --> 00:00:40,739
idea is that if you've got a system here

17
00:00:38,430 --> 00:00:42,450
that can make you see whatever we want

18
00:00:40,739 --> 00:00:44,850
you to see then we can make you believe

19
00:00:42,450 --> 00:00:46,680
that you're anywhere else like on top of

20
00:00:44,850 --> 00:00:49,379
a mountain or in a dungeon or under the

21
00:00:46,680 --> 00:00:51,270
ocean or in Minecraft yeah or in

22
00:00:49,379 --> 00:00:53,550
Minecraft when you look at an TV on the

23
00:00:51,270 --> 00:00:55,770
wall there he's showing like a picture

24
00:00:53,550 --> 00:00:57,420
of a mountain or something how can you

25
00:00:55,770 --> 00:00:59,070
tell that it's not just a window and

26
00:00:57,420 --> 00:01:01,770
there's something else behind it because

27
00:00:59,070 --> 00:01:04,890
it always doesn't look quite right if

28
00:01:01,770 --> 00:01:07,409
you have a static picture of a person on

29
00:01:04,890 --> 00:01:10,170
a screen and you move around like this

30
00:01:07,409 --> 00:01:11,520
it's not really changing and it's

31
00:01:10,170 --> 00:01:13,560
interesting those are things that we

32
00:01:11,520 --> 00:01:15,390
have to figure out the ways around it to

33
00:01:13,560 --> 00:01:17,369
fool you in virtual reality we need to

34
00:01:15,390 --> 00:01:19,350
figure out when you look at something in

35
00:01:17,369 --> 00:01:21,600
reality how can you tell whether it's

36
00:01:19,350 --> 00:01:23,520
real or not have you been to a 3d movie

37
00:01:21,600 --> 00:01:26,759
where you put on the little glasses and

38
00:01:23,520 --> 00:01:29,310
um yeah so what they do the trick for

39
00:01:26,759 --> 00:01:31,020
that is if you ever add a theater and

40
00:01:29,310 --> 00:01:32,549
you take up the glasses and you look at

41
00:01:31,020 --> 00:01:34,020
it you'll see it's blurry where there's

42
00:01:32,549 --> 00:01:36,299
actually two pictures that they're

43
00:01:34,020 --> 00:01:39,840
showing at the same time and what those

44
00:01:36,299 --> 00:01:41,880
little glasses do is they like one I see

45
00:01:39,840 --> 00:01:44,189
one picture and the other I see a

46
00:01:41,880 --> 00:01:45,869
different picture so then your eyes can

47
00:01:44,189 --> 00:01:48,090
say oh it looks like I'm seeing right

48
00:01:45,869 --> 00:01:50,310
through the screen or something as

49
00:01:48,090 --> 00:01:52,619
floating out in front of it in the VR

50
00:01:50,310 --> 00:01:55,380
headsets what we do is there's actually

51
00:01:52,619 --> 00:01:57,689
either two screens or one screen split

52
00:01:55,380 --> 00:01:59,159
in half so that it draws a different

53
00:01:57,689 --> 00:02:01,829
picture a completely different picture

54
00:01:59,159 --> 00:02:04,020
for each eye and we make sure that each

55
00:02:01,829 --> 00:02:06,000
eye can only see the picture it intended

56
00:02:04,020 --> 00:02:07,829
to and that's what can make things feel

57
00:02:06,000 --> 00:02:09,209
like they've got this real depth to them

58
00:02:07,829 --> 00:02:11,220
that it's something that you could reach

59
00:02:09,209 --> 00:02:12,950
out and touch and it doesn't feel like a

60
00:02:11,220 --> 00:02:14,379
flat TV screen

61
00:02:12,950 --> 00:02:17,660
[Music]

62
00:02:14,379 --> 00:02:20,000
try and look I look over there and

63
00:02:17,660 --> 00:02:21,230
concentrate on his face over there can

64
00:02:20,000 --> 00:02:24,349
you see me waving my hand without

65
00:02:21,230 --> 00:02:25,910
turning your eyes no all right so at

66
00:02:24,349 --> 00:02:26,780
some point you can probably see it right

67
00:02:25,910 --> 00:02:29,269
now

68
00:02:26,780 --> 00:02:31,250
all right so without moving your eyes

69
00:02:29,269 --> 00:02:36,739
this is kind of hard tell me how many

70
00:02:31,250 --> 00:02:39,380
fingers I'm holding up there's barely

71
00:02:36,739 --> 00:02:40,519
tell hmm and you see that even in like

72
00:02:39,380 --> 00:02:43,310
if you hold out your hand

73
00:02:40,519 --> 00:02:45,200
and you focus on your hand then your

74
00:02:43,310 --> 00:02:46,700
foot would be blurry because there's

75
00:02:45,200 --> 00:02:48,410
that difference there and you can change

76
00:02:46,700 --> 00:02:50,209
between that like you can then focus on

77
00:02:48,410 --> 00:02:52,640
your foot and your hand gets blurry

78
00:02:50,209 --> 00:02:55,549
because your eyes can see different

79
00:02:52,640 --> 00:02:57,620
amounts of detail in different places so

80
00:02:55,549 --> 00:02:59,329
we're hoping in the future that hardware

81
00:02:57,620 --> 00:03:02,150
can be like that where we can make a

82
00:02:59,329 --> 00:03:03,769
display that puts lots of detail right

83
00:03:02,150 --> 00:03:06,230
where you're looking and every time you

84
00:03:03,769 --> 00:03:08,090
look someplace else it moves the detail

85
00:03:06,230 --> 00:03:10,519
over to there so we don't need a render

86
00:03:08,090 --> 00:03:12,230
a hundred times as much as we've got

87
00:03:10,519 --> 00:03:14,709
right now figuring out where you're

88
00:03:12,230 --> 00:03:17,660
looking is a pretty hard problem what we

89
00:03:14,709 --> 00:03:20,389
try to go about this is by taking a

90
00:03:17,660 --> 00:03:22,250
camera and looking at people's eyes and

91
00:03:20,389 --> 00:03:24,290
then try to figure out is the eye

92
00:03:22,250 --> 00:03:25,940
looking over here or up here and we're

93
00:03:24,290 --> 00:03:29,750
working hard on stuff like this right

94
00:03:25,940 --> 00:03:32,209
now so we hopefully can have a virtual

95
00:03:29,750 --> 00:03:34,069
reality that's as detailed and realistic

96
00:03:32,209 --> 00:03:36,200
feeling as the reality that we've

97
00:03:34,069 --> 00:03:38,870
actually got around us but it's gonna be

98
00:03:36,200 --> 00:03:43,370
a long time before we get to I you know

99
00:03:38,870 --> 00:03:45,049
where we can really fool people so do

100
00:03:43,370 --> 00:03:46,760
you have a basic sense of what latency

101
00:03:45,049 --> 00:03:48,440
is yeah so my understanding of what

102
00:03:46,760 --> 00:03:51,470
latency is is it's it's basically the

103
00:03:48,440 --> 00:03:53,150
time delay between the rendering at

104
00:03:51,470 --> 00:03:55,250
different points so it's basically a

105
00:03:53,150 --> 00:03:57,590
delay and it happens in all parts of the

106
00:03:55,250 --> 00:03:59,900
system monitors can be a big one like

107
00:03:57,590 --> 00:04:01,730
you know consumer televisions can often

108
00:03:59,900 --> 00:04:04,970
have 50 milliseconds or more of latency

109
00:04:01,730 --> 00:04:06,530
just in the TV part and then you've got

110
00:04:04,970 --> 00:04:09,590
the processing in the computer and all

111
00:04:06,530 --> 00:04:11,239
of these add up to the total latency the

112
00:04:09,590 --> 00:04:13,130
latency is in my opinion the most

113
00:04:11,239 --> 00:04:14,930
important part of the AR because if you

114
00:04:13,130 --> 00:04:17,239
have that offset your body is no longer

115
00:04:14,930 --> 00:04:18,889
immersed and you get you gain that

116
00:04:17,239 --> 00:04:20,810
motion sickness which can pull a lot of

117
00:04:18,889 --> 00:04:22,970
people out of the experience games that

118
00:04:20,810 --> 00:04:25,159
feel really good they've got that sense

119
00:04:22,970 --> 00:04:26,650
of it happens instantly when you're you

120
00:04:25,159 --> 00:04:29,020
know when you're doing something

121
00:04:26,650 --> 00:04:31,660
it really is a testament to this kind of

122
00:04:29,020 --> 00:04:33,250
technology and the how it's developing

123
00:04:31,660 --> 00:04:36,430
and how over time it's gonna just be

124
00:04:33,250 --> 00:04:38,080
you're gonna be able to pack more pixel

125
00:04:36,430 --> 00:04:40,090
density in this displays and it's gonna

126
00:04:38,080 --> 00:04:43,330
get a lot more immersive like 30 years

127
00:04:40,090 --> 00:04:45,190
ago you had desktop PCs which were you

128
00:04:43,330 --> 00:04:46,600
spent whatever on that but there was

129
00:04:45,190 --> 00:04:48,400
always this idea well you could spend a

130
00:04:46,600 --> 00:04:50,050
million dollars and buy a supercomputer

131
00:04:48,400 --> 00:04:52,150
and it's gonna be a lot faster and

132
00:04:50,050 --> 00:04:53,919
that's not really true today for scalar

133
00:04:52,150 --> 00:04:55,840
processing when you just do one thing

134
00:04:53,919 --> 00:04:58,840
after another okay a high-end

135
00:04:55,840 --> 00:05:00,520
overclocked cooled gaming PC is about

136
00:04:58,840 --> 00:05:02,800
the fastest thing in the world it is

137
00:05:00,520 --> 00:05:04,389
within a very small Delta yeah for some

138
00:05:02,800 --> 00:05:06,490
things you'll get some power from you

139
00:05:04,389 --> 00:05:09,550
IBM power system that might be a little

140
00:05:06,490 --> 00:05:10,870
bit faster but not a whole lot so if I'm

141
00:05:09,550 --> 00:05:12,940
looking at this and saying we need to be

142
00:05:10,870 --> 00:05:14,680
five times faster you know what do you

143
00:05:12,940 --> 00:05:16,539
do you can't just say make each thing

144
00:05:14,680 --> 00:05:18,340
faster you're bait as a developer you're

145
00:05:16,539 --> 00:05:20,110
making a trade-off I can put my effort

146
00:05:18,340 --> 00:05:22,419
into making this more efficient or

147
00:05:20,110 --> 00:05:24,430
making it more fun and more fun usually

148
00:05:22,419 --> 00:05:26,410
wins out for very good reasons so

149
00:05:24,430 --> 00:05:28,539
there's some good judgment and trickery

150
00:05:26,410 --> 00:05:30,160
that goes into the design of things you

151
00:05:28,539 --> 00:05:32,740
can always design a game that will just

152
00:05:30,160 --> 00:05:35,590
not work well I am I mean in the old

153
00:05:32,740 --> 00:05:37,510
days games had to be so precisely

154
00:05:35,590 --> 00:05:39,820
designed and nowadays you've got a ton

155
00:05:37,510 --> 00:05:42,400
more freedom but you really can't just

156
00:05:39,820 --> 00:05:44,110
the hardware old games are running off

157
00:05:42,400 --> 00:05:46,449
and they fit and that's all the days you

158
00:05:44,110 --> 00:05:47,919
could have yeah any crazy idea you know

159
00:05:46,449 --> 00:05:49,659
you could probably make a pretty good

160
00:05:47,919 --> 00:05:51,280
video game which is a wonderful

161
00:05:49,659 --> 00:05:53,320
wonderful thing yeah a lot of freedom

162
00:05:51,280 --> 00:05:54,520
but VR makes you have to give up a

163
00:05:53,320 --> 00:05:57,669
little bit of that freedom you don't

164
00:05:54,520 --> 00:05:59,289
know not do so many crazy things so that

165
00:05:57,669 --> 00:06:01,659
you could wind up having it be as

166
00:05:59,289 --> 00:06:05,340
responsive and high-quality as it needs

167
00:06:01,659 --> 00:06:05,340
to be mm-hmm that's very interesting

168
00:06:06,280 --> 00:06:10,070
you know an interesting topic is what

169
00:06:08,600 --> 00:06:12,259
are the limits to what we can do with

170
00:06:10,070 --> 00:06:14,419
virtual reality where I'm pretty pleased

171
00:06:12,259 --> 00:06:16,610
with what we have today what we can show

172
00:06:14,419 --> 00:06:18,590
people and say virtual reality it's cool

173
00:06:16,610 --> 00:06:20,870
people get an amazing response from it

174
00:06:18,590 --> 00:06:23,300
but we're still clearly a very very long

175
00:06:20,870 --> 00:06:24,860
ways from reality that kind of notes

176
00:06:23,300 --> 00:06:26,630
back to realism in our history and how

177
00:06:24,860 --> 00:06:28,789
realism was response to romanticism and

178
00:06:26,630 --> 00:06:30,919
realism was meant to capture the mundane

179
00:06:28,789 --> 00:06:33,320
everyday lives of you know individuals

180
00:06:30,919 --> 00:06:34,340
and not idealize any of their activities

181
00:06:33,320 --> 00:06:35,570
or any way and I think that that's

182
00:06:34,340 --> 00:06:36,560
really important for virtual Rhianna I

183
00:06:35,570 --> 00:06:38,000
think it's kind of like a rite of

184
00:06:36,560 --> 00:06:40,039
passage for any kind of our technology

185
00:06:38,000 --> 00:06:42,680
to go through mostly in VR we talk about

186
00:06:40,039 --> 00:06:44,539
the display and optics the visual side

187
00:06:42,680 --> 00:06:48,229
of things but we should at least take

188
00:06:44,539 --> 00:06:50,210
off the other senses and haptics is an

189
00:06:48,229 --> 00:06:52,220
interesting thing about virtual reality

190
00:06:50,210 --> 00:06:54,139
really doesn't have that aspect of

191
00:06:52,220 --> 00:06:55,729
touching things you can move your hands

192
00:06:54,139 --> 00:06:58,039
around you could do everything but it's

193
00:06:55,729 --> 00:06:59,840
a disconnected experience because you

194
00:06:58,039 --> 00:07:03,110
know you don't have you know the actual

195
00:06:59,840 --> 00:07:05,750
solidity there and I am pessimistic

196
00:07:03,110 --> 00:07:07,729
about progress and haptics technology

197
00:07:05,750 --> 00:07:09,650
about almost all other areas I'm an

198
00:07:07,729 --> 00:07:12,770
optimist I'm excited about what's coming

199
00:07:09,650 --> 00:07:14,020
up but I don't have any brilliant vision

200
00:07:12,770 --> 00:07:16,130
about how we're going to revolutionize

201
00:07:14,020 --> 00:07:17,630
haptics they make it feel like we're

202
00:07:16,130 --> 00:07:20,150
touching the things in the virtual world

203
00:07:17,630 --> 00:07:21,949
so I've you know tried the demos where

204
00:07:20,150 --> 00:07:23,630
there's like VRLA there's one that has

205
00:07:21,949 --> 00:07:24,740
waves like audio waves I believe that

206
00:07:23,630 --> 00:07:25,789
come up and then you can put your hands

207
00:07:24,740 --> 00:07:26,720
through that and feel the way is

208
00:07:25,789 --> 00:07:28,370
whenever you're supposed to be feeling

209
00:07:26,720 --> 00:07:29,449
bubbles or any kind of force note or

210
00:07:28,370 --> 00:07:31,039
something those are pretty interesting

211
00:07:29,449 --> 00:07:32,150
I've seen some pretty interesting things

212
00:07:31,039 --> 00:07:34,310
that you could do with audios you can

213
00:07:32,150 --> 00:07:35,330
cut down a lot of the storage I guess in

214
00:07:34,310 --> 00:07:37,009
the power that you would need in order

215
00:07:35,330 --> 00:07:38,360
to power huge scene you can just you

216
00:07:37,009 --> 00:07:39,349
know mimic the sounds of those scenes

217
00:07:38,360 --> 00:07:41,300
actually being there and then not

218
00:07:39,349 --> 00:07:43,639
actually building out for example the

219
00:07:41,300 --> 00:07:45,440
professor at USC would have the sound of

220
00:07:43,639 --> 00:07:46,880
a train drive by without a very actually

221
00:07:45,440 --> 00:07:48,080
rendering the sound and you'd feel like

222
00:07:46,880 --> 00:07:50,659
you're deeply immersed in this world

223
00:07:48,080 --> 00:07:52,099
without having to have such an expensive

224
00:07:50,659 --> 00:07:53,750
scene built around you so I think those

225
00:07:52,099 --> 00:07:55,759
are pretty you know significant and that

226
00:07:53,750 --> 00:07:57,560
is one potential quality improvement

227
00:07:55,759 --> 00:08:00,320
that's still on the horizon is when we

228
00:07:57,560 --> 00:08:02,180
do spatialization we use the HRT of the

229
00:08:00,320 --> 00:08:03,860
head relative transfer function to make

230
00:08:02,180 --> 00:08:06,259
it sound like it's in different places

231
00:08:03,860 --> 00:08:09,380
but usually we just use this one kind of

232
00:08:06,259 --> 00:08:12,590
generic here is your average human HRT F

233
00:08:09,380 --> 00:08:14,029
function and it's possible that of

234
00:08:12,590 --> 00:08:15,320
course if you are right in the average

235
00:08:14,029 --> 00:08:17,210
then it's perfect for you but there's

236
00:08:15,320 --> 00:08:18,020
always people off to the extremes that

237
00:08:17,210 --> 00:08:20,060
it does

238
00:08:18,020 --> 00:08:22,280
do a very good job at and there may be

239
00:08:20,060 --> 00:08:24,650
better ways to allow people to sample

240
00:08:22,280 --> 00:08:26,870
their own perfect H RTF which can

241
00:08:24,650 --> 00:08:28,280
improve the audio experiences a lot it

242
00:08:26,870 --> 00:08:30,650
all comes down to all these trade-offs

243
00:08:28,280 --> 00:08:32,090
you know with display and with

244
00:08:30,650 --> 00:08:34,070
resolution it's one of those things

245
00:08:32,090 --> 00:08:35,780
where if people have one bad experience

246
00:08:34,070 --> 00:08:38,360
they kind of out rule everything else is

247
00:08:35,780 --> 00:08:40,640
really difficult to build trust again

248
00:08:38,360 --> 00:08:42,200
with people who haven't done beer before

249
00:08:40,640 --> 00:08:43,760
but it's easy to break all that trust

250
00:08:42,200 --> 00:08:45,890
whether they do it was a huge concern

251
00:08:43,760 --> 00:08:47,300
about that at oculus and the term

252
00:08:45,890 --> 00:08:49,460
internally that went around was

253
00:08:47,300 --> 00:08:51,320
poisoning the well they were very very

254
00:08:49,460 --> 00:08:52,910
concerned I mean for a long time there

255
00:08:51,320 --> 00:08:55,010
was a fight about whether gear VR should

256
00:08:52,910 --> 00:08:57,290
even be done because the worry was that

257
00:08:55,010 --> 00:08:59,750
if we let a product go out like year VR

258
00:08:57,290 --> 00:09:02,000
that didn't have those things that if

259
00:08:59,750 --> 00:09:04,550
somebody saw it and he was bad it made

260
00:09:02,000 --> 00:09:06,020
them sick I made their eyes hurt then

261
00:09:04,550 --> 00:09:07,880
they'd be like I'm never gonna try VR

262
00:09:06,020 --> 00:09:10,310
again I tried at that time and it was

263
00:09:07,880 --> 00:09:13,040
you know it was terrible and there was

264
00:09:10,310 --> 00:09:16,190
legitimate arguments about whether it

265
00:09:13,040 --> 00:09:17,840
was even a good idea to do that and it

266
00:09:16,190 --> 00:09:19,340
turned out that yes it's obviously

267
00:09:17,840 --> 00:09:20,570
better to have all of those things but

268
00:09:19,340 --> 00:09:22,910
you can still do something that's

269
00:09:20,570 --> 00:09:25,460
valuable for the user without it it's

270
00:09:22,910 --> 00:09:27,620
weird being at the beginning of a medium

271
00:09:25,460 --> 00:09:30,920
like this right I'm very excited to see

272
00:09:27,620 --> 00:09:32,360
how filmmakers tackle kind of creating

273
00:09:30,920 --> 00:09:33,530
content in those things especially if

274
00:09:32,360 --> 00:09:35,050
there are experiences of traditional

275
00:09:33,530 --> 00:09:37,190
medium

276
00:09:35,050 --> 00:09:39,740
mostly today I've been talking a lot

277
00:09:37,190 --> 00:09:41,720
about what can we do what's possible we

278
00:09:39,740 --> 00:09:43,400
think might be possible in the next

279
00:09:41,720 --> 00:09:44,930
couple years but really at the

280
00:09:43,400 --> 00:09:46,580
professional level it's more the

281
00:09:44,930 --> 00:09:47,840
question of wisdom of what should we be

282
00:09:46,580 --> 00:09:49,490
doing I mean that's what I think we're

283
00:09:47,840 --> 00:09:51,470
trying to figure out is from artists and

284
00:09:49,490 --> 00:09:52,610
storage or storytelling perspective kind

285
00:09:51,470 --> 00:09:54,560
of what are the things that will make

286
00:09:52,610 --> 00:09:56,120
this meaningfully different from what

287
00:09:54,560 --> 00:09:58,520
we're used to like a television or a

288
00:09:56,120 --> 00:10:00,500
wall and we've been finding a lot of

289
00:09:58,520 --> 00:10:01,850
things that aspects of virtual reality

290
00:10:00,500 --> 00:10:03,920
that very much do that in my opinion

291
00:10:01,850 --> 00:10:05,270
things that allow you to feel presence

292
00:10:03,920 --> 00:10:06,740
first and foremost where you get lost

293
00:10:05,270 --> 00:10:08,240
you have to remind yourself this isn't

294
00:10:06,740 --> 00:10:10,040
actually happening and things that

295
00:10:08,240 --> 00:10:11,660
ultimately allow you to embody other

296
00:10:10,040 --> 00:10:13,640
characters things that you can actually

297
00:10:11,660 --> 00:10:15,020
change your own self-perception and play

298
00:10:13,640 --> 00:10:17,840
with neuroplasticity and teach yourself

299
00:10:15,020 --> 00:10:19,610
things that are bizarre and unique as an

300
00:10:17,840 --> 00:10:21,800
engineer of course I love quantifiable

301
00:10:19,610 --> 00:10:23,750
things I like saying here's my 18

302
00:10:21,800 --> 00:10:25,760
millisecond motion to photon here's my

303
00:10:23,750 --> 00:10:28,400
angular resolution that I'm improving

304
00:10:25,760 --> 00:10:30,080
I'm doing the color space right but you

305
00:10:28,400 --> 00:10:31,350
can look at you know not too far back

306
00:10:30,080 --> 00:10:34,560
where you say we have

307
00:10:31,350 --> 00:10:36,540
DVDs at this amazing resolution but more

308
00:10:34,560 --> 00:10:38,820
people want to watch YouTube videos at

309
00:10:36,540 --> 00:10:40,830
really bad early internet video speeds

310
00:10:38,820 --> 00:10:42,720
where there are things that if you

311
00:10:40,830 --> 00:10:45,120
deliver a value to people then these

312
00:10:42,720 --> 00:10:46,860
objective quantities may not be the most

313
00:10:45,120 --> 00:10:48,750
important thing and well I'm certainly

314
00:10:46,860 --> 00:10:50,490
pushing as hard as we can on lots of

315
00:10:48,750 --> 00:10:53,130
these things that make the experience

316
00:10:50,490 --> 00:10:54,780
better in potentially every way or maybe

317
00:10:53,130 --> 00:10:56,940
just for videos or the different things

318
00:10:54,780 --> 00:10:59,580
I don't think that it's necessary

319
00:10:56,940 --> 00:11:01,290
I've commented that I think usually my

320
00:10:59,580 --> 00:11:03,120
favorite titles on mobile they're fully

321
00:11:01,290 --> 00:11:04,890
synthetic are ones that don't even try

322
00:11:03,120 --> 00:11:06,900
they just go and do like light mapped

323
00:11:04,890 --> 00:11:09,300
flat shaded and I think it's a lovely

324
00:11:06,900 --> 00:11:11,460
aesthetic I think that you don't wind up

325
00:11:09,300 --> 00:11:12,840
fighting all of the aliasing while you

326
00:11:11,460 --> 00:11:14,580
get some other titles and I we're gonna

327
00:11:12,840 --> 00:11:16,620
be high tech with our you know our

328
00:11:14,580 --> 00:11:18,150
specular bump maps with roughness and

329
00:11:16,620 --> 00:11:19,470
you've got aliasing everywhere and you

330
00:11:18,150 --> 00:11:21,420
can't hold framerate and it's all

331
00:11:19,470 --> 00:11:24,630
problematic while some of these that are

332
00:11:21,420 --> 00:11:27,150
clearly you know very synthetic worlds

333
00:11:24,630 --> 00:11:29,040
where I it's nothing but these cartoony

334
00:11:27,150 --> 00:11:30,720
flat shaded things with lighting but

335
00:11:29,040 --> 00:11:32,610
they look and they feel good and you can

336
00:11:30,720 --> 00:11:34,290
buy that you're in that place and we

337
00:11:32,610 --> 00:11:36,330
want to know what's around that monolith

338
00:11:34,290 --> 00:11:37,890
over there we did a project called life

339
00:11:36,330 --> 00:11:39,960
of us which is exactly that mindset

340
00:11:37,890 --> 00:11:42,030
where let's embrace low poly aesthetic

341
00:11:39,960 --> 00:11:43,380
and just simple vertex shading and we

342
00:11:42,030 --> 00:11:44,940
ended up you know realizing you can

343
00:11:43,380 --> 00:11:46,620
embody these two various creatures and

344
00:11:44,940 --> 00:11:47,970
transform yourself and when you do that

345
00:11:46,620 --> 00:11:49,410
with co-presence of another creature

346
00:11:47,970 --> 00:11:51,330
another human

347
00:11:49,410 --> 00:11:53,280
it makes for a totally magical journey

348
00:11:51,330 --> 00:11:54,630
and you don't even think for a second

349
00:11:53,280 --> 00:11:56,220
you actually dismiss the whole idea of

350
00:11:54,630 --> 00:11:58,680
photorealism and embrace that reality

351
00:11:56,220 --> 00:12:00,420
for it is I think it actually helps put

352
00:11:58,680 --> 00:12:02,190
you at ease a little bit the end goal of

353
00:12:00,420 --> 00:12:04,410
reality of course in computer graphics

354
00:12:02,190 --> 00:12:05,910
people have chased photorealistic form

355
00:12:04,410 --> 00:12:08,010
you know for a long time and basically

356
00:12:05,910 --> 00:12:09,690
we've we've achieved it photo realism if

357
00:12:08,010 --> 00:12:11,670
you're willing to throw enough you know

358
00:12:09,690 --> 00:12:13,410
discrete path trace trees and things you

359
00:12:11,670 --> 00:12:15,690
can generate photorealistic views you

360
00:12:13,410 --> 00:12:17,280
understand the light really well of

361
00:12:15,690 --> 00:12:19,170
course it still takes a half hour per

362
00:12:17,280 --> 00:12:20,970
frame like it always has or more to

363
00:12:19,170 --> 00:12:23,310
render the different things so it's an

364
00:12:20,970 --> 00:12:26,190
understood problem and given infinite

365
00:12:23,310 --> 00:12:29,250
computing power I'm we could be doing

366
00:12:26,190 --> 00:12:31,050
that in virtual reality however a point

367
00:12:29,250 --> 00:12:33,000
that I've made to two people in recent

368
00:12:31,050 --> 00:12:34,440
years is that we are running out of

369
00:12:33,000 --> 00:12:35,700
Moore's Law I am

370
00:12:34,440 --> 00:12:37,230
I mean maybe we'll see some wonderful

371
00:12:35,700 --> 00:12:39,390
breakthrough in you know quantum

372
00:12:37,230 --> 00:12:42,120
structures or whatever or bandwidth I

373
00:12:39,390 --> 00:12:43,770
can but yeah but if we just wind wind up

374
00:12:42,120 --> 00:12:46,470
following the path that we're on we're

375
00:12:43,770 --> 00:12:48,030
gonna get double and quadruple but we're

376
00:12:46,470 --> 00:12:50,130
not gonna get 50 times more powerful

377
00:12:48,030 --> 00:12:53,220
than we are right now we will run into

378
00:12:50,130 --> 00:12:54,690
atomic limits on our fabrication so

379
00:12:53,220 --> 00:12:57,870
given that I'm trying to tell people

380
00:12:54,690 --> 00:12:59,940
that start buying back into optimization

381
00:12:57,870 --> 00:13:01,710
start buying back into thinking a little

382
00:12:59,940 --> 00:13:03,540
bit more creatively because you can't

383
00:13:01,710 --> 00:13:05,970
just wait it's not going to get to that

384
00:13:03,540 --> 00:13:08,040
point where it really is fixed to to

385
00:13:05,970 --> 00:13:10,320
those highest degrees just by waiting

386
00:13:08,040 --> 00:13:11,940
for computing to advance if we want this

387
00:13:10,320 --> 00:13:15,090
to be something used by a billion people

388
00:13:11,940 --> 00:13:17,610
then we need it to be lighter cheaper

389
00:13:15,090 --> 00:13:20,760
more comfortable and there's constantly

390
00:13:17,610 --> 00:13:23,580
novel iux innovations like Google Earth

391
00:13:20,760 --> 00:13:25,170
whether that is the kind of elimination

392
00:13:23,580 --> 00:13:27,540
of your periphery as you zoom in and

393
00:13:25,170 --> 00:13:29,310
move and also giving you the user action

394
00:13:27,540 --> 00:13:31,320
to decide where you're going and they're

395
00:13:29,310 --> 00:13:33,000
looking there's a constantly seeing

396
00:13:31,320 --> 00:13:35,220
people coming up with ways to kind of

397
00:13:33,000 --> 00:13:39,150
break from the paradigms of actual

398
00:13:35,220 --> 00:13:40,890
reality and then introduce very well you

399
00:13:39,150 --> 00:13:42,390
know there's tons of opportunities for

400
00:13:40,890 --> 00:13:43,560
the synthetic case where you want to be

401
00:13:42,390 --> 00:13:46,230
able to have your synthetic fantasy

402
00:13:43,560 --> 00:13:48,540
world where everybody is a creature

403
00:13:46,230 --> 00:13:50,850
that's created by the inviter and

404
00:13:48,540 --> 00:13:53,100
simulated reasonably yeah but of course

405
00:13:50,850 --> 00:13:55,200
we've got been you know we still don't

406
00:13:53,100 --> 00:13:56,940
do people well simulated that's a hard

407
00:13:55,200 --> 00:13:58,680
problem we've been beating our heads

408
00:13:56,940 --> 00:14:01,800
against it for a long time I do think

409
00:13:58,680 --> 00:14:03,570
we're making progress and I wouldn't bet

410
00:14:01,800 --> 00:14:05,280
on that being solved in 10 years but

411
00:14:03,570 --> 00:14:06,840
maybe 20 years because it is gonna take

412
00:14:05,280 --> 00:14:08,970
a lot of AI it's gonna take a lot of

413
00:14:06,840 --> 00:14:11,250
machine learning where it's not gonna be

414
00:14:08,970 --> 00:14:13,590
a matter of us dissecting all the micro

415
00:14:11,250 --> 00:14:15,840
expressions that people do it's gonna be

416
00:14:13,590 --> 00:14:17,730
let's take every YouTube video ever made

417
00:14:15,840 --> 00:14:19,650
and run it through some enormous learner

418
00:14:17,730 --> 00:14:21,990
that's going to figure out how to make

419
00:14:19,650 --> 00:14:23,610
people look realistic absolutely there

420
00:14:21,990 --> 00:14:25,530
are these crucial thresholds where you

421
00:14:23,610 --> 00:14:26,850
pass you know technological hurdle and

422
00:14:25,530 --> 00:14:28,740
all of a sudden that unlocks a whole

423
00:14:26,850 --> 00:14:30,390
world of creative potential but I think

424
00:14:28,740 --> 00:14:32,330
to your point very much we need to solve

425
00:14:30,390 --> 00:14:35,100
the actual human and social challenges

426
00:14:32,330 --> 00:14:37,050
and and turn those into opportunities to

427
00:14:35,100 --> 00:14:38,550
figure out how this technology fits into

428
00:14:37,050 --> 00:14:40,050
our lives I'm still believe that the

429
00:14:38,550 --> 00:14:42,720
magics out there and you haven't found

430
00:14:40,050 --> 00:14:45,270
it yet so somebody is going to you know

431
00:14:42,720 --> 00:14:47,130
happen upon the formula I feel like I've

432
00:14:45,270 --> 00:14:49,830
felt the little pockets of math there's

433
00:14:47,130 --> 00:14:51,720
that oh you can imagine the utility by

434
00:14:49,830 --> 00:14:54,120
creating a world or you can imagine the

435
00:14:51,720 --> 00:14:55,860
power of the story that would be told

436
00:14:54,120 --> 00:14:57,690
you in this context and a lot of it I

437
00:14:55,860 --> 00:14:59,010
think is just picking those putting them

438
00:14:57,690 --> 00:15:00,510
together in a meaningful way and then

439
00:14:59,010 --> 00:15:01,980
crafting something that's that's really

440
00:15:00,510 --> 00:15:04,200
bigger than intentional but I've now

441
00:15:01,980 --> 00:15:06,480
been joining with real people in virtual

442
00:15:04,200 --> 00:15:08,100
reality and you I think we also have

443
00:15:06,480 --> 00:15:10,230
different levels of connection like the

444
00:15:08,100 --> 00:15:12,120
audio side is the leaps and bounds above

445
00:15:10,230 --> 00:15:13,590
in terms of the nuance of personality in

446
00:15:12,120 --> 00:15:16,320
humanity so yeah when I hear people

447
00:15:13,590 --> 00:15:18,090
laughing and joking and and you know

448
00:15:16,320 --> 00:15:19,950
really enjoying something soon we'll get

449
00:15:18,090 --> 00:15:22,020
into the word macro gesture land or I

450
00:15:19,950 --> 00:15:23,700
can wave and say you know but when we

451
00:15:22,020 --> 00:15:25,980
get into micro gestures and I'm actually

452
00:15:23,700 --> 00:15:27,600
getting a sense of facial reactions and

453
00:15:25,980 --> 00:15:31,170
other things I think then we'll have a

454
00:15:27,600 --> 00:15:32,850
really incredibly rewarding time

455
00:15:31,170 --> 00:15:36,150
spending spending time with each other

456
00:15:32,850 --> 00:15:37,920
so v4 right now is pretty amazing when

457
00:15:36,150 --> 00:15:39,720
you look at it it's things that you

458
00:15:37,920 --> 00:15:41,670
haven't seen but we are just getting

459
00:15:39,720 --> 00:15:44,400
started the next five years both

460
00:15:41,670 --> 00:15:46,200
technologically and creatively are going

461
00:15:44,400 --> 00:15:48,900
to really take this medium someplace

462
00:15:46,200 --> 00:15:53,470
that you've never imagined

463
00:15:48,900 --> 00:15:53,470
[Music]

