1
00:00:00,093 --> 00:00:01,670
- Hi, I'm Robert J. Lang.

2
00:00:01,670 --> 00:00:03,820
I'm a physicist and origami artist

3
00:00:03,820 --> 00:00:06,630
and today I've been
challenged to explain origami

4
00:00:06,630 --> 00:00:07,680
in five levels.

5
00:00:07,680 --> 00:00:08,900
If you know a little origami

6
00:00:08,900 --> 00:00:11,740
you might think it's nothing
more than simple toys,

7
00:00:11,740 --> 00:00:13,570
like cranes or cootie catchers,

8
00:00:13,570 --> 00:00:15,920
but origami is much more than that.

9
00:00:15,920 --> 00:00:19,563
Out of the vast cloud
of origami possibilities

10
00:00:19,563 --> 00:00:21,546
I've chosen five different levels

11
00:00:21,546 --> 00:00:23,895
that illustrate the diversity of this art.

12
00:00:23,895 --> 00:00:26,812
[thoughtful music]

13
00:00:28,280 --> 00:00:30,410
Do you know what origami is?

14
00:00:30,410 --> 00:00:32,820
- Is that where you fold paper

15
00:00:32,820 --> 00:00:36,000
to make different animals, like those?

16
00:00:36,000 --> 00:00:37,690
- Yes, in fact it is.

17
00:00:37,690 --> 00:00:40,200
Have you ever done any origami before?

18
00:00:40,200 --> 00:00:41,220
- Nope.

19
00:00:41,220 --> 00:00:42,430
- [Robert] Would you
like to give it a try?

20
00:00:42,430 --> 00:00:44,540
- Sure.
- Okay, so we'll do some,

21
00:00:44,540 --> 00:00:46,640
but I want to tell you a
little bit about origami.

22
00:00:46,640 --> 00:00:50,330
Most origami follows two,
I'll call them customs,

23
00:00:50,330 --> 00:00:51,650
almost like rules.

24
00:00:51,650 --> 00:00:53,689
It's usually from a square

25
00:00:53,689 --> 00:00:57,830
and the other is it's
usually folded with no cuts.

26
00:00:57,830 --> 00:01:01,300
So these guys are folded
from an uncut square.

27
00:01:01,300 --> 00:01:02,133
- That's awesome.

28
00:01:02,133 --> 00:01:03,090
- So you ready?

29
00:01:03,090 --> 00:01:04,160
- Yep.
- Okay.

30
00:01:04,160 --> 00:01:06,040
We're going to start with a model

31
00:01:06,040 --> 00:01:08,621
that every Japanese person
learns in kindergarten,

32
00:01:08,621 --> 00:01:11,480
it's called a crane,
traditional origami design,

33
00:01:11,480 --> 00:01:13,090
it's over 400 years old.

34
00:01:13,090 --> 00:01:14,810
So, people have been doing
what we're about to do

35
00:01:14,810 --> 00:01:16,830
for 400 years.
- Wow.

36
00:01:16,830 --> 00:01:20,830
- Let's fold it in half from
corner to corner, unfold it

37
00:01:21,810 --> 00:01:24,210
and then we'll fold it in
half the other direction,

38
00:01:24,210 --> 00:01:27,200
also corner to corner but
we're going to lift it up

39
00:01:27,200 --> 00:01:29,982
and we're going to hold
the fold with both hands.

40
00:01:29,982 --> 00:01:32,020
We're going to bring
these corners together,

41
00:01:32,020 --> 00:01:33,920
making a little pocket and then,

42
00:01:33,920 --> 00:01:36,260
this is the trickiest
part of this whole design,

43
00:01:36,260 --> 00:01:39,210
so you're going to put your
finger underneath the top layer

44
00:01:39,210 --> 00:01:40,980
and we're going to try to make that layer

45
00:01:40,980 --> 00:01:43,140
fold right along the edge.

46
00:01:43,140 --> 00:01:45,100
Now you see how the sides
kind of want to come in

47
00:01:45,100 --> 00:01:47,000
as you're doing that?
- Yeah.

48
00:01:47,000 --> 00:01:48,550
- It's called a petal fold,

49
00:01:48,550 --> 00:01:50,407
it's a part of a lot of origami designs

50
00:01:50,407 --> 00:01:52,380
and it's key to the crane.

51
00:01:52,380 --> 00:01:54,790
Now we're ready for the magic.

52
00:01:54,790 --> 00:01:57,000
We're going to hold it in
between thumb and forefinger,

53
00:01:57,000 --> 00:01:58,320
reach inside,

54
00:01:58,320 --> 00:02:00,630
grab the skinny point that's
between the two layers,

55
00:02:00,630 --> 00:02:01,650
which are the wings,

56
00:02:01,650 --> 00:02:05,620
and I'm going to slide it out
so it pokes out at an angle.

57
00:02:05,620 --> 00:02:08,480
We'll take the two wings, we
spread them out to the side

58
00:02:08,480 --> 00:02:11,310
and you have made your
first origami crane.

59
00:02:11,310 --> 00:02:12,380
- Wow.

60
00:02:12,380 --> 00:02:15,020
- Now, this is a
traditional Japanese design

61
00:02:15,020 --> 00:02:20,020
but there are origami designs
that have been around so long

62
00:02:20,314 --> 00:02:23,180
we're not entirely sure
where they originated.

63
00:02:23,180 --> 00:02:25,199
We're going to learn how
to fold a cootie catcher.

64
00:02:25,199 --> 00:02:26,032
- Okay, good.

65
00:02:26,032 --> 00:02:26,880
- So we'll start with the white side up

66
00:02:26,880 --> 00:02:30,134
and we're going to fold it in
half from corner to corner,

67
00:02:30,134 --> 00:02:33,910
in one fold and now we're
going to fold all four corners

68
00:02:33,910 --> 00:02:35,763
to the crossing point in the center.

69
00:02:36,630 --> 00:02:39,440
We'll fold it in half like a book.

70
00:02:39,440 --> 00:02:41,803
On the folded side we'll take
one of the folded corners

71
00:02:41,803 --> 00:02:44,470
and I'm going to fold it
up through all layers.

72
00:02:44,470 --> 00:02:45,800
There's a pocket in the middle.

73
00:02:45,800 --> 00:02:47,110
We're going to spread the pocket

74
00:02:47,110 --> 00:02:49,260
and bring all four corners together.

75
00:02:49,260 --> 00:02:51,440
Where you have original
corners of the square,

76
00:02:51,440 --> 00:02:53,520
we're gonna just pop those out.

77
00:02:53,520 --> 00:02:55,590
This is one of the most
satisfying moments,

78
00:02:55,590 --> 00:02:56,930
I think-
- Yeah.

79
00:02:56,930 --> 00:02:58,630
- because it suddenly changes shape.

80
00:02:58,630 --> 00:03:01,505
- I have seen these before,
my friends use these.

81
00:03:01,505 --> 00:03:02,338
- Yeah,

82
00:03:02,338 --> 00:03:04,500
but there's something else
we can do with this model.

83
00:03:04,500 --> 00:03:06,680
If we set it down and push on the middle

84
00:03:07,700 --> 00:03:09,110
then pop it inside out

85
00:03:10,060 --> 00:03:13,570
so that three flaps come
up and one stays down

86
00:03:13,570 --> 00:03:15,630
and then it's called talking crow

87
00:03:15,630 --> 00:03:17,690
because here's a little
crow's beak and mouth.

88
00:03:17,690 --> 00:03:18,523
- Wow.

89
00:03:18,523 --> 00:03:21,260
- There's thousands of
other origami designs

90
00:03:21,260 --> 00:03:23,910
but these are some of
the first people learn

91
00:03:23,910 --> 00:03:25,160
and this was, in fact,

92
00:03:25,160 --> 00:03:27,777
one of the first origami designs I learned

93
00:03:27,777 --> 00:03:30,698
some 50 years ago.
- Wow.

94
00:03:30,698 --> 00:03:32,133
- So, what do you think of that?

95
00:03:32,133 --> 00:03:34,140
What do you think of origami?

96
00:03:34,140 --> 00:03:37,580
- I think that the people
that make them are talented.

97
00:03:37,580 --> 00:03:38,620
It's hard.

98
00:03:38,620 --> 00:03:40,730
Seeing the stuff that we've made here,

99
00:03:40,730 --> 00:03:44,240
I'd bet that they could do rocket ships.

100
00:03:44,240 --> 00:03:46,530
Just so much that you can do with them.

101
00:03:46,530 --> 00:03:47,363
- Thanks for coming.

102
00:03:47,363 --> 00:03:48,880
- Thanks for having me.

103
00:03:48,880 --> 00:03:51,797
[thoughtful music]

104
00:03:53,600 --> 00:03:57,490
- A lot of origami is
animals, birds and things.

105
00:03:57,490 --> 00:04:00,897
There's also a branch of origami that is,

106
00:04:00,897 --> 00:04:05,720
it's more abstract or
geometric, called tessellations.

107
00:04:05,720 --> 00:04:08,460
Tessellations, like most origami,

108
00:04:08,460 --> 00:04:10,710
are folded from a single sheet of paper

109
00:04:10,710 --> 00:04:12,230
but they make patterns,

110
00:04:12,230 --> 00:04:14,790
whether it's woven patterns like that,

111
00:04:14,790 --> 00:04:16,630
or woven patterns like this.

112
00:04:16,630 --> 00:04:18,330
If you hold them up to the light

113
00:04:18,330 --> 00:04:20,140
you can see patterns.
- Wow.

114
00:04:20,140 --> 00:04:21,750
- The thing that makes them cool

115
00:04:21,750 --> 00:04:23,000
is they're sort of like tilings,

116
00:04:23,000 --> 00:04:24,750
it looks like you could put this together

117
00:04:24,750 --> 00:04:27,960
by cutting little pieces of
paper and sliding them together,

118
00:04:27,960 --> 00:04:29,440
but they're still one sheet.

119
00:04:29,440 --> 00:04:30,510
- They weren't cut?

120
00:04:30,510 --> 00:04:32,840
- There's no cuts in these just folding.

121
00:04:32,840 --> 00:04:37,240
We can build these up from
smaller building blocks of folds,

122
00:04:37,240 --> 00:04:39,880
learn how to fold little
pieces and put them together

123
00:04:39,880 --> 00:04:42,606
in the same way that a tiling like this

124
00:04:42,606 --> 00:04:45,420
looks like it's built up of little pieces.

125
00:04:45,420 --> 00:04:48,030
Can you make a fold that starts at the dot

126
00:04:48,030 --> 00:04:50,110
that doesn't run all the
way across the paper?

127
00:04:50,110 --> 00:04:54,580
- How about like that?
- Mm-hmm.

128
00:04:54,580 --> 00:04:58,447
Each of these folds is
peaked like a mountain

129
00:04:58,447 --> 00:05:00,519
and we call these mountain folds

130
00:05:00,519 --> 00:05:03,456
but if I made it the other
way, then it's shaped this way

131
00:05:03,456 --> 00:05:05,480
and we call it a valley fold.

132
00:05:05,480 --> 00:05:08,530
In all of origami there's
just mountains and valleys.

133
00:05:08,530 --> 00:05:10,470
- So all the folds are reversible?

134
00:05:10,470 --> 00:05:12,568
- So they're all
reversible and it turns out

135
00:05:12,568 --> 00:05:17,310
that in every origami
shape that folds flat,

136
00:05:17,310 --> 00:05:19,980
it's going to be either
three mountains and a valley

137
00:05:19,980 --> 00:05:22,450
or, if we're looking at the backside,

138
00:05:22,450 --> 00:05:24,430
three valleys and a mountain,

139
00:05:24,430 --> 00:05:25,920
they always differ by two.
- Oh.

140
00:05:25,920 --> 00:05:28,450
- That's a rule of all flat origami

141
00:05:28,450 --> 00:05:30,680
no matter how many folds
come together at a point

142
00:05:30,680 --> 00:05:34,120
and I'm going to show you a
building block of tessellations,

143
00:05:34,120 --> 00:05:35,302
it's called a twist

144
00:05:35,302 --> 00:05:39,462
because that center
square, as I unfold it,

145
00:05:39,462 --> 00:05:41,960
it twists, it rotates.
- Twists?

146
00:05:41,960 --> 00:05:46,350
- If I had another twist
in the same sheet of paper

147
00:05:46,350 --> 00:05:48,017
I could make these
folds connect with that,

148
00:05:48,017 --> 00:05:49,440
and these folds connect with that.

149
00:05:49,440 --> 00:05:52,010
And if I had another one up
here, I could make all three.

150
00:05:52,010 --> 00:05:54,530
And if I had a square array
and all the folds lined up

151
00:05:54,530 --> 00:05:57,167
I could make bigger and
bigger arrays, like these,

152
00:05:57,167 --> 00:06:00,310
because these are just very large twists.

153
00:06:00,310 --> 00:06:03,270
In this case it's an octagon
rather than a square,

154
00:06:03,270 --> 00:06:05,450
but they're arranged in rows and columns.

155
00:06:05,450 --> 00:06:07,113
And let's just try going along.

156
00:06:11,460 --> 00:06:14,240
All right, there is our tessellation

157
00:06:14,240 --> 00:06:16,570
with squares and hexagons.

158
00:06:16,570 --> 00:06:19,000
So you have now designed and folded

159
00:06:19,000 --> 00:06:20,990
your first origami tessellation

160
00:06:20,990 --> 00:06:24,600
and perhaps you can see
how just using this idea

161
00:06:24,600 --> 00:06:27,820
of building up tiles and
small building blocks

162
00:06:27,820 --> 00:06:32,153
you could make tessellations
as big and complex as you want.

163
00:06:32,153 --> 00:06:33,650
- That was cool.
- Yeah,

164
00:06:33,650 --> 00:06:37,309
so what do you think now of
origami and tessellations?

165
00:06:37,309 --> 00:06:39,310
- Origami, I think,

166
00:06:39,310 --> 00:06:43,830
is the folding of paper to
make anything in general,

167
00:06:43,830 --> 00:06:48,280
from 3D things to flat things

168
00:06:48,280 --> 00:06:52,020
and I think origami is
about turning simple things

169
00:06:52,020 --> 00:06:55,470
into complex things and
it's all about patterns.

170
00:06:55,470 --> 00:06:57,057
- That is a great definition.

171
00:06:57,057 --> 00:06:59,640
[upbeat music]

172
00:07:00,560 --> 00:07:04,590
- So here's a dragon fly and
he's got six legs, four wings.

173
00:07:04,590 --> 00:07:05,590
- Wow.
- Here's a spider

174
00:07:05,590 --> 00:07:07,590
with eight legs, ants with legs

175
00:07:07,590 --> 00:07:09,640
and these, just like the crane,

176
00:07:09,640 --> 00:07:11,620
are folded from a single uncut square.

177
00:07:11,620 --> 00:07:12,470
- What?

178
00:07:12,470 --> 00:07:13,460
- To figure out how to do that

179
00:07:13,460 --> 00:07:17,810
we need to learn a little
bit about what makes a point.

180
00:07:17,810 --> 00:07:19,220
So, let's come back to the crane.

181
00:07:19,220 --> 00:07:20,053
You can probably tell

182
00:07:20,053 --> 00:07:22,920
that the corners of the
square ended up as points,

183
00:07:22,920 --> 00:07:23,753
right?
- Yes.

184
00:07:23,753 --> 00:07:25,950
- That's a corner, four corners
of the square, four points.

185
00:07:25,950 --> 00:07:29,990
How would you make one point
out of this sheet of paper?

186
00:07:29,990 --> 00:07:31,670
- I'm thinking of, like, a paper airplane.

187
00:07:31,670 --> 00:07:33,380
- Yeah, exactly.

188
00:07:33,380 --> 00:07:35,700
Actually you've discovered
something pretty neat

189
00:07:35,700 --> 00:07:39,560
because you made your
point not from a corner

190
00:07:39,560 --> 00:07:42,350
so you've already discovered
one of the key insights.

191
00:07:42,350 --> 00:07:45,370
Any flap, any point, leg of the ant,

192
00:07:45,370 --> 00:07:48,520
takes up a circular region of paper.

193
00:07:48,520 --> 00:07:50,280
Here's our boundary.

194
00:07:50,280 --> 00:07:54,350
To make your point from an
edge you use that much paper

195
00:07:54,350 --> 00:07:57,808
and the shape, it's almost a circle.

196
00:07:57,808 --> 00:07:59,260
If we take the crane

197
00:07:59,260 --> 00:08:04,113
we'll see if the circles are
visible in the crane pattern.

198
00:08:05,000 --> 00:08:09,438
Here's the crane pattern, and
here's a boundary of the wing,

199
00:08:09,438 --> 00:08:11,540
and here's the other wing.
- Okay.

200
00:08:11,540 --> 00:08:13,626
- The crane has four circles

201
00:08:13,626 --> 00:08:16,360
but, actually, there's a
little bit of a surprise

202
00:08:16,360 --> 00:08:18,530
because what about this?

203
00:08:18,530 --> 00:08:22,130
There's a fifth circle,
which is like that,

204
00:08:22,130 --> 00:08:25,670
but does the crane have
a fifth flap in it?

205
00:08:25,670 --> 00:08:30,670
Let's refold it and put the wings up.

206
00:08:31,130 --> 00:08:33,540
Well, yes, there is, there's another point

207
00:08:33,540 --> 00:08:35,680
and that point is the
fifth circle of our crane.

208
00:08:35,680 --> 00:08:37,750
- Okay.
- And to do that

209
00:08:37,750 --> 00:08:40,770
we use a new technique
called circle packing

210
00:08:40,770 --> 00:08:43,740
in which all of the long
features of the design

211
00:08:43,740 --> 00:08:45,540
are represented by circles.

212
00:08:45,540 --> 00:08:48,734
So, each leg becomes a circle,
each wing becomes a circle

213
00:08:48,734 --> 00:08:50,920
and things that can be big and thick,

214
00:08:50,920 --> 00:08:54,290
like the head or the abdomen,
can be points in the middle.

215
00:08:54,290 --> 00:08:58,090
Now we have the basic idea
of how to design the pattern,

216
00:08:58,090 --> 00:08:59,740
we just count the number of legs we want.

217
00:08:59,740 --> 00:09:02,900
We want a spider, if it's
got let's say eight legs,

218
00:09:02,900 --> 00:09:05,097
it's also got an abdomen,
that's another point,

219
00:09:05,097 --> 00:09:07,814
and it's got a head, so
maybe that's 10 points.

220
00:09:07,814 --> 00:09:10,310
If we find an arrangement of 10 circles

221
00:09:10,310 --> 00:09:13,660
we should be able to fold
that into the spider.

222
00:09:13,660 --> 00:09:17,150
So in this book, Origami
Insects II, it's one of my books

223
00:09:17,150 --> 00:09:20,672
and has some patterns,
and this is one of them

224
00:09:20,672 --> 00:09:23,960
for a flying ladybug and, in fact,

225
00:09:23,960 --> 00:09:27,520
it is exactly this flying ladybug.

226
00:09:27,520 --> 00:09:29,450
We've got the crease
pattern here in the circles

227
00:09:29,450 --> 00:09:32,010
and you might now be able to see

228
00:09:32,010 --> 00:09:35,470
which circles end up as which parts,

229
00:09:35,470 --> 00:09:38,410
knowing that the largest
features like the wings

230
00:09:38,410 --> 00:09:40,020
are going to be the largest circles,

231
00:09:40,020 --> 00:09:41,970
smaller points will be smaller circles.

232
00:09:41,970 --> 00:09:44,490
So any thoughts which might be?

233
00:09:44,490 --> 00:09:46,420
- Well, the legs and the antenna

234
00:09:46,420 --> 00:09:48,990
would probably have to
be these smaller ones,

235
00:09:48,990 --> 00:09:51,250
in the middle.
- Yeah, that's right.

236
00:09:51,250 --> 00:09:53,090
- [College Student] Oh,
this looks like the back

237
00:09:53,090 --> 00:09:55,260
'cause there's a bunch of
circles all the way down,

238
00:09:55,260 --> 00:09:57,210
like here.
- Mm-hmm, exactly.

239
00:09:57,210 --> 00:09:59,220
And then the wings?

240
00:09:59,220 --> 00:10:01,550
- You've got four big wings

241
00:10:01,550 --> 00:10:03,260
which you could see on the ends there

242
00:10:03,260 --> 00:10:04,200
and then, I guess, the head.

243
00:10:04,200 --> 00:10:07,940
- You've got it, so you are
ready to design origami.

244
00:10:07,940 --> 00:10:08,773
- Awesome.

245
00:10:10,090 --> 00:10:11,997
- Origami artists all around the world

246
00:10:11,997 --> 00:10:16,660
now use ideas like this to
design, not just insects,

247
00:10:16,660 --> 00:10:20,155
but animals, and birds,
and all sorts of things

248
00:10:20,155 --> 00:10:25,155
that are, I think, unbelievably
complex and realistic

249
00:10:25,320 --> 00:10:27,740
but most importantly, beautiful.

250
00:10:27,740 --> 00:10:29,131
- Wow, that's so impressive.

251
00:10:29,131 --> 00:10:30,593
I think I learned how to make
one of these paper cranes

252
00:10:30,593 --> 00:10:33,590
when I was in third grade but
I guess I never unfolded it

253
00:10:33,590 --> 00:10:35,360
to actually see where it was coming from.

254
00:10:35,360 --> 00:10:37,570
And so now that it's all
broken up into circles

255
00:10:37,570 --> 00:10:40,820
it makes these super
complicated insects and animals

256
00:10:40,820 --> 00:10:43,160
and everything seem so much
simpler, so that's so cool.

257
00:10:43,160 --> 00:10:44,430
I'm pretty excited about it.
- That's so cool.

258
00:10:44,430 --> 00:10:46,185
- Thank you so much for
telling me about this.

259
00:10:46,185 --> 00:10:49,090
[upbeat music]

260
00:10:49,090 --> 00:10:51,090
- Whenever there's a part of a spacecraft

261
00:10:51,090 --> 00:10:53,650
that is shaped somewhat like paper,

262
00:10:53,650 --> 00:10:55,060
meaning it's big and flat,

263
00:10:55,060 --> 00:10:58,100
we can use folding mechanisms from origami

264
00:10:58,100 --> 00:10:59,120
to make it smaller.

265
00:10:59,120 --> 00:11:01,400
- Right.
- Telescopes, solar arrays,

266
00:11:01,400 --> 00:11:03,890
they need to be packed
into a rocket, go up,

267
00:11:03,890 --> 00:11:07,035
but then expand in a very
controlled, deterministic way

268
00:11:07,035 --> 00:11:08,690
when they get up into space.
- Okay.

269
00:11:08,690 --> 00:11:11,059
- These are the building blocks

270
00:11:11,059 --> 00:11:14,660
of many, many origami deployable shapes,

271
00:11:14,660 --> 00:11:16,550
it's called a degree-4 vertex.

272
00:11:16,550 --> 00:11:17,800
It's the number of lines.

273
00:11:17,800 --> 00:11:21,180
So in this case, we use
solid lines for mountain,

274
00:11:21,180 --> 00:11:23,170
we use dash lines for valley.

275
00:11:23,170 --> 00:11:25,870
We're going to fold it and
use these two to illustrate

276
00:11:25,870 --> 00:11:29,270
some important properties
of origami mechanisms.

277
00:11:29,270 --> 00:11:31,840
It's important in the study of mechanisms

278
00:11:31,840 --> 00:11:33,690
to take into account the rigidity.

279
00:11:33,690 --> 00:11:36,924
So what we're going to do
to help simulate rigidity

280
00:11:36,924 --> 00:11:39,290
is to take these rectangles

281
00:11:39,290 --> 00:11:42,920
and we're going to fold them over and over

282
00:11:42,920 --> 00:11:45,630
so that they just become stiff and rigid.

283
00:11:45,630 --> 00:11:46,463
- [Grad Student] Okay.

284
00:11:46,463 --> 00:11:47,760
- So this is what's called

285
00:11:47,760 --> 00:11:50,230
a single degree of freedom mechanism.

286
00:11:50,230 --> 00:11:53,017
You have one degree of freedom,
I can choose this fold,

287
00:11:53,017 --> 00:11:55,246
and then if these are perfectly rigid

288
00:11:55,246 --> 00:11:58,220
every other fold angle
is fully determined.

289
00:11:58,220 --> 00:11:59,810
One of the key behaviors here

290
00:11:59,810 --> 00:12:02,360
is that with the smaller angles up here,

291
00:12:02,360 --> 00:12:04,590
the two folds that are the same parity

292
00:12:04,590 --> 00:12:06,710
and the folds that are of opposite parity

293
00:12:06,710 --> 00:12:09,120
move at about the same rate

294
00:12:09,120 --> 00:12:12,710
but with this, as we're
getting closer to 90 degrees,

295
00:12:12,710 --> 00:12:15,760
we find they move at very different rates

296
00:12:15,760 --> 00:12:18,900
and then at the end of the
motion, the opposite happens.

297
00:12:18,900 --> 00:12:20,410
This one is almost folded

298
00:12:20,410 --> 00:12:22,873
but this one goes through
a much larger motion so

299
00:12:22,873 --> 00:12:24,970
the relative speeds differ.
- Right.

300
00:12:24,970 --> 00:12:27,880
- So when we start sticking
together vertices like this,

301
00:12:27,880 --> 00:12:30,046
if they're individually
single degree of freedom

302
00:12:30,046 --> 00:12:34,143
then we can make very large
mechanisms that open and close

303
00:12:34,143 --> 00:12:36,310
but with just one degree of freedom.

304
00:12:36,310 --> 00:12:39,340
So, these are examples of a
pattern called the Miura-Ori.

305
00:12:39,340 --> 00:12:40,780
When you stretch them out

306
00:12:40,780 --> 00:12:42,470
they're pretty big.
- Okay.

307
00:12:42,470 --> 00:12:46,940
- And they fold flat and a
pattern almost exactly like this

308
00:12:46,940 --> 00:12:49,710
was used for a solar array
for a Japanese mission

309
00:12:49,710 --> 00:12:51,210
that flew in 1995.

310
00:12:51,210 --> 00:12:54,220
- So then you like fly it up compactly

311
00:12:54,220 --> 00:12:55,340
and then once you get up there,

312
00:12:55,340 --> 00:12:57,560
there's like some sort of
like motorized mechanism,

313
00:12:57,560 --> 00:12:59,110
but you only need it on one fold.

314
00:12:59,110 --> 00:13:01,550
- Yeah, so typically the mechanism

315
00:13:01,550 --> 00:13:03,540
will run from corner to corner,

316
00:13:03,540 --> 00:13:05,424
to diagonally to opposite corners

317
00:13:05,424 --> 00:13:09,780
because then you can
stretch it out that way.

318
00:13:09,780 --> 00:13:11,697
Notice some differences
between the one you have

319
00:13:11,697 --> 00:13:12,884
and the one I have

320
00:13:12,884 --> 00:13:17,194
in how this one sort of
opens out almost evenly

321
00:13:17,194 --> 00:13:20,790
but this one opens out more
one way and then the other.

322
00:13:20,790 --> 00:13:21,623
- Yeah.

323
00:13:21,623 --> 00:13:23,210
What sort of angle would you want

324
00:13:23,210 --> 00:13:25,100
so that they open up the same rate?

325
00:13:25,100 --> 00:13:26,970
- Infinitesimally small.
- Okay.

326
00:13:26,970 --> 00:13:27,940
- So, sadly,

327
00:13:27,940 --> 00:13:30,320
the only way to get them
at exactly the same rate

328
00:13:30,320 --> 00:13:32,524
is when these are microscopic slivers

329
00:13:32,524 --> 00:13:34,440
and then that's not useful.
- For sure, right, right.

330
00:13:34,440 --> 00:13:36,293
- And it's exactly the difference

331
00:13:36,293 --> 00:13:39,260
between the motions of these two vertices.

332
00:13:39,260 --> 00:13:42,300
So these angles are closer to right angles

333
00:13:42,300 --> 00:13:44,530
and the closer you get to a right angle

334
00:13:44,530 --> 00:13:46,490
the more asymmetry there is

335
00:13:46,490 --> 00:13:48,450
between the two directions of motion.

336
00:13:48,450 --> 00:13:51,330
And then the other difference
is how efficiently they pack,

337
00:13:51,330 --> 00:13:53,896
so these started at about the same size

338
00:13:53,896 --> 00:13:55,253
but when they're flat

339
00:13:55,253 --> 00:13:58,660
notice that yours is much more compact.

340
00:13:58,660 --> 00:14:00,470
So if I were you making a solar array,

341
00:14:00,470 --> 00:14:02,090
I'd say, oh, I want that one.

342
00:14:02,090 --> 00:14:05,690
But if I say, well, I want
them to open at the same rate,

343
00:14:05,690 --> 00:14:07,380
then I want this one.

344
00:14:07,380 --> 00:14:08,590
- So, it's kind of a trade-off?

345
00:14:08,590 --> 00:14:11,300
- There's an engineering trade-off
to get them both to work.

346
00:14:11,300 --> 00:14:12,240
And there's another place

347
00:14:12,240 --> 00:14:14,115
that shows up in deployable structures

348
00:14:14,115 --> 00:14:16,000
in a very cool structure.

349
00:14:16,000 --> 00:14:19,610
This is a folded tube, it
sort of pops out like this

350
00:14:19,610 --> 00:14:22,443
but it has this neat property
that if you twist it quickly,

351
00:14:22,443 --> 00:14:24,520
it changes color.

352
00:14:24,520 --> 00:14:26,912
There's a Mars Rover application

353
00:14:26,912 --> 00:14:30,410
where they need a sleeve
that protects a drill

354
00:14:30,410 --> 00:14:33,740
and as the drill goes down,
the sleeve is going to collapse

355
00:14:33,740 --> 00:14:36,384
and they're using a pattern
very much like this.

356
00:14:36,384 --> 00:14:37,660
- Interesting.

357
00:14:37,660 --> 00:14:40,960
- There are many open
mathematical questions

358
00:14:40,960 --> 00:14:44,420
and so room for
mathematicians, like yourself,

359
00:14:44,420 --> 00:14:48,710
to have a big impact on the
world of origami and mechanisms.

360
00:14:48,710 --> 00:14:50,840
And even though those studies

361
00:14:50,840 --> 00:14:52,670
are mathematically interesting,

362
00:14:52,670 --> 00:14:57,030
they're going to also have
real-world applications in space,

363
00:14:57,030 --> 00:15:00,330
solar arrays, drills,
telescopes, and more.

364
00:15:00,330 --> 00:15:02,520
Any questions or thoughts about this?

365
00:15:02,520 --> 00:15:04,480
- If you want to send something to space

366
00:15:04,480 --> 00:15:06,750
it probably makes sense
to do it compactly,

367
00:15:06,750 --> 00:15:08,820
so if you have something
that you can fold up

368
00:15:08,820 --> 00:15:12,040
and then unfold, just one of the folds,

369
00:15:12,040 --> 00:15:14,100
that's going to be
probably the easiest way

370
00:15:14,100 --> 00:15:15,690
to get something up there

371
00:15:15,690 --> 00:15:17,478
and expand it to what it needs to be.

372
00:15:17,478 --> 00:15:20,061
[upbeat music]

373
00:15:23,400 --> 00:15:26,210
- I'm Tom Hull, I'm a math
professor, a mathematician.

374
00:15:26,210 --> 00:15:28,940
I've been doing origami
since I was eight years old

375
00:15:28,940 --> 00:15:30,870
and studying the mathematics of origami

376
00:15:30,870 --> 00:15:32,230
ever since grad school, at least.

377
00:15:32,230 --> 00:15:33,570
- The first thing I want to show you

378
00:15:33,570 --> 00:15:35,770
is origami in the real world.

379
00:15:35,770 --> 00:15:37,280
This is the origami lamp.

380
00:15:37,280 --> 00:15:42,280
It comes shipped flat but it
folds, clip holds it together.

381
00:15:43,210 --> 00:15:45,860
The lamp has LEDs on the inside

382
00:15:45,860 --> 00:15:48,580
so when we power it up we get
light, we have a lampshade

383
00:15:48,580 --> 00:15:49,790
and we get the base.

384
00:15:49,790 --> 00:15:51,300
- Why does origami lend itself

385
00:15:51,300 --> 00:15:53,370
to, say, this type of application?

386
00:15:53,370 --> 00:15:55,960
- Origami applications have in common,

387
00:15:55,960 --> 00:15:59,030
is that at some stage the thing is flat

388
00:15:59,030 --> 00:16:03,141
and so whenever you need to
either start from a flat state

389
00:16:03,141 --> 00:16:05,935
and then take it to a 3D state,

390
00:16:05,935 --> 00:16:09,840
or conversely, for deployables like space,

391
00:16:09,840 --> 00:16:13,340
you want to have it in a
fully folded flat state

392
00:16:13,340 --> 00:16:15,390
but then take it to a 3D state,

393
00:16:15,390 --> 00:16:17,670
or possibly an unfolded flat state.

394
00:16:17,670 --> 00:16:19,730
Whenever a flat state is involved,

395
00:16:19,730 --> 00:16:22,750
origami is a really effective way

396
00:16:22,750 --> 00:16:25,030
of making the transition
between those states.

397
00:16:25,030 --> 00:16:28,540
- Another aspect of origami
and origami mechanisms

398
00:16:28,540 --> 00:16:31,870
that has leant itself
to many different uses

399
00:16:31,870 --> 00:16:33,570
is the fact that it's scalable.

400
00:16:33,570 --> 00:16:36,040
When you have an origami crease pattern

401
00:16:36,040 --> 00:16:39,740
like the Miura-Ori used
in solar panel deployment,

402
00:16:39,740 --> 00:16:41,400
the type of motion that
you see happening here

403
00:16:41,400 --> 00:16:43,190
will happen whether this
is on a piece of paper

404
00:16:43,190 --> 00:16:46,230
that's small like this,
or on a larger scale,

405
00:16:46,230 --> 00:16:48,580
or even on a smaller, smaller,
smaller, smaller scale.

406
00:16:48,580 --> 00:16:50,930
Engineers, in particular
robotics engineers,

407
00:16:50,930 --> 00:16:52,020
are turning to origami

408
00:16:52,020 --> 00:16:55,860
toward designing mechanisms
that will either be really big

409
00:16:55,860 --> 00:16:57,130
or really, really small.

410
00:16:57,130 --> 00:16:59,040
This looks like the most promising way

411
00:16:59,040 --> 00:17:00,630
of getting nano robotics to work.

412
00:17:00,630 --> 00:17:03,140
- This is another real-world application

413
00:17:03,140 --> 00:17:05,070
but this particular implementation

414
00:17:05,070 --> 00:17:07,290
is used to make a wheel for a Rover.

415
00:17:07,290 --> 00:17:08,980
- Cool, so this is something

416
00:17:08,980 --> 00:17:11,590
that can actually get really, really tiny

417
00:17:11,590 --> 00:17:14,410
but then get big and fat and roll.

418
00:17:14,410 --> 00:17:15,794
- New problems arise

419
00:17:15,794 --> 00:17:19,990
when we try to make origami
out of things other than paper,

420
00:17:19,990 --> 00:17:22,460
but also new opportunities.

421
00:17:22,460 --> 00:17:23,860
An example here

422
00:17:23,860 --> 00:17:26,980
which is a kind of a
variant of the Miura-Ori.

423
00:17:26,980 --> 00:17:28,730
It's got a three-dimensional structure.

424
00:17:28,730 --> 00:17:31,050
If I stretch it one way,
it expands the other

425
00:17:31,050 --> 00:17:34,774
but because it has these
S-bends in the pattern,

426
00:17:34,774 --> 00:17:38,490
if you squeeze it, it
doesn't go all the way flat.

427
00:17:38,490 --> 00:17:42,610
This is a epoxy impregnated aramid fiber

428
00:17:42,610 --> 00:17:45,103
and so if I put this fold pattern into it

429
00:17:45,103 --> 00:17:46,907
and then compress it

430
00:17:46,907 --> 00:17:50,263
and then put a skin on the top and bottom,

431
00:17:50,263 --> 00:17:54,810
this becomes incredibly
lightweight but incredibly strong.

432
00:17:54,810 --> 00:17:55,643
- Yeah!

433
00:17:55,643 --> 00:17:56,660
- Another origami challenge

434
00:17:56,660 --> 00:17:59,112
that comes up with these patterns

435
00:17:59,112 --> 00:18:02,250
is if we're going to make an
aircraft out of this thing

436
00:18:02,250 --> 00:18:06,340
we're going to need hundreds
of yards of folded origami.

437
00:18:06,340 --> 00:18:07,650
We're not going to do it by hand

438
00:18:07,650 --> 00:18:12,054
and this might be the new
frontier in origami engineering,

439
00:18:12,054 --> 00:18:14,718
which is the design of machines

440
00:18:14,718 --> 00:18:18,380
that can fold patterns
that have applications.

441
00:18:18,380 --> 00:18:19,520
- So you're talking about a machine

442
00:18:19,520 --> 00:18:22,360
that is actually folding it into this,

443
00:18:22,360 --> 00:18:24,706
not just making the creases
but actually folding it.

444
00:18:24,706 --> 00:18:26,450
- Yeah, so what goes in as sheet

445
00:18:26,450 --> 00:18:29,277
and what comes out is this,
or something this wide.

446
00:18:29,277 --> 00:18:30,810
- That's cool, yeah.

447
00:18:30,810 --> 00:18:34,460
What do you see as kind of
like the next big breakthrough?

448
00:18:34,460 --> 00:18:35,800
Is there anything out there on the horizon

449
00:18:35,800 --> 00:18:37,650
that you're just like, oh
wow, this is really exciting?

450
00:18:37,650 --> 00:18:40,200
- It's something we've
talked about a little bit

451
00:18:40,200 --> 00:18:42,888
that with all the richness of behavior

452
00:18:42,888 --> 00:18:47,080
of origami from a flat sheet,

453
00:18:47,080 --> 00:18:50,490
it seems like there ought
to be an equally rich world

454
00:18:50,490 --> 00:18:53,010
of things that don't start flat

455
00:18:53,010 --> 00:18:54,970
but are still made from
flat sheets of paper.

456
00:18:54,970 --> 00:18:57,990
- So like a cone?
- Bi-stable properties

457
00:18:57,990 --> 00:19:02,270
and you can combine them together
with copies of themselves

458
00:19:02,270 --> 00:19:03,910
to make cellular structures.

459
00:19:03,910 --> 00:19:07,290
They're astonishingly stiff and
rigid, useful for mechanics.

460
00:19:07,290 --> 00:19:09,120
- The thing that I think
I'm the most excited about

461
00:19:09,120 --> 00:19:11,100
comes from math mainly.

462
00:19:11,100 --> 00:19:12,310
When I look at origami,

463
00:19:12,310 --> 00:19:13,630
when I look at all these applications

464
00:19:13,630 --> 00:19:17,150
or just all these different
origami folds, I see structure.

465
00:19:17,150 --> 00:19:19,050
Math is really about patterns.

466
00:19:19,050 --> 00:19:21,310
The patterns that we see in origami

467
00:19:21,310 --> 00:19:24,660
are reflecting some kind
of mathematical structure

468
00:19:24,660 --> 00:19:28,070
and we don't quite know yet
what all of that structure is

469
00:19:28,070 --> 00:19:30,260
and if we can tie a mathematical structure

470
00:19:30,260 --> 00:19:31,620
that's already well-studied

471
00:19:31,620 --> 00:19:33,630
to something we see happening in origami,

472
00:19:33,630 --> 00:19:36,070
then we can use the math tools right away

473
00:19:36,070 --> 00:19:37,480
to help solve the engineering problems

474
00:19:37,480 --> 00:19:38,630
and the origami problems.

475
00:19:38,630 --> 00:19:40,910
And the fact that there's
so many applications to this

476
00:19:40,910 --> 00:19:43,540
is really making people excited
who are working in the area.

477
00:19:43,540 --> 00:19:45,120
I'm really excited to see
what happens with that

478
00:19:45,120 --> 00:19:46,703
in the next five years or so.

479
00:19:46,703 --> 00:19:49,703
[encouraging music]

