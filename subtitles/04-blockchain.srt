1
00:00:00,030 --> 00:00:03,540
my name is Bettina Warburg I'm a

2
00:00:02,280 --> 00:00:05,759
researcher of transformative

3
00:00:03,540 --> 00:00:08,610
technologies and co-founder of animal

4
00:00:05,759 --> 00:00:10,920
ventures today I've been challenged to

5
00:00:08,610 --> 00:00:14,969
explain one concept at five levels of

6
00:00:10,920 --> 00:00:17,130
increasing complexity my topic is

7
00:00:14,969 --> 00:00:19,529
blockchain technology blockchain is a

8
00:00:17,130 --> 00:00:22,230
new network and it's going to help us

9
00:00:19,529 --> 00:00:24,300
decentralize trade allowing us to do a

10
00:00:22,230 --> 00:00:27,240
lot of our transactions much more

11
00:00:24,300 --> 00:00:30,179
peer-to-peer directly and lower our use

12
00:00:27,240 --> 00:00:32,759
of intermediaries like companies or

13
00:00:30,179 --> 00:00:34,200
banks maybe I think today everyone can

14
00:00:32,759 --> 00:00:37,950
leave understanding something about

15
00:00:34,200 --> 00:00:41,070
blockchain at some level do you know

16
00:00:37,950 --> 00:00:43,770
what we're gonna talk about today it's

17
00:00:41,070 --> 00:00:45,149
called blockchain with black teen that's

18
00:00:43,770 --> 00:00:48,450
a really good question

19
00:00:45,149 --> 00:00:50,760
it's actually a way that we can trade do

20
00:00:48,450 --> 00:00:52,920
you know what trade is it's what is when

21
00:00:50,760 --> 00:00:55,110
you take turn your sign that's when you

22
00:00:52,920 --> 00:00:56,550
give up most of what you want right when

23
00:00:55,110 --> 00:00:58,469
you give up most of what you want well

24
00:00:56,550 --> 00:01:01,230
sometimes that definitely happens for

25
00:00:58,469 --> 00:01:04,229
sure what if I told you that there's a

26
00:01:01,230 --> 00:01:06,810
kind of technology that I work on that

27
00:01:04,229 --> 00:01:10,350
means you could trade with any kid all

28
00:01:06,810 --> 00:01:13,530
over the world yeah I could train with

29
00:01:10,350 --> 00:01:15,600
any kid I would trade well I would trace

30
00:01:13,530 --> 00:01:17,100
children they don't like you much that's

31
00:01:15,600 --> 00:01:19,200
probably a good idea maybe somebody else

32
00:01:17,100 --> 00:01:21,030
likes it more than you do so normally

33
00:01:19,200 --> 00:01:23,850
when people trade they have to go to the

34
00:01:21,030 --> 00:01:25,380
store or they have to know the person so

35
00:01:23,850 --> 00:01:28,020
that they can get what they asked for

36
00:01:25,380 --> 00:01:30,540
with blockchain you can make that exact

37
00:01:28,020 --> 00:01:32,820
same trade but you don't need the store

38
00:01:30,540 --> 00:01:36,020
and you don't even necessarily need to

39
00:01:32,820 --> 00:01:36,020
know the other person

40
00:01:39,690 --> 00:01:45,040
so Ian do you know what blockchain is no

41
00:01:42,970 --> 00:01:47,050
have you ever traded or sold anything

42
00:01:45,040 --> 00:01:49,030
actually I'm selling my computer on eBay

43
00:01:47,050 --> 00:01:51,580
right now that's amazing what made you

44
00:01:49,030 --> 00:01:53,680
decide to trade on eBay um well I mean

45
00:01:51,580 --> 00:01:55,840
I've heard of it and I trust it a lot

46
00:01:53,680 --> 00:01:58,090
because there's they have like all their

47
00:01:55,840 --> 00:01:59,979
guarantees so I I know that I'm gonna

48
00:01:58,090 --> 00:02:01,930
get money and the person is gonna get

49
00:01:59,979 --> 00:02:04,510
what they want so what if I told you

50
00:02:01,930 --> 00:02:07,090
that blockchain technology is basically

51
00:02:04,510 --> 00:02:10,000
a tool where you can do the exact same

52
00:02:07,090 --> 00:02:12,670
thing but it goes to you and I directly

53
00:02:10,000 --> 00:02:15,040
you wouldn't need an eBay or a brand in

54
00:02:12,670 --> 00:02:16,360
between that's cool and there's a lot of

55
00:02:15,040 --> 00:02:18,850
those kinds of middlemen

56
00:02:16,360 --> 00:02:22,239
in our society today right we have a lot

57
00:02:18,850 --> 00:02:24,370
of banks we have a lot of companies that

58
00:02:22,239 --> 00:02:26,709
sort of help us make sure that our

59
00:02:24,370 --> 00:02:30,610
trades happen but if we could guarantee

60
00:02:26,709 --> 00:02:33,580
the same trade using technology as sort

61
00:02:30,610 --> 00:02:34,900
of like a technological trust then we

62
00:02:33,580 --> 00:02:37,690
wouldn't really need all those middlemen

63
00:02:34,900 --> 00:02:41,350
in between so how does it work it's

64
00:02:37,690 --> 00:02:44,850
basically a network of computers that

65
00:02:41,350 --> 00:02:47,830
all have the same history of

66
00:02:44,850 --> 00:02:49,600
transactions and so instead of sort of

67
00:02:47,830 --> 00:02:51,090
there being one company with one

68
00:02:49,600 --> 00:02:54,730
database that holds all the information

69
00:02:51,090 --> 00:02:56,140
the same sort of list is held by all

70
00:02:54,730 --> 00:02:57,910
these different people like you could

71
00:02:56,140 --> 00:03:01,150
have it on your computer and then it

72
00:02:57,910 --> 00:03:03,280
gets validated by everyone and basically

73
00:03:01,150 --> 00:03:06,070
that turns into the next part of the

74
00:03:03,280 --> 00:03:07,750
list so it's sort of constantly updating

75
00:03:06,070 --> 00:03:10,870
itself so like how do you make sure that

76
00:03:07,750 --> 00:03:14,620
it's secure so it uses cryptography and

77
00:03:10,870 --> 00:03:17,140
that helps it basically encode all of

78
00:03:14,620 --> 00:03:19,060
the transactions so you can't really see

79
00:03:17,140 --> 00:03:20,890
exactly what happened but you know it

80
00:03:19,060 --> 00:03:23,079
happened because it's like a marker so

81
00:03:20,890 --> 00:03:25,600
you could kind of like I don't know say

82
00:03:23,079 --> 00:03:27,579
trade you trade apples but you would

83
00:03:25,600 --> 00:03:28,959
just see like random letters yeah where

84
00:03:27,579 --> 00:03:31,390
it's saying I wouldn't be able to like

85
00:03:28,959 --> 00:03:34,299
track it exactly that's cool so it's

86
00:03:31,390 --> 00:03:36,400
kind of this like really big ledger or

87
00:03:34,299 --> 00:03:38,110
accounting system from all sorts of

88
00:03:36,400 --> 00:03:41,290
things that get traded but instead of

89
00:03:38,110 --> 00:03:45,340
being owned by one company it's owned by

90
00:03:41,290 --> 00:03:47,470
everybody that's cool yeah we're gonna

91
00:03:45,340 --> 00:03:49,250
talk about blockchain technology have

92
00:03:47,470 --> 00:03:52,280
you heard of boxing

93
00:03:49,250 --> 00:03:54,620
I've heard of the words blockchain but

94
00:03:52,280 --> 00:03:57,530
I'm not sure I know what it is so when

95
00:03:54,620 --> 00:03:59,300
we were much smaller societies you and I

96
00:03:57,530 --> 00:04:01,700
could trade in our community pretty

97
00:03:59,300 --> 00:04:04,460
easily but as the distance in our trade

98
00:04:01,700 --> 00:04:07,940
grew we ended up inventing institutions

99
00:04:04,460 --> 00:04:10,850
right if you use uber or you use Airbnb

100
00:04:07,940 --> 00:04:13,850
or you use Amazon even these are just

101
00:04:10,850 --> 00:04:16,130
digital marketplaces and platforms that

102
00:04:13,850 --> 00:04:19,130
help us facilitate an exchange of value

103
00:04:16,130 --> 00:04:21,940
mm-hmm but today we actually have a

104
00:04:19,130 --> 00:04:25,130
technology that allows us to trade

105
00:04:21,940 --> 00:04:27,740
one-to-one but at scale and it's called

106
00:04:25,130 --> 00:04:29,240
blockchain technology there is some kind

107
00:04:27,740 --> 00:04:32,090
of interface for it you could have an

108
00:04:29,240 --> 00:04:35,000
app or you could use your computer to do

109
00:04:32,090 --> 00:04:37,370
it but instead of there being a company

110
00:04:35,000 --> 00:04:39,890
in the middle or what's helping you make

111
00:04:37,370 --> 00:04:43,670
that transaction is a bunch of software

112
00:04:39,890 --> 00:04:46,460
code and so it's being run by all of

113
00:04:43,670 --> 00:04:48,800
these different computers that have like

114
00:04:46,460 --> 00:04:51,230
a node so they're all running the same

115
00:04:48,800 --> 00:04:54,650
software and guaranteeing your

116
00:04:51,230 --> 00:04:57,470
transactions as they happen I mean I

117
00:04:54,650 --> 00:05:00,169
would assume this technology is taking

118
00:04:57,470 --> 00:05:03,800
away business or activity from these

119
00:05:00,169 --> 00:05:05,480
middlemen in some cases yeah it is and a

120
00:05:03,800 --> 00:05:07,550
lot of people in the financial industry

121
00:05:05,480 --> 00:05:10,640
in particular are looking at it from the

122
00:05:07,550 --> 00:05:12,560
banking side of how do we use this

123
00:05:10,640 --> 00:05:17,240
technology to trade things like Bitcoin

124
00:05:12,560 --> 00:05:19,730
or other tokens that are easier to use

125
00:05:17,240 --> 00:05:21,680
instead of today's currency you know a

126
00:05:19,730 --> 00:05:24,860
lot of people think about blockchain as

127
00:05:21,680 --> 00:05:26,570
Bitcoin because it's sort of in the news

128
00:05:24,860 --> 00:05:29,990
a lot and it's this new cryptocurrency

129
00:05:26,570 --> 00:05:32,840
and it's kind of exciting but we're

130
00:05:29,990 --> 00:05:34,970
actually seeing a lot more use cases for

131
00:05:32,840 --> 00:05:37,130
blockchain that aren't around the

132
00:05:34,970 --> 00:05:41,060
currency side they're more around how do

133
00:05:37,130 --> 00:05:44,510
you take any asset and be able to trade

134
00:05:41,060 --> 00:05:49,640
that using the same technology is there

135
00:05:44,510 --> 00:05:53,540
a mechanism for verifying that person a

136
00:05:49,640 --> 00:05:55,780
is a legitimate seller or producer of

137
00:05:53,540 --> 00:05:58,090
the item

138
00:05:55,780 --> 00:06:01,180
so today a lot of people are working on

139
00:05:58,090 --> 00:06:03,490
how to create identity structures that

140
00:06:01,180 --> 00:06:05,080
leverage blockchain and one of the tools

141
00:06:03,490 --> 00:06:09,100
for doing that is being able to

142
00:06:05,080 --> 00:06:12,220
cryptographically assign for a given

143
00:06:09,100 --> 00:06:15,400
attribute so your government could sign

144
00:06:12,220 --> 00:06:18,250
that you have a u.s. passport or a

145
00:06:15,400 --> 00:06:20,710
university could sign that you are a

146
00:06:18,250 --> 00:06:23,310
currently enrolled student and you could

147
00:06:20,710 --> 00:06:25,930
then dole out that information and

148
00:06:23,310 --> 00:06:27,910
control it yourself and be able to show

149
00:06:25,930 --> 00:06:31,180
people those certifications on an

150
00:06:27,910 --> 00:06:33,370
as-needed basis so today we're gonna

151
00:06:31,180 --> 00:06:35,620
talk about blockchain technology okay

152
00:06:33,370 --> 00:06:38,050
have you ever heard of blockchain I have

153
00:06:35,620 --> 00:06:39,790
whenever we have a transaction and let's

154
00:06:38,050 --> 00:06:42,100
say I buy something from you this

155
00:06:39,790 --> 00:06:45,460
information gets logged and it gets

156
00:06:42,100 --> 00:06:48,460
verified by a third person or third

157
00:06:45,460 --> 00:06:50,680
party and then if like all this

158
00:06:48,460 --> 00:06:52,360
information verified and it all matches

159
00:06:50,680 --> 00:06:54,880
right the transaction goes through

160
00:06:52,360 --> 00:06:57,580
without any intermediary basically right

161
00:06:54,880 --> 00:06:59,860
then it gets stored and when we make

162
00:06:57,580 --> 00:07:02,140
further transactions this information is

163
00:06:59,860 --> 00:07:04,030
ready and better that's in in the ledger

164
00:07:02,140 --> 00:07:05,680
yeah that's exactly right

165
00:07:04,030 --> 00:07:07,750
and you can append it you can sort of

166
00:07:05,680 --> 00:07:09,910
add new information that's more current

167
00:07:07,750 --> 00:07:12,580
but you can't actually go erase anything

168
00:07:09,910 --> 00:07:14,560
absolutely right the way the technology

169
00:07:12,580 --> 00:07:16,450
is changing nothing is gonna be like it

170
00:07:14,560 --> 00:07:18,430
used to be and there's no like firm and

171
00:07:16,450 --> 00:07:20,530
then the buyer and the seller and it's

172
00:07:18,430 --> 00:07:23,140
basically will have to rewrite a lot of

173
00:07:20,530 --> 00:07:24,669
rules in economics as well for sure a

174
00:07:23,140 --> 00:07:26,800
lot of the assumptions won't hold true

175
00:07:24,669 --> 00:07:28,750
same with who are the who are the actors

176
00:07:26,800 --> 00:07:31,180
it's not just people anymore it's

177
00:07:28,750 --> 00:07:33,520
machines absolutely we're gonna have to

178
00:07:31,180 --> 00:07:35,800
create entire new concepts of how they

179
00:07:33,520 --> 00:07:38,140
do trade and how they work with us -

180
00:07:35,800 --> 00:07:40,960
what kind of barriers or roadblocks

181
00:07:38,140 --> 00:07:43,300
would you imagine are gonna happen in

182
00:07:40,960 --> 00:07:45,640
the blockchain space so at least going

183
00:07:43,300 --> 00:07:48,039
there we we talk about Bitcoin there are

184
00:07:45,640 --> 00:07:53,140
some trust issues have been some hacks

185
00:07:48,039 --> 00:07:56,860
and so there's obviously a need to work

186
00:07:53,140 --> 00:07:59,350
on on trust and feeling that it's it's a

187
00:07:56,860 --> 00:08:01,450
safe technology yeah one of the problems

188
00:07:59,350 --> 00:08:04,690
that Bitcoin is faced is bitcoins

189
00:08:01,450 --> 00:08:07,120
getting stolen or lost but a lot of that

190
00:08:04,690 --> 00:08:09,610
actually comes from people trying to

191
00:08:07,120 --> 00:08:11,439
resent relize coin in different way

192
00:08:09,610 --> 00:08:13,990
making themselves actually a pretty easy

193
00:08:11,439 --> 00:08:16,120
target absolutely so education will be a

194
00:08:13,990 --> 00:08:19,449
big part before we actually can use the

195
00:08:16,120 --> 00:08:21,340
technology in a wide sense yeah to

196
00:08:19,449 --> 00:08:24,759
actually transition into the mainstream

197
00:08:21,340 --> 00:08:27,069
and make it useful for average people to

198
00:08:24,759 --> 00:08:29,469
use we're going to need to make sure we

199
00:08:27,069 --> 00:08:32,200
have a lot more education a lot more

200
00:08:29,469 --> 00:08:34,570
standards and probably work with a lot

201
00:08:32,200 --> 00:08:37,510
of enterprises to create sort of the

202
00:08:34,570 --> 00:08:40,209
user experience around this being a

203
00:08:37,510 --> 00:08:42,820
technology that is safe and usable and

204
00:08:40,209 --> 00:08:44,079
understandable what is the current state

205
00:08:42,820 --> 00:08:46,750
of blockchain in your understanding

206
00:08:44,079 --> 00:08:49,300
what's gonna have to happen next the

207
00:08:46,750 --> 00:08:53,050
current state is in this the research

208
00:08:49,300 --> 00:08:55,720
state and it's being developed to be

209
00:08:53,050 --> 00:08:57,850
applied in many many areas it might be

210
00:08:55,720 --> 00:09:00,160
used almost everywhere - we're like we

211
00:08:57,850 --> 00:09:02,440
won't even imagine how we live without

212
00:09:00,160 --> 00:09:04,870
it but I'm not sure where it's gonna go

213
00:09:02,440 --> 00:09:09,100
I mean no one really knows right well we

214
00:09:04,870 --> 00:09:10,839
have a lot of public box chains like we

215
00:09:09,100 --> 00:09:15,329
have Bitcoin we have the etherium

216
00:09:10,839 --> 00:09:17,860
Network NXT but a lot of companies and

217
00:09:15,329 --> 00:09:21,220
consortiums are getting together to

218
00:09:17,860 --> 00:09:23,529
build private blockchains so ones that

219
00:09:21,220 --> 00:09:25,720
are more closed off at first and then

220
00:09:23,529 --> 00:09:28,120
they evolved into a public network when

221
00:09:25,720 --> 00:09:30,370
people feel comfortable using it and

222
00:09:28,120 --> 00:09:33,040
some are also proof of concepts out in

223
00:09:30,370 --> 00:09:36,490
the real world projects in energy and in

224
00:09:33,040 --> 00:09:38,440
pharmaceuticals and in retail and lots

225
00:09:36,490 --> 00:09:40,779
of different fields are starting

226
00:09:38,440 --> 00:09:43,269
experiments and we'll see in the next

227
00:09:40,779 --> 00:09:45,850
few years how all of those interact and

228
00:09:43,269 --> 00:09:48,310
what we learn about the best use cases

229
00:09:45,850 --> 00:09:51,610
for blockchain and what it means for

230
00:09:48,310 --> 00:09:53,430
trade so tell me your version of a

231
00:09:51,610 --> 00:09:55,570
technical definition of blockchain a

232
00:09:53,430 --> 00:09:58,870
technical definition of blockchain is

233
00:09:55,570 --> 00:10:02,110
that it is a persistent transparent

234
00:09:58,870 --> 00:10:05,470
public append-only ledger so it is a

235
00:10:02,110 --> 00:10:08,680
system that you can add data to and not

236
00:10:05,470 --> 00:10:11,620
change previous data within it it does

237
00:10:08,680 --> 00:10:14,709
this through a mechanism for creating

238
00:10:11,620 --> 00:10:17,230
consensus between scattered or

239
00:10:14,709 --> 00:10:18,850
distributed parties that do not need to

240
00:10:17,230 --> 00:10:20,620
trust each other but just need to trust

241
00:10:18,850 --> 00:10:21,770
the mechanism by which their consensus

242
00:10:20,620 --> 00:10:24,650
has arrived at

243
00:10:21,770 --> 00:10:28,340
in the case of blockchain they realize

244
00:10:24,650 --> 00:10:31,040
on some form of challenge such that no

245
00:10:28,340 --> 00:10:34,160
one actor on the network is able to

246
00:10:31,040 --> 00:10:37,400
solve this challenge consistently more

247
00:10:34,160 --> 00:10:39,200
than everyone else on the network yes

248
00:10:37,400 --> 00:10:42,680
yeah it randomizes the process and in

249
00:10:39,200 --> 00:10:45,020
theory ensures that no one can force the

250
00:10:42,680 --> 00:10:46,760
blockchain to accept a particular entry

251
00:10:45,020 --> 00:10:49,880
onto the ledger that others disagree

252
00:10:46,760 --> 00:10:52,070
with one that relies on the mechanism

253
00:10:49,880 --> 00:10:55,640
for a peer-to-peer network that can

254
00:10:52,070 --> 00:10:57,770
maintain updates to the ledger and then

255
00:10:55,640 --> 00:10:59,420
verify those updates in such a way that

256
00:10:57,770 --> 00:11:01,580
it is impossible to defraud and

257
00:10:59,420 --> 00:11:03,710
impossible to alter after the fact do

258
00:11:01,580 --> 00:11:05,810
you see it as defining a new discipline

259
00:11:03,710 --> 00:11:08,510
of kinds or where where are we gonna see

260
00:11:05,810 --> 00:11:10,940
blockchain emerge in the real world

261
00:11:08,510 --> 00:11:12,950
first I mean just so like as an example

262
00:11:10,940 --> 00:11:14,720
one thing that I think about a lot in

263
00:11:12,950 --> 00:11:17,180
terms of possible blockchain

264
00:11:14,720 --> 00:11:19,580
applications is electricity right the

265
00:11:17,180 --> 00:11:21,890
next generation of distributed smart

266
00:11:19,580 --> 00:11:23,720
grid technology effectively because

267
00:11:21,890 --> 00:11:25,070
working on yes yeah yeah exactly

268
00:11:23,720 --> 00:11:25,970
and it's this very fruitful area of

269
00:11:25,070 --> 00:11:28,070
research and you can find yourself

270
00:11:25,970 --> 00:11:30,440
looking like 20 years out right where

271
00:11:28,070 --> 00:11:32,810
you have an enormous number like of

272
00:11:30,440 --> 00:11:36,250
electric cars you have all of these

273
00:11:32,810 --> 00:11:39,350
batteries like that's essentially a

274
00:11:36,250 --> 00:11:40,850
distributed peak load power grid right

275
00:11:39,350 --> 00:11:42,620
like the cars are getting plugged in and

276
00:11:40,850 --> 00:11:44,530
unplugged at different times if you have

277
00:11:42,620 --> 00:11:47,780
a mechanism that is able to

278
00:11:44,530 --> 00:11:50,120
automatically and autonomously be

279
00:11:47,780 --> 00:11:51,710
distributing power based on batteries

280
00:11:50,120 --> 00:11:53,690
that are scattered throughout the grid

281
00:11:51,710 --> 00:11:55,100
that are being used for other purposes

282
00:11:53,690 --> 00:11:56,720
their owners don't even necessarily need

283
00:11:55,100 --> 00:11:58,540
to be aware you begin to have something

284
00:11:56,720 --> 00:12:00,860
that looks like a much more viable

285
00:11:58,540 --> 00:12:02,600
society that still has a lot of

286
00:12:00,860 --> 00:12:04,459
electricity needs but is able to base

287
00:12:02,600 --> 00:12:06,260
that much more on renewables is able to

288
00:12:04,459 --> 00:12:08,390
make up the difference during peak load

289
00:12:06,260 --> 00:12:10,100
periods or during differences in weather

290
00:12:08,390 --> 00:12:11,750
that is able to have power much closer

291
00:12:10,100 --> 00:12:13,010
to where it's needed rather than having

292
00:12:11,750 --> 00:12:15,980
to be distributed over great distances

293
00:12:13,010 --> 00:12:18,529
like that's something that would be an

294
00:12:15,980 --> 00:12:19,910
enormous ly hard problem to solve and

295
00:12:18,529 --> 00:12:21,680
it's not that the blockchain makes it

296
00:12:19,910 --> 00:12:24,170
easy but it makes it possible a lot of

297
00:12:21,680 --> 00:12:26,570
people are seeing watching the news and

298
00:12:24,170 --> 00:12:28,780
maybe seeing a lot of sort of initial

299
00:12:26,570 --> 00:12:31,450
coin offerings this sort of monetization

300
00:12:28,780 --> 00:12:33,520
opportunities

301
00:12:31,450 --> 00:12:36,430
excited about it now some of that

302
00:12:33,520 --> 00:12:38,200
excitement is is real and should be you

303
00:12:36,430 --> 00:12:40,030
know encouraged but some of it is also

304
00:12:38,200 --> 00:12:42,160
hype how do you feel about the hype

305
00:12:40,030 --> 00:12:43,600
cycle around blockchain today I'm glad

306
00:12:42,160 --> 00:12:46,090
you mentioned initial coin offerings

307
00:12:43,600 --> 00:12:48,520
because for me those really exemplified

308
00:12:46,090 --> 00:12:50,650
the sort of that the problem that we're

309
00:12:48,520 --> 00:12:53,080
having at this exact moment they are a

310
00:12:50,650 --> 00:12:54,550
an idea with enormous potential

311
00:12:53,080 --> 00:12:56,890
significance down the road

312
00:12:54,550 --> 00:12:58,690
however the promise of the initial coin

313
00:12:56,890 --> 00:13:00,670
offering has been kind of hijacked into

314
00:12:58,690 --> 00:13:02,830
this string of like basically pump and

315
00:13:00,670 --> 00:13:06,190
dump scams or sort of desperate gold

316
00:13:02,830 --> 00:13:09,070
rush schemes the faster that we can we

317
00:13:06,190 --> 00:13:11,440
can shift from this this fantasy that it

318
00:13:09,070 --> 00:13:15,030
is going to make you rich in a sort of

319
00:13:11,440 --> 00:13:18,910
you know 1920s tulip bubble kind of way

320
00:13:15,030 --> 00:13:20,320
is is that is the sooner that we can get

321
00:13:18,910 --> 00:13:22,330
into people honestly and I mean this

322
00:13:20,320 --> 00:13:24,400
like with all the hope in the world

323
00:13:22,330 --> 00:13:26,500
people being disappointed in blockchains

324
00:13:24,400 --> 00:13:28,330
like oh I didn't get rich it's just

325
00:13:26,500 --> 00:13:30,490
being used now to make it easier to

326
00:13:28,330 --> 00:13:32,860
transact goods and services safely

327
00:13:30,490 --> 00:13:35,050
across borders this is an exciting at

328
00:13:32,860 --> 00:13:36,730
all and then we'll see that like become

329
00:13:35,050 --> 00:13:37,810
part of the everyday infrastructure of

330
00:13:36,730 --> 00:13:39,340
the world in a way that I think will be

331
00:13:37,810 --> 00:13:40,900
very significant but I think it is now

332
00:13:39,340 --> 00:13:42,340
almost every aspect of it that's

333
00:13:40,900 --> 00:13:47,860
connected with the concept of money is

334
00:13:42,340 --> 00:13:49,900
wildly overhyped and people get a lot of

335
00:13:47,860 --> 00:13:56,770
excitement around it yes they're waiting

336
00:13:49,900 --> 00:13:58,810
for the world to change yeah because

337
00:13:56,770 --> 00:14:00,910
we've had a lot of PR a lot of proof of

338
00:13:58,810 --> 00:14:03,220
concept but yeah truthfully this is more

339
00:14:00,910 --> 00:14:04,960
like a science yes it will develop it

340
00:14:03,220 --> 00:14:07,810
will take five years it will take ten

341
00:14:04,960 --> 00:14:10,120
years that kind of time is necessary one

342
00:14:07,810 --> 00:14:12,130
of the areas that brought me to supply

343
00:14:10,120 --> 00:14:13,900
chain research is the fact that we're

344
00:14:12,130 --> 00:14:15,760
we're not just gonna see blockchain

345
00:14:13,900 --> 00:14:17,650
emerge we're gonna see artificial

346
00:14:15,760 --> 00:14:19,630
intelligence continue to evolve we're

347
00:14:17,650 --> 00:14:21,600
gonna see connected devices and this

348
00:14:19,630 --> 00:14:24,250
sort of growing internet of things

349
00:14:21,600 --> 00:14:26,260
machinery that can do a lot more and

350
00:14:24,250 --> 00:14:29,380
have a wallet and transact on their own

351
00:14:26,260 --> 00:14:31,510
and so I actually think a lot of where

352
00:14:29,380 --> 00:14:33,880
blockchain and other technologies are

353
00:14:31,510 --> 00:14:36,370
headed is this interesting synergy that

354
00:14:33,880 --> 00:14:38,260
will help us elevate the kinds of

355
00:14:36,370 --> 00:14:40,530
institutions we've used in the past

356
00:14:38,260 --> 00:14:43,770
gobbled together yeah to create that

357
00:14:40,530 --> 00:14:46,200
identifiable verifiable trust

358
00:14:43,770 --> 00:14:48,150
and one of the most interesting ideas to

359
00:14:46,200 --> 00:14:49,710
me that came out of the currency side of

360
00:14:48,150 --> 00:14:52,350
the blockchain project was the notion

361
00:14:49,710 --> 00:14:55,380
around like coloring coins right assign

362
00:14:52,350 --> 00:14:57,030
properties to a particular coin and in

363
00:14:55,380 --> 00:14:58,170
particular to assign those properties in

364
00:14:57,030 --> 00:15:00,120
ways that would allow you to do things

365
00:14:58,170 --> 00:15:02,490
like bring in the graph of previous

366
00:15:00,120 --> 00:15:04,140
transactions you know this is sort of an

367
00:15:02,490 --> 00:15:05,070
obvious application for this which is

368
00:15:04,140 --> 00:15:06,660
something that would look a little bit

369
00:15:05,070 --> 00:15:08,730
like an extreme version of like a

370
00:15:06,660 --> 00:15:10,140
boycott divestment and sanctions kind of

371
00:15:08,730 --> 00:15:12,150
approach right a model where it's sort

372
00:15:10,140 --> 00:15:13,680
of like I will not touch money my wallet

373
00:15:12,150 --> 00:15:15,060
will automatically not touch money that

374
00:15:13,680 --> 00:15:17,580
has been exchanged with the following as

375
00:15:15,060 --> 00:15:19,050
a sort of very extreme way of cutting

376
00:15:17,580 --> 00:15:21,480
those things out but there's also ways

377
00:15:19,050 --> 00:15:24,500
in which I think that could be applied

378
00:15:21,480 --> 00:15:27,000
to to clearing out the entire logistics

379
00:15:24,500 --> 00:15:28,860
process of many kinds of bad actors

380
00:15:27,000 --> 00:15:31,500
about trying to make a supply chain

381
00:15:28,860 --> 00:15:33,480
better in some ways realize a lot on

382
00:15:31,500 --> 00:15:34,980
whose version of better that is and

383
00:15:33,480 --> 00:15:39,030
exactly kind of what gets qualified but

384
00:15:34,980 --> 00:15:40,800
also how that is quantified within say

385
00:15:39,030 --> 00:15:42,390
some kind of like smart contract system

386
00:15:40,800 --> 00:15:44,490
there's multiple ways that I could see

387
00:15:42,390 --> 00:15:47,280
this backfiring and one is the ways in

388
00:15:44,490 --> 00:15:49,530
which that is implemented for example

389
00:15:47,280 --> 00:15:52,170
leaving certain kinds of bad behavior

390
00:15:49,530 --> 00:15:54,210
untouched or unanalyzed or one that

391
00:15:52,170 --> 00:15:57,090
feels even more dire to me in some ways

392
00:15:54,210 --> 00:15:58,920
is creating an extremely inflexible

393
00:15:57,090 --> 00:16:00,480
conte like because human contracts legal

394
00:15:58,920 --> 00:16:01,560
contracts are actually very flexible

395
00:16:00,480 --> 00:16:05,120
like there's a lot of kind of room to

396
00:16:01,560 --> 00:16:07,530
negotiate yeah but we we already have

397
00:16:05,120 --> 00:16:09,000
real-world versions of some of these

398
00:16:07,530 --> 00:16:10,470
nightmare scenarios right where like

399
00:16:09,000 --> 00:16:12,180
your kid has a medical emergency and you

400
00:16:10,470 --> 00:16:14,310
can't unlock your car because you're two

401
00:16:12,180 --> 00:16:17,120
days behind on payments for it and so

402
00:16:14,310 --> 00:16:19,650
the car system will no longer respond to

403
00:16:17,120 --> 00:16:21,930
the app mechanism that you're supposed

404
00:16:19,650 --> 00:16:22,920
to be able to use to drive it sort of

405
00:16:21,930 --> 00:16:24,960
rigidity

406
00:16:22,920 --> 00:16:27,150
yes structures that maybe don't

407
00:16:24,960 --> 00:16:28,890
accommodate real-life scenarios very

408
00:16:27,150 --> 00:16:30,270
well as you have been talking one of the

409
00:16:28,890 --> 00:16:32,340
things that keeps coming up is that we

410
00:16:30,270 --> 00:16:34,260
tend to hold novel technologies to an

411
00:16:32,340 --> 00:16:35,690
unrealistically high standard in terms

412
00:16:34,260 --> 00:16:38,010
of what they are supposed to deliver

413
00:16:35,690 --> 00:16:39,720
rather than comparing them to actually

414
00:16:38,010 --> 00:16:41,070
existing systems where we can begin to

415
00:16:39,720 --> 00:16:42,930
see the possibility that even like a

416
00:16:41,070 --> 00:16:44,940
slight incremental improvement would

417
00:16:42,930 --> 00:16:48,930
still be an enormous game the system we

418
00:16:44,940 --> 00:16:51,150
have today is also broken it's it's hard

419
00:16:48,930 --> 00:16:54,210
to compare it to some perfect future

420
00:16:51,150 --> 00:16:56,520
just like we compare you know autonomous

421
00:16:54,210 --> 00:16:58,050
vehicles to this very high standard

422
00:16:56,520 --> 00:16:59,550
yeah because machines are doing it

423
00:16:58,050 --> 00:17:01,860
should be perfect right there should be

424
00:16:59,550 --> 00:17:03,810
zero car - yes and yeah when you say

425
00:17:01,860 --> 00:17:06,150
well there's hundreds of thousands

426
00:17:03,810 --> 00:17:08,640
millions of you know car deaths on the

427
00:17:06,150 --> 00:17:10,350
road due to human error today I think

428
00:17:08,640 --> 00:17:12,929
we're in a similar place we're going to

429
00:17:10,350 --> 00:17:15,329
see problems with this technology and we

430
00:17:12,929 --> 00:17:18,270
are going to need to develop multiple

431
00:17:15,329 --> 00:17:22,530
kinds of standards that also have some

432
00:17:18,270 --> 00:17:27,569
flexibility but it's it's going to also

433
00:17:22,530 --> 00:17:29,429
be a version a system that allows us to

434
00:17:27,569 --> 00:17:31,910
fix some of the things that are broken

435
00:17:29,429 --> 00:17:35,370
today blockchain may sound complicated

436
00:17:31,910 --> 00:17:38,040
but at its core it's just another tool

437
00:17:35,370 --> 00:17:40,260
for humans and eventually robots and

438
00:17:38,040 --> 00:17:42,510
other kinds of identities to trade at

439
00:17:40,260 --> 00:17:45,090
scale and make that trade more

440
00:17:42,510 --> 00:17:46,920
decentralized and it's part of our

441
00:17:45,090 --> 00:17:48,510
future so it's important that people

442
00:17:46,920 --> 00:17:51,380
have these kinds of conversations and

443
00:17:48,510 --> 00:17:51,380
start to learn about it

