1
00:00:00,060 --> 00:00:04,350
my name is Bobby Kasturi I'm a assistant

2
00:00:02,460 --> 00:00:06,240
professor at the University of Chicago

3
00:00:04,350 --> 00:00:21,510
and a neuroscientist at Argonne National

4
00:00:06,240 --> 00:00:24,060
Labs what the connectome is is it's a

5
00:00:21,510 --> 00:00:26,640
kind of a newly made-up term for

6
00:00:24,060 --> 00:00:28,529
describing a kind of neuroscience

7
00:00:26,640 --> 00:00:31,099
research where we try to map the brain

8
00:00:28,529 --> 00:00:33,899
at a scale that's never been back before

9
00:00:31,099 --> 00:00:39,059
every person here can leave with

10
00:00:33,899 --> 00:00:43,800
understanding it at some level do you

11
00:00:39,059 --> 00:00:45,329
know why we're here today yes we're

12
00:00:43,800 --> 00:00:47,730
gonna talk about science and we're gonna

13
00:00:45,329 --> 00:00:50,550
talk about a very specific kind of

14
00:00:47,730 --> 00:00:55,699
science about people who study brains

15
00:00:50,550 --> 00:00:55,699
did you know what a brain is what is it

16
00:00:57,350 --> 00:01:02,430
definitely so we're gonna talk about

17
00:01:00,030 --> 00:01:05,280
this is something that people study in

18
00:01:02,430 --> 00:01:08,580
the brain called the connectome do you

19
00:01:05,280 --> 00:01:13,170
know that your body is made up of really

20
00:01:08,580 --> 00:01:15,210
tiny things called cells okay

21
00:01:13,170 --> 00:01:18,270
well there's more cells in your brain

22
00:01:15,210 --> 00:01:23,280
like way more cells and then all the

23
00:01:18,270 --> 00:01:26,580
stars we can see and so what the

24
00:01:23,280 --> 00:01:29,670
connectome is is we'd like to know where

25
00:01:26,580 --> 00:01:32,540
every cell in your brain is and how it

26
00:01:29,670 --> 00:01:36,920
talks to every other cell in your brain

27
00:01:32,540 --> 00:01:36,920
that was awesome Daniel thank you

28
00:01:38,119 --> 00:01:43,860
connectome connectome to be honest I

29
00:01:42,150 --> 00:01:45,420
have no idea that's good that's a great

30
00:01:43,860 --> 00:01:48,509
place to start there are cells in your

31
00:01:45,420 --> 00:01:51,299
brain those brain cells are connected by

32
00:01:48,509 --> 00:01:54,960
wires to each other electricity travels

33
00:01:51,299 --> 00:01:57,240
down those wires and communicates from

34
00:01:54,960 --> 00:01:59,040
one part of the brain to the other part

35
00:01:57,240 --> 00:02:01,770
of the brain and each of those brain

36
00:01:59,040 --> 00:02:03,810
cells makes you know a thousand

37
00:02:01,770 --> 00:02:07,290
connections something like a hundred

38
00:02:03,810 --> 00:02:09,840
trillion connections in one brain in

39
00:02:07,290 --> 00:02:10,960
your brain could I take all of that

40
00:02:09,840 --> 00:02:14,620
information

41
00:02:10,960 --> 00:02:18,070
and put it inside a computer would that

42
00:02:14,620 --> 00:02:19,360
computer then be you computers they

43
00:02:18,070 --> 00:02:21,400
don't have fillings they won't have

44
00:02:19,360 --> 00:02:23,620
fillings and I think that's one thing

45
00:02:21,400 --> 00:02:25,750
that makes the human race wrong I would

46
00:02:23,620 --> 00:02:29,020
say that that map also has your feelings

47
00:02:25,750 --> 00:02:31,360
in it because here's why your feelings

48
00:02:29,020 --> 00:02:34,600
most neuroscientists think come from

49
00:02:31,360 --> 00:02:37,570
your brain anyway and amazingly weather

50
00:02:34,600 --> 00:02:38,320
when you feel happy or sad or angry or

51
00:02:37,570 --> 00:02:40,510
scared

52
00:02:38,320 --> 00:02:44,920
that's just brain cells communicating

53
00:02:40,510 --> 00:02:52,170
with each other so I think today we're

54
00:02:44,920 --> 00:02:55,030
going to talk about a connecting yeah no

55
00:02:52,170 --> 00:02:58,890
it's a map of all the connections

56
00:02:55,030 --> 00:03:01,570
between every neuron in your brain

57
00:02:58,890 --> 00:03:04,540
literally in a human brain something

58
00:03:01,570 --> 00:03:06,430
like the map of the one quadrillion

59
00:03:04,540 --> 00:03:08,800
connections that a hundred billion

60
00:03:06,430 --> 00:03:12,010
neurons make with each other is this

61
00:03:08,800 --> 00:03:13,360
like a map where that's like an actual

62
00:03:12,010 --> 00:03:16,060
visual representation like using

63
00:03:13,360 --> 00:03:18,090
microscopy or just data Wow well I'm

64
00:03:16,060 --> 00:03:22,900
understanding more so that it's these

65
00:03:18,090 --> 00:03:25,420
these the a mapping of the nerve the the

66
00:03:22,900 --> 00:03:30,190
circuitry the pathways between neurons

67
00:03:25,420 --> 00:03:31,450
that can lead to evidence of patterns in

68
00:03:30,190 --> 00:03:33,490
your brain that are common between

69
00:03:31,450 --> 00:03:35,320
different people we have to use electron

70
00:03:33,490 --> 00:03:37,300
microscopes and then we have been

71
00:03:35,320 --> 00:03:40,270
developing our ways to slice the brain

72
00:03:37,300 --> 00:03:42,250
into really thin slices use an electron

73
00:03:40,270 --> 00:03:44,590
microscope to take a picture of each

74
00:03:42,250 --> 00:03:46,960
slice and then use computers to put it

75
00:03:44,590 --> 00:03:50,260
all back imagine that we could get the

76
00:03:46,960 --> 00:03:52,900
map of every connection right and we

77
00:03:50,260 --> 00:03:54,880
knew how neurons fired do you think we

78
00:03:52,900 --> 00:03:58,300
could put that in a computer that map

79
00:03:54,880 --> 00:04:00,430
and then therefore that computer should

80
00:03:58,300 --> 00:04:03,550
be able to think just like the brain

81
00:04:00,430 --> 00:04:04,990
that we extracted it from well the

82
00:04:03,550 --> 00:04:06,940
computer only communicates with itself

83
00:04:04,990 --> 00:04:08,740
in binary so it only has two options it

84
00:04:06,940 --> 00:04:12,250
can only ask itself yes-or-no questions

85
00:04:08,740 --> 00:04:14,440
but a human brain has an infinity of

86
00:04:12,250 --> 00:04:19,270
directions that it can go neurons are

87
00:04:14,440 --> 00:04:21,640
also digital meaning a neuron either

88
00:04:19,270 --> 00:04:22,400
fires or it doesn't fire so that's

89
00:04:21,640 --> 00:04:25,910
either

90
00:04:22,400 --> 00:04:28,040
one or zero and it's the combination of

91
00:04:25,910 --> 00:04:30,770
those ones or zeros that actually

92
00:04:28,040 --> 00:04:36,650
produce the 10,000 different answers

93
00:04:30,770 --> 00:04:38,690
that you say it's a large-scale attempt

94
00:04:36,650 --> 00:04:40,729
to understand the wiring map of the

95
00:04:38,690 --> 00:04:44,419
brain definitely great

96
00:04:40,729 --> 00:04:46,400
I think that it's definitely needed ha

97
00:04:44,419 --> 00:04:48,650
understanding the anatomy of the brain

98
00:04:46,400 --> 00:04:50,330
is definitely important but it doesn't

99
00:04:48,650 --> 00:04:52,460
necessarily tell us everything about the

100
00:04:50,330 --> 00:04:55,250
function so there's some sort of

101
00:04:52,460 --> 00:04:56,990
temporal order from neuron to neuron and

102
00:04:55,250 --> 00:04:59,780
region to region that we may not be able

103
00:04:56,990 --> 00:05:04,310
to pick up this is where it gets really

104
00:04:59,780 --> 00:05:06,530
crazy could we simulate that map inside

105
00:05:04,310 --> 00:05:08,990
a computer and would that computer then

106
00:05:06,530 --> 00:05:11,389
be thinking like that original brain for

107
00:05:08,990 --> 00:05:13,639
which we made the map I mean that's not

108
00:05:11,389 --> 00:05:17,060
that's not the person I mean having a

109
00:05:13,639 --> 00:05:19,699
representation of someone's neural

110
00:05:17,060 --> 00:05:22,900
network is just that it's just a

111
00:05:19,699 --> 00:05:25,729
representation of their neural network

112
00:05:22,900 --> 00:05:28,460
because there's more to the to you in

113
00:05:25,729 --> 00:05:31,880
here than just information passing

114
00:05:28,460 --> 00:05:34,190
between neurons I'd like to think so huh

115
00:05:31,880 --> 00:05:36,949
it would be like if you simulated a

116
00:05:34,190 --> 00:05:39,139
hurricane imagine we can keep track of

117
00:05:36,949 --> 00:05:42,380
every variable of a hurricane wind speed

118
00:05:39,139 --> 00:05:45,020
every water molecule etc etc temperature

119
00:05:42,380 --> 00:05:48,320
and we put that inside a super-fast

120
00:05:45,020 --> 00:05:49,849
computer and we simulated right I don't

121
00:05:48,320 --> 00:05:52,940
think anyone would think that the inside

122
00:05:49,849 --> 00:05:54,919
of the computer would get wet even

123
00:05:52,940 --> 00:05:58,580
though we had simulated the hurricane

124
00:05:54,919 --> 00:06:01,250
perfectly that wetness is consciousness

125
00:05:58,580 --> 00:06:04,099
is what we are is it ethical to imagine

126
00:06:01,250 --> 00:06:06,229
mapping a male brain versus a female

127
00:06:04,099 --> 00:06:09,409
brain to look for differences between

128
00:06:06,229 --> 00:06:11,389
those two explain alleged behavioral

129
00:06:09,409 --> 00:06:13,639
differences between them every single

130
00:06:11,389 --> 00:06:16,940
person is different and so it should be

131
00:06:13,639 --> 00:06:19,699
okay to map every single person's brain

132
00:06:16,940 --> 00:06:22,699
I mean I understand that there are that

133
00:06:19,699 --> 00:06:23,210
it's very sensitive you know you're

134
00:06:22,699 --> 00:06:24,979
sensitive

135
00:06:23,210 --> 00:06:25,900
mapping in Indian brain versus a

136
00:06:24,979 --> 00:06:28,120
Caucasian brain

137
00:06:25,900 --> 00:06:32,669
or politically I think that people may

138
00:06:28,120 --> 00:06:35,050
have some issue with mapping out what

139
00:06:32,669 --> 00:06:40,080
causes or what makes a difference

140
00:06:35,050 --> 00:06:43,180
between huh different types of people

141
00:06:40,080 --> 00:06:44,590
maybe a wiring diagram is not sufficient

142
00:06:43,180 --> 00:06:46,270
to understand the brain and it would be

143
00:06:44,590 --> 00:06:48,250
crazy to think that that would be

144
00:06:46,270 --> 00:06:50,139
sufficient actually if you limit the

145
00:06:48,250 --> 00:06:51,759
connect them to be just the wiring

146
00:06:50,139 --> 00:06:54,789
diagram without you know more

147
00:06:51,759 --> 00:06:56,919
information about myelination or glial

148
00:06:54,789 --> 00:06:59,320
cells correct all types of environmental

149
00:06:56,919 --> 00:07:01,660
features that surround the neurons and

150
00:06:59,320 --> 00:07:03,490
axons then then you have an incomplete

151
00:07:01,660 --> 00:07:06,820
picture right no no sometimes when

152
00:07:03,490 --> 00:07:08,949
people get they worry about connectomics

153
00:07:06,820 --> 00:07:11,080
I think what they're actually worrying

154
00:07:08,949 --> 00:07:13,090
about is that it's the end of the way

155
00:07:11,080 --> 00:07:14,800
that we used to do neuroscience what do

156
00:07:13,090 --> 00:07:18,910
you think about memory do you think that

157
00:07:14,800 --> 00:07:20,229
there's ways of resolving the substrate

158
00:07:18,910 --> 00:07:24,009
of human memories you know is it just

159
00:07:20,229 --> 00:07:26,259
LTP and OCD I'm not sure if you had a

160
00:07:24,009 --> 00:07:28,960
connect home of a human brain of an

161
00:07:26,259 --> 00:07:32,320
adult human I would be able to read out

162
00:07:28,960 --> 00:07:34,479
memories from that you don't think it's

163
00:07:32,320 --> 00:07:36,130
just the synaptic weights like a an

164
00:07:34,479 --> 00:07:37,270
artificial neural network that's trying

165
00:07:36,130 --> 00:07:40,000
to do a particular it could absolutely

166
00:07:37,270 --> 00:07:42,159
be but without knowing what the weights

167
00:07:40,000 --> 00:07:44,409
were before the memory was made what if

168
00:07:42,159 --> 00:07:46,780
you had a violinist learn a piece of

169
00:07:44,409 --> 00:07:48,909
Bach music yes could you find those

170
00:07:46,780 --> 00:07:53,380
notes somewhere in their brain yes I

171
00:07:48,909 --> 00:07:55,780
didn't know before yes you know at I'm a

172
00:07:53,380 --> 00:07:58,030
musician and I don't think it's possible

173
00:07:55,780 --> 00:08:00,490
I think that there are too many you know

174
00:07:58,030 --> 00:08:03,099
so much of it is associative to what you

175
00:08:00,490 --> 00:08:04,690
already know uh-huh and as a musician

176
00:08:03,099 --> 00:08:07,000
how much of it do you think is in your

177
00:08:04,690 --> 00:08:10,449
hands versus in your brains meaning like

178
00:08:07,000 --> 00:08:12,699
you do have connections in your muscles

179
00:08:10,449 --> 00:08:15,909
from the nerves that are from your

180
00:08:12,699 --> 00:08:17,949
spinal cord what if some of the learning

181
00:08:15,909 --> 00:08:20,229
is there are you still doing yeah more

182
00:08:17,949 --> 00:08:22,630
yeah and we do a lot of x-ray in

183
00:08:20,229 --> 00:08:24,789
addition to II M and this is actually

184
00:08:22,630 --> 00:08:26,409
I'm not saying it's the only problem but

185
00:08:24,789 --> 00:08:27,430
it's the only problem that needs to be

186
00:08:26,409 --> 00:08:30,190
solved right away

187
00:08:27,430 --> 00:08:31,780
is that the data analysis right in fact

188
00:08:30,190 --> 00:08:35,959
I think we calculated that there aren't

189
00:08:31,780 --> 00:08:38,310
enough humans ever to map a mouse brain

190
00:08:35,959 --> 00:08:42,390
wherever you collect every connection

191
00:08:38,310 --> 00:08:46,200
and etc so the problem is to get

192
00:08:42,390 --> 00:08:49,399
algorithms to to trace to recognize

193
00:08:46,200 --> 00:08:51,930
things in brains the way humans

194
00:08:49,399 --> 00:08:54,450
recognize things in brains or map things

195
00:08:51,930 --> 00:08:57,149
trace things in brains it's gonna cost a

196
00:08:54,450 --> 00:08:59,490
lot of money to imagine setting the gold

197
00:08:57,149 --> 00:09:02,279
standard for the wiring diagram even

198
00:08:59,490 --> 00:09:03,570
once and that's what I'm those are the

199
00:09:02,279 --> 00:09:05,670
kinds of ethical concerns that I'm

200
00:09:03,570 --> 00:09:07,890
worried about one of the things that

201
00:09:05,670 --> 00:09:10,200
we're not doing well as a field is sort

202
00:09:07,890 --> 00:09:13,380
of educating and telling people beyond

203
00:09:10,200 --> 00:09:15,959
our field the benefits of what we could

204
00:09:13,380 --> 00:09:17,310
achieve but I'm impressed that when you

205
00:09:15,959 --> 00:09:19,829
talk to people about something that

206
00:09:17,310 --> 00:09:21,540
seems kind of crazy and outlandish and

207
00:09:19,829 --> 00:09:23,700
perhaps they hadn't been talking about

208
00:09:21,540 --> 00:09:25,620
before it doesn't take them long to come

209
00:09:23,700 --> 00:09:27,390
to a kind of considered opinion

210
00:09:25,620 --> 00:09:30,209
especially children I think it's kind of

211
00:09:27,390 --> 00:09:32,490
amazing I mean I do hope that more

212
00:09:30,209 --> 00:09:35,490
people talk about brains and what we use

213
00:09:32,490 --> 00:09:38,610
brains for and the ways that we should

214
00:09:35,490 --> 00:09:42,680
use our brains so I think this field has

215
00:09:38,610 --> 00:09:42,680
the opportunity to make that more real

